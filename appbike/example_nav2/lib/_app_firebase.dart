// part of 'main.dart';
//
// final AndroidInitializationSettings initializationSettingsAndroid =
//     AndroidInitializationSettings('@mipmap/ic_launcher');
// final IOSInitializationSettings initializationSettingsIOS =
//     IOSInitializationSettings();
// final MacOSInitializationSettings initializationSettingsMacOS =
//     MacOSInitializationSettings();
// final InitializationSettings initializationSettings = InitializationSettings(
//     android: initializationSettingsAndroid,
//     iOS: initializationSettingsIOS,
//     macOS: initializationSettingsMacOS);
//
// Future<void> _onBackgroundMessageListen(RemoteMessage message) async {
//   await Firebase.initializeApp();
//   // GlobalState.fcmLog +=
//   //     'onBackgroundMessageListen_'; // + json.encode(message ?? '');
//   MessageHandle.onBackgroundMessageListen(message);
// }
//
// Stream<String>? _tokenStream;
//
// /// Create a [AndroidNotificationChannel] for heads up notifications
// AndroidNotificationChannel? channel;
//
// /// Initialize the [FlutterLocalNotificationsPlugin] package.
// FlutterLocalNotificationsPlugin? flutterLocalNotificationsPlugin;
//
// class MessageHandle {
//   static void getInitialMessage(RemoteMessage message) {}
//
//   static void getNotificationAppLaunchDetails(String payload) {
//     print(payload);
//   }
//
//   static void onBackgroundMessageListen(RemoteMessage message) {
//     print('Handling a background message ${message.messageId}');
//   }
//
//   static void onMessageOpenedAppListen(RemoteMessage message) {
//     print('A new onMessageOpenedApp event was published!');
//   }
//
//   // static void onMessageListen(
//   //     RemoteMessage message, FCMMessageState notificationState) {
//   //   notificationState.addMessage(message);
//   // }
// }
