import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_navigation/src/nav2/get_router_delegate.dart';

import 'app/routes/app_pages.dart';
import 'lang/translation_service.dart';

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
  print('Handling a background message ${message.messageId}');
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
  runApp(BikeApp());
}

class BikeApp extends StatefulWidget {
  _BikeAppState createState() => _BikeAppState();
}

class _BikeAppState extends State<BikeApp> {
  @override
  void initState() {
    super.initState();
    FirebaseMessaging.instance
        .getInitialMessage()
        .then((RemoteMessage? message) {
      print('getInitialMessage');
    });

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      print('onMessageOpenedApp');
    });
    registerNotification();
  }

  Future<void> registerNotification() async {
    FirebaseMessaging _messaging = FirebaseMessaging.instance;

    NotificationSettings settings = await _messaging.requestPermission(
      alert: true,
      badge: true,
      provisional: false,
      sound: true,
    );

    if (settings.authorizationStatus == AuthorizationStatus.authorized) {
      print('User granted permission');
      FirebaseMessaging.onMessage.listen((RemoteMessage message) {
        print(
            'FirebaseMessaging.onMessage.listen: ${message.notification?.title}');
      });
    } else {
      print('User declined or has not accepted permission');
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: GetMaterialApp.router(
        debugShowCheckedModeBanner: false,
        title: "Application",
        getPages: AppPages.routes,
        routeInformationParser: GetInformationParser(
            // initialRoute: Routes.HOME,
            ),
        routerDelegate: GetDelegate(
          backButtonPopMode: PopMode.History,
          preventDuplicateHandlingMode:
              PreventDuplicateHandlingMode.PopUntilOriginalRoute,
        ),
        locale: TranslationService.locale,
        fallbackLocale: TranslationService.fallbackLocale,
        translations: TranslationService(),
      ),
    );
  }
}

void hideKeyboard() {
  FocusManager.instance.primaryFocus!.unfocus();
}

void showToast(BuildContext context, String text) {
  ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(
      content: Text(
        text,
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 14),
      ),
      duration: Duration(milliseconds: 1200),
      behavior: SnackBarBehavior.floating,
      margin: EdgeInsets.symmetric(
          horizontal: 80,
          vertical: Get.height * 0.4),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
    ),
  );
}
