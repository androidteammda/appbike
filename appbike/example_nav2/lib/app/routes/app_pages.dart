import 'package:example_nav2/app/modules/book/bindings/book_binding.dart';
import 'package:example_nav2/app/modules/book/views/address_view.dart';
import 'package:example_nav2/app/modules/book/views/book_done_view.dart';
import 'package:example_nav2/app/modules/book/views/book_view.dart';
import 'package:example_nav2/app/modules/cart/bindings/cart_binding.dart';
import 'package:example_nav2/app/modules/cart/views/cart_view.dart';
import 'package:example_nav2/app/modules/categories/bindings/categories_binding.dart';
import 'package:example_nav2/app/modules/categories/views/categories_view.dart';
import 'package:example_nav2/app/modules/chat/bindings/chat_binding.dart';
import 'package:example_nav2/app/modules/favorites/bindings/favorite_binding.dart';
import 'package:example_nav2/app/modules/forgot_pass/bindings/forgot_pass_binding.dart';
import 'package:example_nav2/app/modules/forgot_pass/view/forgot_pass_view.dart';
import 'package:example_nav2/app/modules/home/bindings/home_binding.dart';
import 'package:example_nav2/app/modules/intro/bindings/user_binding.dart';
import 'package:example_nav2/app/modules/intro/views/user_root_view.dart';
import 'package:example_nav2/app/modules/login/bindings/login_binding.dart';
import 'package:example_nav2/app/modules/login/views/login_view.dart';
import 'package:example_nav2/app/modules/menu/bindings/menu_binding.dart';
import 'package:example_nav2/app/modules/menu/views/menu_root_view.dart';
import 'package:example_nav2/app/modules/notification/bindings/notification_binding.dart';
import 'package:example_nav2/app/modules/product_details/bindings/product_detail_binding.dart';
import 'package:example_nav2/app/modules/product_details/views/product_detail_view.dart';
import 'package:example_nav2/app/modules/register/bindings/register_binding.dart';
import 'package:example_nav2/app/modules/register/view/register_view.dart';
import 'package:example_nav2/app/modules/sales/bindings/flash_sale_bindding.dart';
import 'package:example_nav2/app/modules/sales/views/flash_sale_view.dart';
import 'package:example_nav2/app/modules/search/bindings/search_binding.dart';
import 'package:example_nav2/app/modules/search/views/search_view.dart';
import 'package:get/get.dart';

import '../modules/profile/bindings/profile_binding.dart';
import '../modules/root/bindings/root_binding.dart';
import '../modules/root/views/root_view.dart';

part 'app_routes.dart';

/**
 * Quản lý và khởi tạo các pages.
 */
class AppPages {
  AppPages._();

  static const INITIAL = Routes.ROOT;

  static final routes = [
    GetPage(
      page: () => UserView(),
      name: '/',
      binding: UserBinding(),
      preventDuplicates: true,
    ),
    GetPage(name: _Paths.ROOT, page: () => RootView(), bindings: [
      RootBinding(),
      HomeBinding(),
      NotificationBinding(),
      ChatBinding(),
      FavoriteBinding(),
      ProfileBinding(),
      MenuBinding(),
    ], children: [
      GetPage(
        name: _Paths.BOOK,
        page: () => BookView(),
        bindings: [BookBindings()],
        title: "Book",
      ),
      GetPage(
        name: _Paths.CART,
        binding: CartBinding(),
        page: () => CartView(),
        title: "Book",
      ),
      GetPage(
        name: _Paths.SEARCH,
        binding: SearchBinding(),
        page: () => SearchView(),
        title: "Search",
      ),
      GetPage(
        name: _Paths.PRODUCT_DETAIL,
        page: () => ProductDetailView(),
        binding: ProductDetailBinding(),
        title: "Root Product Detail",
        // preventDuplicates: false,

      ),
      GetPage(
        name: _Paths.MENU,
        page: () => MenuRootView(),
        title: 'Menu',
        binding: MenuBinding(),
      ),
      GetPage(
        name: _Paths.REGISTER,
        page: () => RegisterView(),
        title: 'Register',
        binding: RegisterBinding(),
      ),
      GetPage(
          name: _Paths.LOGIN,
          page: () => LoginView(),
          title: 'Login',
          binding: LoginBinding(),
          children: [
            GetPage(
              name: _Paths.FORGOT_PASS,
              page: () => ForgotPassView(),
              title: 'Forgot',
              binding: ForgotPassBinding(),
            ),
          ]),
      GetPage(
        name: _Paths.CATEGORIES,
        page: () => CategoriesView(),
        title: 'Categories',
        binding: CategoriesBinding(),
      ),
      GetPage(
        name: _Paths.FLASHSALES,
        page: () => FlashSaleView(),
        title: 'Flash Sale',
        binding: FlashBindings(),
      ),
      GetPage(
        name: _Paths.BEST_SALE,
        page: () => FlashSaleView(),
        title: 'Best Sale',
        binding: FlashBindings(),
      ),
      GetPage(
        name: _Paths.ADDRESS,
        page: () => AddressView(),
        title: 'Address',
      ),
      GetPage(
        name: _Paths.BOOK_DONE,
        page: () => BookDoneView(),
        bindings: [BookBindings()],
      ),
    ]),
  ];
}
