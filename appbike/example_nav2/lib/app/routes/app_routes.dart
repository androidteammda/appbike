part of 'app_pages.dart';

// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart
/***
 * Class quản lý routes page,
 * Chuyển cấp từ parent page > child page trên 1 container : paths /A > /A/B > /A/B/C
 */
abstract class Routes {
  Routes._();

  static const ROOT = _Paths.ROOT;

  static const MENU = ROOT + _Paths.MENU;

  static const MENU_DETAIL = ROOT + _Paths.MENU_DETAIL;

  static const PRODUCT_LIST = ROOT + _Paths.PRODUCT_LIST;

  static const PRODUCT_DETAIL = ROOT + _Paths.PRODUCT_DETAIL;

  static const CART = ROOT + _Paths.CART;

  static const SEARCH = ROOT + _Paths.SEARCH;

  static const BOOK = ROOT + _Paths.BOOK;

  static const LOGIN = ROOT + _Paths.LOGIN;

  static const REGISTER = ROOT + _Paths.REGISTER;

  static const SUBSCRIPTION = ROOT + _Paths.SUBSCRIPTION;

  static const CATEGORIES = ROOT + _Paths.CATEGORIES;

  static const FLASHSALES = ROOT + _Paths.FLASHSALES;

  static const BEST_SALE = ROOT + _Paths.BEST_SALE;

  static const ADDRESS = ROOT + _Paths.ADDRESS;

  static const BOOK_DONE = ROOT + _Paths.BOOK_DONE;

  static const FORGOT_PASS = LOGIN + _Paths.FORGOT_PASS;
}

abstract class _Paths {
  static const ROOT = '/root';
  static const MENU = '/menu';
  static const PROFILE = '/profile';
  static const MENU_DETAIL = '/menu_detail';
  static const SUBSCRIPTION = '/subscription';

  static const PRODUCT_LIST = '/product_list';

  static const PRODUCT_DETAIL = '/product_detail';

  static const BOOK = '/book';

  static const CART = '/cart';

  static const SEARCH = '/search';

  //-- Open Details In BottomBar
  static const LOGIN = '/login';
  static const REGISTER = '/book_register';

  static const CATEGORIES = '/categories';

  static const FLASHSALES = '/flashsales';

  static const BEST_SALE = '/bestsale';

  static const ADDRESS = '/address';

  static const FORGOT_PASS = '/forgot_pass';

  static const BOOK_DONE = '/book_done';
}
