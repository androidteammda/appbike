class ProductFavorite {
  int? status;
  String? message;

  ProductFavorite({this.status, this.message});

  factory ProductFavorite.fromJson(Map<String, dynamic> jsonData) {
    return ProductFavorite(
      status: jsonData['status'],
      message: jsonData['message'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    return data;
  }
}
