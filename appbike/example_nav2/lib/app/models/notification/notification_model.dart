import 'dart:convert';

class NotificationModel {
  String? id;
  String? title;
  String? description;

  NotificationModel({this.id, this.title, this.description});

  factory NotificationModel.fromJson(dynamic json) => NotificationModel(
        id: json["id"].toString(),
        title: json["title"].toString(),
        description: json["description"].toString(),
      );
}

class NotificationEntity {
  String? total;
  String? perPage;
  String? currentPage;
  int? lastPage;
  List<NotificationModel>? listNotification;

  NotificationEntity({
    this.total = '',
    this.perPage = '',
    this.currentPage = '',
    this.lastPage = 0,
    this.listNotification = const [],
  });

  factory NotificationEntity.fromJson(dynamic jsonNoti) {
    try {
      var jsonData = jsonNoti['data'];
      var jsonPage = jsonData['pagination'];
      var jsonItems = jsonData['items'];

      return NotificationEntity(
        total: jsonPage["total"],
        perPage: jsonPage["per_page"],
        currentPage: jsonPage["current_page"],
        lastPage: jsonPage["last_page"],
        listNotification: (json.decode(jsonItems) as List<NotificationModel>)
            .map((e) => NotificationModel.fromJson(e))
            .toList(),
      );
    } catch (e) {
      return NotificationEntity();
    }
  }
}
