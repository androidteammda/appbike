class District {
  int? status;
  String? message;
  DataDistrict? data;

  District({this.status, this.message, this.data});

  factory District.fromJson(Map<String, dynamic> json) => District(
    status : json['status'],
    message : json['message'],
    data : json['data'] != null ? new DataDistrict.fromJson(json['data']) : null,
  );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class DataDistrict {
  List<ItemsDistrict>? items;
  Pagination? pagination;

  DataDistrict({this.items, this.pagination});

  factory DataDistrict.fromJson(Map<String, dynamic> json) => DataDistrict(
    items: List<ItemsDistrict>.from((json["items"] as Iterable)
        .map((x) => ItemsDistrict.fromJson(x as Map<String, dynamic>))),
    pagination: json['pagination'] != null
        ? new Pagination.fromJson(json['pagination'])
        : null,
  );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.items != null) {
      data['items'] = this.items!.map((v) => v.toJson()).toList();
    }
    if (this.pagination != null) {
      data['pagination'] = this.pagination!.toJson();
    }
    return data;
  }
}

class ItemsDistrict {
  String? id;
  String? name;
  String? code;

  ItemsDistrict({this.id='', this.name='', this.code='flutter build a'});

  ItemsDistrict.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['code'] = this.code;
    return data;
  }
}

class Pagination {
  String? total;
  String? perPage;
  String? currentPage;
  int? lastPage;

  Pagination({this.total, this.perPage, this.currentPage, this.lastPage});

  Pagination.fromJson(Map<String, dynamic> json) {
    total = json['total'];
    perPage = json['per_page'];
    currentPage = json['current_page'];
    lastPage = json['last_page'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['total'] = this.total;
    data['per_page'] = this.perPage;
    data['current_page'] = this.currentPage;
    data['last_page'] = this.lastPage;
    return data;
  }
}