class ConfigAppModel {
  String? version;

  FlashSaleConfig? flashSaleConfig;

  List<ImagesHomeUrl>? listImageHome;

  String? imageLogoUrl;

  String? shopInfo;

  String? linkAbout;

  String? fbMessengerUrl;

  List<RecievedBankAccounts>? listBankAccount;

  ConfigAppModel({
    this.version = '',
    this.flashSaleConfig,
    this.listImageHome,
    this.imageLogoUrl = '',
    this.shopInfo = '',
    this.linkAbout = '',
    this.fbMessengerUrl = '',
    this.listBankAccount,
  });

  factory ConfigAppModel.fromJson(dynamic jsonConfig) {
    try {
      var jsonData = jsonConfig['data'];

      List<ImagesHomeUrl> parseListImageHome() {
        List<ImagesHomeUrl> listImage = [];
        List list = jsonData['image_home_urls'];
        for (int i = 0; i < list.length; i++) {
          listImage.add(ImagesHomeUrl.fromJson(list[i]));
        }
        return listImage;
      }

      List<RecievedBankAccounts> parseListBank() {
        List<RecievedBankAccounts> listBank = [];
        List list = jsonData['recieved_bank_accounts'];
        for (int i = 0; i < list.length; i++) {
          listBank.add(RecievedBankAccounts.fromJson(list[i]));
        }
        return listBank;
      }

      return ConfigAppModel(
        version: '${jsonData["version"]}',
        flashSaleConfig: FlashSaleConfig.fromJson(jsonData['flash_sale']),
        listImageHome: parseListImageHome(),
        imageLogoUrl: '${jsonData["image_logo_url"]}',
        shopInfo: '${jsonData["shop_info"]}',
        linkAbout: '${jsonData["link_about_us"]}',
        fbMessengerUrl: '${jsonData["fb_messenger_url"]}',
        listBankAccount: parseListBank(),
      );
    } catch (e) {
      return ConfigAppModel();
    }
  }
}

class FlashSaleConfig {
  String? id;
  String? name;
  String? date_from;
  String? date_to;
  String? discount;
  String? unit;
  String? create_at;

  FlashSaleConfig({
    this.id = '',
    this.name = '',
    this.date_from = '',
    this.date_to = '',
    this.discount = '',
    this.unit = '',
    this.create_at = '',
  });

  factory FlashSaleConfig.fromJson(dynamic jsonFS) {
    try {
      return FlashSaleConfig(
        id: '${jsonFS["id"]}',
        name: '${jsonFS["name"]}',
        date_from: '${jsonFS["date_from"]}',
        date_to: '${jsonFS["date_to"]}',
        discount: '${jsonFS["discount"]}',
        unit: '${jsonFS["unit"]}',
        create_at: '${jsonFS["create_at"]}',
      );
    } catch (e) {
      return FlashSaleConfig();
    }
  }
}

class ImagesHomeUrl {
  String? category_id;
  String? image_url;

  ImagesHomeUrl({
    this.category_id = '',
    this.image_url = '',
  });

  factory ImagesHomeUrl.fromJson(dynamic jsonIMH) {
    try {
      return ImagesHomeUrl(
        category_id: '${jsonIMH["category_id"]}',
        image_url: '${jsonIMH["image_url"]}',
      );
    } catch (e) {
      return ImagesHomeUrl();
    }
  }
}

class RecievedBankAccounts {
  String? bankName;
  String? bankBranch;
  String? holderName;
  String? accountNumber;

  RecievedBankAccounts({
    this.bankName = '',
    this.bankBranch = '',
    this.holderName = '',
    this.accountNumber = '',
  });

  factory RecievedBankAccounts.fromJson(dynamic jsonFS) {
    try {
      return RecievedBankAccounts(
        bankName: '${jsonFS["bank_name"]}',
        bankBranch: '${jsonFS["bank_branch"]}',
        holderName: '${jsonFS["holder"]}',
        accountNumber: '${jsonFS["account_number"]}',
      );
    } catch (e) {
      return RecievedBankAccounts();
    }
  }
}
