class SettingModel {
  bool? isNotification;

  SettingModel({this.isNotification = false});

  factory SettingModel.fromJson(Map<String, dynamic> json) => SettingModel(
        isNotification: json["isNotification"] ?? false,
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['isNotification'] = this.isNotification;
    return data;
  }
}
