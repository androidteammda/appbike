class Promotion {
  int? status;
  String? message;
  DataPromotion? data;

  Promotion({this.status, this.message, this.data});

  factory Promotion.fromJson(Map<String, dynamic> json) {
    DataPromotion? parsePromotion() {
      try {
        if (json['data'] != null) {
          return new DataPromotion.fromJson(json['data']);
        }
        return null;
      } catch (e) {
        return null;
      }
    }

    return Promotion(
      status: json['status'],
      message: json['message'],
      data: parsePromotion(),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class DataPromotion {
  String? id;
  String? name;
  String? code;
  String? discount;
  String? dateFrom;
  String? dateTo;
  String? total;
  String? used;
  String? description;
  String? createdAt;

  DataPromotion(
      {this.id,
      this.name,
      this.code,
      this.discount,
      this.dateFrom,
      this.dateTo,
      this.total,
      this.used,
      this.description,
      this.createdAt});

  factory DataPromotion.fromJson(Map<String, dynamic> json) => DataPromotion(
        id: json['id'],
        name: json['name'],
        code: json['code'],
        discount: json['discount'],
        dateFrom: json['date_from'],
        dateTo: json['date_to'],
        total: json['total'],
        used: json['used'],
        description: json['description'],
        createdAt: json['created_at'],
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['code'] = this.code;
    data['discount'] = this.discount;
    data['date_from'] = this.dateFrom;
    data['date_to'] = this.dateTo;
    data['total'] = this.total;
    data['used'] = this.used;
    data['description'] = this.description;
    data['created_at'] = this.createdAt;
    return data;
  }
}
