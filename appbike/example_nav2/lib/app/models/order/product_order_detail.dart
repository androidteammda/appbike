import 'package:example_nav2/app/models/product/products_model.dart';

class ProductOrderDetail {
  String payment_method = '';
  String ship_method = '';
  String promotion_code = '';
  String address = '';
  String region = '';
  String district = '';
  String regionId = '';
  String districtId = '';
  String name = '';
  String phone = '';
  String customer_name = '';
  String note = '';
  int ship_fee = 0;
  String payment_fee = '';
  String total_weight = '';
  String ship_date = '';
  String ship_hour = '';
  var products = <ItemsProduct>[];

  ProductOrderDetail({
    this.payment_method = '',
    this.ship_method = '',
    this.address = '',
    this.region = 'Tỉnh/Thành phố',
    this.district = 'Quận/Huyện',
    this.regionId = '',
    this.districtId = '',
    this.name = '',
    this.phone = '',
    this.customer_name = '',
    this.note = '',
    // this.ship_fee,
    // this.payment_fee,
    // this.total_weight,
    // this.ship_date,
  });

  factory ProductOrderDetail.fromJson(Map<dynamic, dynamic> json) {
    try {
      return ProductOrderDetail(
        payment_method: json["payment_method"],
        ship_method: json["shipping_method"],
        address: json["address"],
        region: json["region"],
        district: json["district"] ?? '',
        regionId: json["regionId"],
        districtId: json["districtId"] ?? '',
        name: json["name"],
        phone: json['phone'] ?? '',
        customer_name: json['customer_name'],
        note: json['note'],
      );
    } catch (e) {
      return ProductOrderDetail();
    }
  }

  toJson() {
    return {
      "payment_method": payment_method,
      'address': address,
      'region': region,
      'district': district,
      'regionId': regionId,
      'districtId': districtId,
      'name': name,
      "phone": phone,
      'customer_name': customer_name,
      'note': note,
    };
  }
}
