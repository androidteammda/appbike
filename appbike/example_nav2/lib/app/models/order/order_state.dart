class OrderState {
  int? status;
  String? message;

  OrderState({this.status, this.message});

  factory OrderState.fromJson(Map<String, dynamic> jsonData) {
    return OrderState(
      status: jsonData['status'],
      message: jsonData['message'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    return data;
  }
}
