class ShipFee {
  int? status;
  String? message;
  Data? data;

  ShipFee({this.status, this.message, this.data});

  factory ShipFee.fromJson(Map<String, dynamic> json) => ShipFee(
    status : json['status'],
    message : json['message'],
    data : json['data'] != null ? new Data.fromJson(json['data']) : null,
  );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class Data {
  int? fee;

  Data({this.fee});

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    fee : json['fee'],
  );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['fee'] = this.fee;
    return data;
  }
}