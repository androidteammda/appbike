class Categories {
  int? status;
  String? message;
  DataCate? data;

  Categories({this.status=0, this.message='', this.data});

  factory Categories.fromJson(Map<String, dynamic> json) => Categories(
        status: json['status'],
        message: json['message'],
        data: json['data'] != null ? new DataCate.fromJson(json['data']) : null,
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class DataCate {
  List<Items>? items;

  DataCate({this.items});

  DataCate.fromJson(Map<String, dynamic> json) {
    if (json['items'] != null) {
      items = <Items>[];
      json['items'].forEach((v) {
        items!.add(new Items.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.items != null) {
      data['items'] = this.items!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Items {
  String? id;
  String? name;
  String? imageUrl;
  List<Children>? children;

  Items({this.id, this.name, this.imageUrl, this.children});

  factory Items.fromJson(Map<String, dynamic> json) => Items(
        id: json['id'],
        name: json['name'],
        imageUrl: json['image_url'],
        children: json["children"] != null
            ? List<Children>.from((json["children"] as Iterable)
                .map((x) => Children.fromJson(x as Map<String, dynamic>)))
            : [],
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['image_url'] = this.imageUrl;
    if (this.children != null) {
      data['children'] = this.children!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Children {
  String? id;
  String? name;
  String? imageUrl;

  Children({this.id, this.name, this.imageUrl});

  factory Children.fromJson(Map<String, dynamic> json) => Children(
        id: json['id'],
        name: json['name'],
        imageUrl: json['image_url'] ?? '',
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['image_url'] = this.imageUrl;
    return data;
  }
}
