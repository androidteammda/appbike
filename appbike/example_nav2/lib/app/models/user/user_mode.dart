class UserMode {
  List<Mode>? mode;

  UserMode({this.mode});

  factory UserMode.fromJson(Map<String, dynamic> json) => UserMode(
      mode: List<Mode>.from((json["Mode"] as Iterable)
          .map((x) => Mode.fromJson(x as Map<String, dynamic>))));

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.mode != null) {
      data['Mode'] = this.mode!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Mode {
  String? id;
  String? title;
  String? sex;
  String? range;
  String? E;
  String? number;
  String? breakfast;
  String? breakfastSnack;
  String? lunch;
  String? afternoonSnack;
  String? dinner;
  String? eveningSnack;
  String? g1;
  String? p1;
  String? r1;
  String? t1;
  String? u1;

  Mode(
      {this.id,
      this.title,
      this.sex,
      this.range,
      this.E,
      this.number,
      this.breakfast,
      this.breakfastSnack,
      this.lunch,
      this.afternoonSnack,
      this.dinner,
      this.eveningSnack,
      this.g1,
      this.p1,
      this.r1,
      this.t1,
      this.u1});

  factory Mode.fromJson(Map<String, dynamic> json) => Mode(
        id: json['id'],
        title: json['title'],
        sex: json['sex'],
        range: json['range'],
        E: json['TTE'],
        number: json['number'],
        breakfast: json['breakfast'],
        breakfastSnack: json['breakfast snack'],
        lunch: json['lunch'],
        afternoonSnack: json['afternoon snack'],
        dinner: json['dinner'],
        eveningSnack: json['evening snack'],
        g1: json['g1'],
        p1: json['p1'],
        r1: json['r1'],
        t1: json['t1'],
        u1: json['u1'],
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['sex'] = this.sex;
    data['range'] = this.range;
    data['TTE'] = this.E;
    data['number'] = this.number;
    data['breakfast'] = this.breakfast;
    data['breakfast snack'] = this.breakfastSnack;
    data['lunch'] = this.lunch;
    data['afternoon snack'] = this.afternoonSnack;
    data['dinner'] = this.dinner;
    data['evening snack'] = this.eveningSnack;
    data['g1'] = this.g1;
    data['p1'] = this.p1;
    data['r1'] = this.r1;
    data['t1'] = this.t1;
    data['u1'] = this.u1;
    return data;
  }
}
