class UserDeviceModel {
  String? id;
  String? username;
  String? display_name;
  String? avatar;
  String? email;
  String? password;
  String? re_password;
  String? phone_number;
  String? token;
  String? birthDay;

  String? createAt;
  String? typeUser;

  String? address;

  String? regionId;
  String? regionName;

  String? districtId;
  String? districtName;

  String? externalToken;

  String? provider;

  String? enable_notification;

  String? balance;

  UserDeviceModel({
    this.id = '',
    this.username = '',
    this.display_name = '',
    this.avatar = '',
    this.email = '',
    this.password = '',
    this.re_password = '',
    this.phone_number = '',
    this.token = '',
    this.typeUser = 'normal',
    this.address = '',
    this.birthDay = '',
    this.externalToken = '',
    this.districtId = '',
    this.districtName = '',
    this.provider = '',
    this.regionId = '',
    this.regionName = '',
    this.balance = '0',
    this.enable_notification = '',
  });

  factory UserDeviceModel.fromJson(Map<dynamic, dynamic> jsonUser) {
    try {
      var jsonData = jsonUser['data'];
      // bool checkVip() {
      //   String type = jsonData["type"];
      //   return type.contains('normal') ? false : true;
      // }

      return UserDeviceModel(
        id: jsonData["id"],
        email: jsonData["email"],
        phone_number: jsonData["phone"],
        avatar: jsonData["avatar"] ?? '',
        display_name: jsonData["name"],
        balance: jsonData["balance"] ?? '0',
        typeUser: jsonData["type"] ?? 'normal',
        birthDay: jsonData['date_of_birth'] ?? '',
        enable_notification: jsonData['setting']['enable_notification'],
      );
    } catch (e) {
      return UserDeviceModel();
    }
  }

  static UserDeviceModel getUserFromLocal(Map<dynamic, dynamic> json) {
    return UserDeviceModel(
      id: json["id"],
      username: json["username"],
      email: json["email"],
      phone_number: json["phone"],
      avatar: json["avatar"] ?? '',
      display_name: json["name"],
      address: json['address'] ?? '',
      token: json['access_token'],
      birthDay: json['birthDay'],
      districtId: json['districtId'],
      districtName: json['districtName'],
      // externalToken: json['externalToken'],
      typeUser: json["typeUser"],
      password: json['password'],
      provider: json['provider'],
      regionId: json['regionId'],
      balance: json['balance'],
      regionName: json['regionName'],
      enable_notification: json['enable_notification'],
    );
  }

  toJson() {
    return {
      "email": email,
      'name': display_name,
      'avatar': avatar,
      'phone': phone_number,
      'username': username,
      "password": password,
      'access_token': token,
      'id': id,
      'address': address,
      'birthDay': birthDay,
      'districtId': districtId,
      'districtName': districtName,
      // 'externalToken': externalToken,
      'typeUser': typeUser,
      'provider': provider,
      'balance': balance,
      'regionId': regionId,
      'regionName': regionName,
    };
  }
}
