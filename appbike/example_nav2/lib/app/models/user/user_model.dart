import 'package:example_nav2/app/models/user/user_mode.dart';
import 'package:example_nav2/app/utils/enum.dart';

class UserModel {
  UserModel({
    this.id,
    this.name,
    this.email,
    this.phoneRef,
    this.phone,
    this.height,
    this.weight,
    this.bmi,
    this.bfp,
    this.TEindex,
    this.activity,
    this.sex,
    this.birthday,
    this.authStatus,
    this.targetMode,
    this.finish,
    this.mode,
    this.data,
  });

  String? id;
  String? name;
  String? email;
  double? phone;
  double? phoneRef;
  double? height;
  double? weight;
  double? bmi;
  double? bfp;
  double? TEindex;
  double? activity;
  bool? sex;
  int? birthday;
  AuthStatus? authStatus;
  int? targetMode;
  bool? finish;
  Mode? mode;
  dynamic? data;

  factory UserModel.fromJson(dynamic json) => UserModel(
      id: json["id"].toString(),
      name: json["name"].toString(),
      email: json["email"].toString(),
      birthday: json["birthday"] ?? 18,
      phoneRef: json["phoneRef"] ?? 0.0,
      phone: json['phone'] ?? 0.0,
      weight: json['weight'] ?? 0.0,
      height: json['height'] ?? 0.0,
      bmi: json['bmi'] ?? 0.0,
      bfp: json['bfp'] ?? 0.0,
      TEindex: json['TEindex'] ?? 0.0,
      activity: json['activity'] ?? 0.0,
      authStatus: json['authStatus'] ?? AuthStatus.DETERMINED,
      targetMode: json['targetMode'] ?? 1,
      finish: json['finish'] ?? false,
      mode: json['mode'] != null ? new Mode.fromJson(json['mode']) : null,
      data: json['data'] ?? '');

  Map<String, dynamic> toMap() => {
        "id": id,
        "name": name,
        "email": email,
        "phoneRef": phoneRef,
        "phone": phone,
        "weight": weight,
        "height": height,
        "bmi": bmi,
        "bfp": bfp,
        "birthday": birthday,
        "TEindex": TEindex,
        "activity": activity,
        'authStatus': authStatus,
        'targetMode': targetMode,
        'finish': finish,
        'mode': this.mode!.toJson(),
        'data': data,
      };

  static get empty => UserModel(
        id: '',
        name: '',
        email: '',
        phone: 0.0,
        height: 0.0,
        weight: 0.0,
        bmi: 0.0,
        bfp: 0.0,
        sex: true,
        birthday: 18,
        finish: false,
        mode: null,
        data: '',
      );
}
