class ProductDetail {
  int? status;
  String? message;
  DataProductDetail? data;

  ProductDetail({this.status, this.message, this.data});

  factory ProductDetail.fromJson(Map<String, dynamic> json) => ProductDetail(
        status: json['status'],
        message: json['message'],
        data: json['data'] != null
            ? new DataProductDetail.fromJson(json['data'])
            : null,
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class DataProductDetail {
  String? id;
  String? name;
  String? description;
  String? price;
  String? price_vip1;
  String? price_vip2;
  String? priceBeforeDiscount;
  String? flashSaleId;
  String? imageMainUrl;
  String? createdDate;
  bool? isHot;
  String? linkWeb;
  List<String>? imageDetailUrls;
  String? soldCount;
  List<Rate>? rate;
  bool? isLiked;

  DataProductDetail(
      {this.id,
      this.name='',
      this.description='',
      this.price='0',
      this.price_vip1='',
      this.price_vip2='',
      this.priceBeforeDiscount='0',
      this.flashSaleId='',
      this.imageMainUrl='',
      this.createdDate='',
      this.isHot,
      this.linkWeb='',
      this.imageDetailUrls,
      this.soldCount,
      this.rate,
      this.isLiked});

  factory DataProductDetail.fromJson(Map<String, dynamic> json) =>
      DataProductDetail(
        id: json['id'],
        name: json['name'],
        description: json['description'],
        price: json['price']??'',
        price_vip1: json['price_vip']??'',
        price_vip2: json['price_vip2']??'',
        priceBeforeDiscount: json['price_before_discount'],
        flashSaleId: '${json['flash_sale_id']}',
        imageMainUrl: json['image_main_url'],
        createdDate: json['created_date'],
        isHot: json['is_hot'],
        linkWeb: json['link_web'],
        imageDetailUrls: json['image_detail_urls'] != null
            ? json['image_detail_urls'].cast<String>()
            : [],
        soldCount: json['sold_count'],
        rate: List<Rate>.from((json["rate"] as Iterable)
            .map((x) => Rate.fromJson(x as Map<String, dynamic>))),
        isLiked: json['is_liked'],
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['description'] = this.description;
    data['price'] = this.price;
    data['price_before_discount'] = this.priceBeforeDiscount;
    data['flash_sale_id'] = this.flashSaleId;
    data['image_main_url'] = this.imageMainUrl;
    data['created_date'] = this.createdDate;
    data['is_hot'] = this.isHot;
    data['link_web'] = this.linkWeb;
    data['image_detail_urls'] = this.imageDetailUrls;
    data['sold_count'] = this.soldCount;
    if (this.rate != null) {
      data['rate'] = this.rate!.map((v) => v.toJson()).toList();
    }
    data['is_liked'] = this.isLiked;
    return data;
  }
}

class Rate {
  String? star;
  String? content;
  String? createdAt;
  String? customerId;
  String? customerName;
  String? customerAvatar;

  Rate({
    this.star,
    this.content,
    this.createdAt,
    this.customerId,
    this.customerName,
    this.customerAvatar,
  });

  factory Rate.fromJson(Map<String, dynamic> json) => Rate(
        star: json['star'],
        content: json['content'],
        createdAt: json['created_at'],
        customerId: json['customer_id'],
        customerName: json['customer_name'],
        customerAvatar: json['customer_avatar'],
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['star'] = this.star;
    data['content'] = this.content;
    data['created_at'] = this.createdAt;
    data['customer_id'] = this.customerId;
    data['customer_name'] = this.customerName;
    data['customer_avatar'] = this.customerAvatar;
    return data;
  }
}
