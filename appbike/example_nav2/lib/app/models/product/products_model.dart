import 'dart:convert';

class Products {
  int? status;
  String? message;
  Data? data;

  Products({this.status=0, this.message='', this.data});

  factory Products.fromJson(Map<String, dynamic> jsonData) {
   try{
     return Products(
       status: jsonData['status'],
       message: jsonData['message'],
       data: (jsonData['data'] != null && jsonData['data'].isNotEmpty)
           ? new Data.fromJson(jsonData['data'])
           : Data(),
     );
   }catch(e){
     return Products();
   }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class Data {
  List<ItemsProduct>? items;
  Pagination? pagination;

  Data({this.items, this.pagination});

  factory Data.fromJson(dynamic json) => Data(
        items: (json["items"] != null && json["items"].isNotEmpty)
            ? List<ItemsProduct>.from((json["items"] as Iterable)
                .map((x) => ItemsProduct.fromJson(x as Map<String, dynamic>)))
            : [],
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.items != null) {
      data['items'] = this.items!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ItemsProduct {
  String? id;
  String? name;
  String? description;
  String? price;
  String? price_vip1;
  String? price_vip2;
  String? priceBeforeDiscount;
  String? flashSaleId;
  String? imageMainUrl;
  String? createdDate;
  bool? isHot;
  List<String>? imageDetailUrls;
  List<Rate>? rate;
  String? soldCount;
  bool? isLiked;
  int? qty = 1;

  ItemsProduct({
    this.id,
    this.name,
    this.description,
    this.price,
    this.price_vip1,
    this.price_vip2,
    this.priceBeforeDiscount,
    this.flashSaleId,
    this.imageMainUrl,
    this.createdDate,
    this.isHot,
    this.imageDetailUrls,
    this.soldCount,
    this.rate,
    this.isLiked,
    this.qty,
  });

  factory ItemsProduct.fromJson(Map<String, dynamic> json) => ItemsProduct(
        id: json['id'],
        name: json['name'],
        description: json['description'],
        price: json['price']??'0',
        price_vip1: json['price_vip']??'0',
        price_vip2: json['price_vip2']??'0',
        priceBeforeDiscount: json['price_before_discount'],
        flashSaleId: '${json['flash_sale_id']}',
        imageMainUrl: json['image_main_url'],
        createdDate: json['created_date'],
        isHot: json['is_hot'],
        imageDetailUrls: json['image_detail_urls'] != null
            ? json['image_detail_urls'].cast<String>()
            : [],
        rate: List<Rate>.from((json["rate"] as Iterable)
            .map((x) => Rate.fromJson(x as Map<String, dynamic>))),
        soldCount: json['sold_count'],
        isLiked: json['is_liked'],
        qty: json['qty'],
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['description'] = this.description;
    data['price'] = this.price;
    data['price_vip'] = this.price_vip1;
    data['price_vip2'] = this.price_vip2;
    data['price_before_discount'] = this.priceBeforeDiscount;
    data['flash_sale_id'] = this.flashSaleId;
    data['image_main_url'] = this.imageMainUrl;
    data['created_date'] = this.createdDate;
    data['is_hot'] = this.isHot;
    data['image_detail_urls'] = this.imageDetailUrls;
    data['sold_count'] = this.soldCount;
    if (this.rate != null) {
      data['rate'] = this.rate!.map((v) => v.toJson()).toList();
    }
    data['is_liked'] = this.isLiked;
    data['qty'] = this.qty;
    return data;
  }
}

class Rate {
  String? star;
  String? total;

  Rate({this.star, this.total});

  factory Rate.fromJson(Map<String, dynamic> json) => Rate(
        star: json['star'],
        total: json['total'],
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['star'] = this.star;
    data['total'] = this.total;
    return data;
  }
}

class Pagination {
  String? total;
  String? perPage;
  String? currentPage;
  int? lastPage;

  Pagination({this.total, this.perPage, this.currentPage, this.lastPage});

  factory Pagination.fromJson(Map<String, dynamic> json) => Pagination(
        total: json['total'],
        perPage: json['per_page'],
        currentPage: json['current_page'],
        lastPage: json['last_page'],
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['total'] = this.total;
    data['per_page'] = this.perPage;
    data['current_page'] = this.currentPage;
    data['last_page'] = this.lastPage;
    return data;
  }
}
