import 'package:example_nav2/app/controller/app_controller.dart';
import 'package:example_nav2/app/data/haibh_repository.dart';
import 'package:example_nav2/app/models/notification/notification_model.dart';
import 'package:example_nav2/app/modules/root/controllers/root_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class NotificationController extends AppController {
  RootController rootController = Get.find<RootController>();
  TextEditingController textEditingController = TextEditingController();
  NotificationEntity _notificationEntity = NotificationEntity();

  NotificationEntity get notificationEntity => _notificationEntity;

  @override
  void onInit() {
    super.onInit();
    print('NotificationController onInit');
    getNotification();
  }

  Future<void> getNotification() async {
    loading.value = true;

    String token = '';
    if (rootController.isLogged) {
      token = rootController.userDeviceModel.token!;
    }
    _notificationEntity = await HaiBHRepository.instance.getNotification(token);
    loading.value = false;
    update();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
