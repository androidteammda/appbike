import 'package:example_nav2/app/modules/notification/controllers/notification_controller.dart';
import 'package:get/get.dart';


class NotificationBinding extends Bindings {
  @override
  void dependencies() {
    print('dependencies NotificationBinding:');
    Get.lazyPut<NotificationController>(() => NotificationController(), fenix: true);
  }
}
