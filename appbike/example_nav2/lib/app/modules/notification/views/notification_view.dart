import 'package:example_nav2/app/models/notification/notification_model.dart';
import 'package:example_nav2/app/utils/app_color.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_view.dart';

import '../controllers/notification_controller.dart';

class NotificationView extends GetView<NotificationController> {
  final TextEditingController controllerText = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    print('build NotificationView:');
    return GetBuilder<NotificationController>(builder: (_) {
      return Scaffold(
        backgroundColor: AppColor.background_app,
        body: controller.loading.value
            ? Container(
                child: Center(
                  child: const SizedBox(
                    height: 50,
                    width: 50,
                    child: CircularProgressIndicator(),
                  ),
                ),
              )
            : controller.notificationEntity.listNotification == null ||
                    controller.notificationEntity.listNotification!.isEmpty
                ? Container(
                    child: Center(
                      child: Text(
                        'Không có thông báo',
                        style: FONT_CONST.REGULAR.copyWith(
                          color: AppColor.blackText,
                          fontSize: 16,
                        ),
                      ),
                    ),
                  )
                : _body(),
      );
    });
  }

  Widget _body() {
    return Container(
      child: ListView.builder(
          padding: const EdgeInsets.symmetric(vertical: 16),
          physics: const BouncingScrollPhysics(),
          // itemCount: 5,
          itemCount: controller.notificationEntity.listNotification!.length,
          itemBuilder: (c, i) {
            NotificationModel model =
                controller.notificationEntity.listNotification![i];
            return _itemNotification(model);
          }),
    );
  }

  Widget _itemNotification(NotificationModel model) {
    return Container(
      color: AppColor.white,
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 12),
      margin: const EdgeInsets.only(bottom: 12),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            '${model.title}',
            style: FONT_CONST.SEMIBOLD.copyWith(
              color: AppColor.blackText,
              fontSize: 16,
            ),
          ),
          const SizedBox(height: 4),
          Html(
            data: '${model.description}',
            style: {
              "body": Style(
                fontSize: FontSize(14),
                fontWeight: FontWeight.w500,
                // fontStyle: FontStyle.normal,
                color: AppColor.blackText,
                maxLines: 2,
                textAlign: TextAlign.start,
                // margin: EdgeInsets.zero,
                padding: EdgeInsets.zero,
              ),
            },
          ),
          // Text(
          //   '${model.description}',
          //   style: FONT_CONST.REGULAR.copyWith(
          //     color: AppColor.grayText,
          //     fontSize: 16,
          //   ),
          //   maxLines: 2,
          // ),
        ],
      ),
    );
  }
}
