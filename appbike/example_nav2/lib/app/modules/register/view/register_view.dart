import 'package:example_nav2/app/modules/register/controller/register_controller.dart';
import 'package:example_nav2/app/modules/root/controllers/root_controller.dart';
import 'package:example_nav2/app/routes/app_pages.dart';
import 'package:example_nav2/app/utils/app_color.dart';
import 'package:example_nav2/app/utils/assets.dart';
import 'package:example_nav2/app/utils/enum.dart';
import 'package:example_nav2/app/utils/text_styles.dart';
import 'package:example_nav2/app/utils/utility.dart';
import 'package:example_nav2/app/widget/button_linear_full_widget_primary.dart';
import 'package:example_nav2/app/widget/customLoader.dart';
import 'package:example_nav2/app/widget/customWidget.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';

import '../../../../main.dart';

class RegisterView extends GetWidget<RegisterController> {
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _mobileController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _confirmController = TextEditingController();

  // final TextEditingController _emailController = TextEditingController();
  final CustomLoader loader = CustomLoader();

  @override
  Widget build(BuildContext context) {
    print('register build:');
    return GetBuilder<RegisterController>(builder: (_) {
      return Scaffold(
        backgroundColor: AppColor.white,
        appBar: AppBar(
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(
              CupertinoIcons.left_chevron,
              color: AppColor.primary,
            ),
          ),
          elevation: 0,
          backgroundColor: Colors.transparent,
          title: Text(
            'Đăng ký',
            style: TextStyles.textNormStyles(AppColor.primary, 20),
          ),
          centerTitle: true,
          // leading: IconButton(
          //   icon: Icon(Icons.arrow_back, color: Colors.white),
          //   onPressed: () => Get.getDelegate()?.toNamed(Routes.BOOK),
          // ),
        ),
        body: GestureDetector(
          onTap: hideKeyboard,
          child: Column(
            children: [
              Expanded(
                child: ListView(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  physics: const BouncingScrollPhysics(),
                  children: [
                    SizedBox(
                      height: 16,
                    ),
                    AssetContentApp.logo_ba_vuong,
                    const SizedBox(
                      height: 32,
                    ),
                    entryField(
                      'Tên của bạn',
                      controller: _nameController,
                    ),
                    entryField('Số điện thoại',
                        controller: _mobileController, isPhone: true),
                    // entryField('Email',
                    //     controller: _emailController, isEmail: false),
                    entryField('Mật khẩu',
                        controller: _passwordController, isPassword: true),
                    entryField('Nhập lại mật khẩu',
                        controller: _confirmController, isPassword: true),
                    const SizedBox(
                      height: 32,
                    ),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.symmetric(horizontal: 16),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    ButtonLinearAppPrimary(
                      title: 'Đăng ký',
                      color: AppColor.button_green,
                      onTap: () {
                        _handleRegister(context);
                      },
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    InkWell(
                      onTap: () {
                        // Get.rootDelegate.popRoute();
                        // Get.rootDelegate.toNamed(Routes.LOGIN);
                        Navigator.pop(context);
                      },
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 4),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'Bạn đã có tài khoản?',
                              style: FONT_CONST.REGULAR.copyWith(
                                color: AppColor.blackText,
                                fontSize: 16,
                              ),
                            ),
                            const SizedBox(
                              width: 4,
                            ),
                            Text(
                              'Đăng nhập',
                              style: FONT_CONST.MEDIUM.copyWith(
                                color: AppColor.button_green,
                                fontSize: 16,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 32,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      );
    });
  }

  void _handleRegister(BuildContext context) {
    RootController controller = Get.find();
    if (_nameController.text.isEmpty) {
      showToast(context, 'Vui lòng nhập tên của bạn');
      return;
    }
    if (_nameController.text.length > 27) {
      showToast(context, 'Độ dài tên không được vượt quá 27 ký tự');
      return;
    }
    if (_mobileController.text.isEmpty ||
            _passwordController.text.isEmpty ||
            _confirmController.text.isEmpty
        // || _emailController.text.isEmpty
        ) {
      showToast(context, 'Vui lòng điền đầy đủ thông tin vào mẫu');
      return;
    } else if (_passwordController.text != _confirmController.text) {
      showToast(context, 'Mật khẩu và mật khẩu xác nhận không khớp');
      return;
    }

    loader.showLoader(context);
    var isValid = Utility.validatePhoneAndEmail(context,
        phone: _mobileController.text,
        // email: _emailController.text,
        password: _passwordController.text);

    if (isValid) {
      controller
          .register(
        context,
        phone: _mobileController.text,
        displayName: _nameController.text,
        pass: _passwordController.text,
        rePass: _confirmController.text,
        username: _nameController.text,
        // email: _emailController.text
      )
          .whenComplete(() {
        if (controller.authStatus.value == AuthStatus.LOGGED_IN) {
          loader.hideLoader();
          Get.rootDelegate.toNamed(Routes.ROOT);
          print('Đăng ký thành công');
          // Future.delayed(const Duration(milliseconds: 300), () {
          //   controller.isRegisterBook
          //       ? Get.getDelegate()?.toNamed(Routes.BOOK)
          //       : Get.getDelegate()?.toNamed(Routes.PROFILE);
          // });
        } else {
          loader.hideLoader();
          showToast(context, 'Đăng ký thất bại');
        }
      });
    } else {
      loader.hideLoader();
    }
  }
}
