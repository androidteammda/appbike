import 'package:example_nav2/app/models/categories/categories.dart';
import 'package:example_nav2/app/modules/home/controllers/home_controller.dart';
import 'package:example_nav2/app/modules/menu/controllers/menu_controller.dart';
import 'package:example_nav2/app/modules/root/controllers/root_controller.dart';
import 'package:example_nav2/app/routes/app_pages.dart';
import 'package:example_nav2/app/utils/app_color.dart';
import 'package:example_nav2/app/utils/assets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

class DrawerMenuView extends GetView {
  DrawerMenuView();

  @override
  Widget build(BuildContext context) {
    final double statusbarHeight = MediaQuery.of(context).padding.top;
    return Drawer(
      child: Stack(
        children: [
          Positioned(
            bottom: 0,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 24),
              child: AssetContentApp.logo_bag,
            ),
          ),
          Container(
            child: ListView(
              padding: const EdgeInsets.all(0),
              physics: const BouncingScrollPhysics(),
              children: [
                SizedBox(
                  height: statusbarHeight,
                ),
                SizedBox(height: 16),
                SizedBox(child: AssetContentApp.logo_ba_vuong),
                SizedBox(height: 16),
                // _itemMenu('Honda', false),
                // _itemMenu('Yamaha', true),
                // _itemMenu('Piaggio', false),
                // _itemMenu('Sym', false),
                _listCate(),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _listCate() {
    HomeController homeController = Get.find();
    return (homeController.categories == null ||
            homeController.categories.data == null ||
            homeController.categories.data!.items == null)
        ? const SizedBox()
        : ListView.builder(
            physics: const BouncingScrollPhysics(),
            shrinkWrap: true,
            itemCount: homeController.categories.data!.items!.length,
            itemBuilder: (c, i) {
              Items item = homeController.categories.data!.items![i];
              return _itemMenuList(item,i);
            });
  }

  Widget _itemMenuList(Items item,int i) {
    return InkWell(
      onTap: () {
        Get.rootDelegate.toNamed(Routes.CATEGORIES,arguments: i);
        print('_itemSetting');
      },
      child: Container(
        padding: const EdgeInsets.only(
          left: 16,
          right: 16,
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            const SizedBox(
              height: 16,
            ),
            Row(
              children: [
                Expanded(
                  child: Container(
                    margin: const EdgeInsets.only(
                      left: 16,
                    ),
                    child: Text(
                      '${item.name}',
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: FONT_CONST.REGULAR.copyWith(
                        fontSize: 20,
                        color: AppColor.blackText,
                      ),
                    ),
                  ),
                ),
                // isChoose
                //     ? Icon(
                //   CupertinoIcons.arrowtriangle_right_fill,
                //   color: AppColor.white,
                //   size: 12,
                // )
                //     :
                // const SizedBox(),
              ],
            ),
            const SizedBox(
              height: 16,
            ),
            Divider(
              height: 1,
            ),
          ],
        ),
      ),
    );
  }

  Widget _itemMenu(String title, bool isChoose) {
    return InkWell(
      onTap: () {
        Get.rootDelegate.toNamed(Routes.CATEGORIES);
        print('_itemSetting');
      },
      child: Container(
        color: isChoose ? AppColor.primary : AppColor.white,
        padding: const EdgeInsets.only(
          left: 16,
          right: 16,
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            const SizedBox(
              height: 16,
            ),
            Row(
              children: [
                Expanded(
                  child: Container(
                    margin: const EdgeInsets.only(
                      left: 16,
                    ),
                    child: Text(
                      '$title',
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: FONT_CONST.REGULAR.copyWith(
                        fontSize: 20,
                        color: isChoose ? AppColor.white : AppColor.blackText,
                      ),
                    ),
                  ),
                ),
                isChoose
                    ? Icon(
                        CupertinoIcons.arrowtriangle_right_fill,
                        color: AppColor.white,
                        size: 12,
                      )
                    : const SizedBox(),
              ],
            ),
            const SizedBox(
              height: 16,
            ),
            Divider(
              height: 1,
            ),
          ],
        ),
      ),
    );
  }
}
