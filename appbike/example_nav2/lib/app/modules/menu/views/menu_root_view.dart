import 'package:example_nav2/app/modules/menu/controllers/menu_controller.dart';
import 'package:example_nav2/app/utils/app_color.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

class MenuRootView extends GetView<MenuController> {
  var viewIntro;

  MenuRootView();

  @override
  Widget build(BuildContext context) {
    return GetBuilder<MenuController>(builder: (_) {
      return Container(
        child: Scaffold(
          backgroundColor: AppColor.primary,
        ),
      );
    });
  }
}
