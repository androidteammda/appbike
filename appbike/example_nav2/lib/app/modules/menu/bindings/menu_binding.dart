import 'package:example_nav2/app/modules/menu/controllers/menu_controller.dart';
import 'package:get/get.dart';

class MenuBinding extends Bindings{
  @override
  void dependencies() {
    // TODO: implement dependencies
    Get.lazyPut<MenuController>(() => MenuController(),
    );
  }
}