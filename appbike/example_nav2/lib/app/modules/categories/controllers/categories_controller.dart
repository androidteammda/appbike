import 'dart:convert';

import 'package:example_nav2/app/data/repository.dart';
import 'package:example_nav2/app/models/categories/categories.dart';
import 'package:example_nav2/app/models/product/products_model.dart';
import 'package:example_nav2/app/modules/root/controllers/root_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CategoriesController extends GetxController
    with SingleGetTickerProviderMixin {
  RootController rootController = Get.find();
  var rootTabs = <Tab>[].obs;
  var childrenTabs = <Tab>[].obs;
  var items = <Items>[].obs;
  var childrenItems = <Children>[].obs;
  late TabController tabRootController;
  var tabChildrenController;
  var listProduct = <ItemsProduct>[].obs;
  var loading = false.obs;
  int positionItem = 0;

  int positionCate = 0;

  int page = 1;
  bool _isLoadingMore = false;

  bool get isLoadingMore => _isLoadingMore;

  bool _loadMoreReady = false;

  bool get loadMoreReady => _loadMoreReady;

  int _count = 0;

  @override
  void onInit() {
    super.onInit();
    positionCate = Get.rootDelegate.arguments();
    getCategories();
  }

  Future<void> getCategories() async {
    var response = await MyRepository.instance.getCategories(1);
    Categories? categories = Categories.fromJson(json.decode(response));
    items.value = categories.data!.items!;
    setRootTabs(categories);
  }

  void setRootTabs(Categories categories) {
    categories.data!.items!.forEach((item) {
      Tab tab = new Tab(text: item.name ?? 'tab');
      rootTabs.value.add(tab);
    });
    tabRootController =
        new TabController(length: rootTabs.value.length, vsync: this);
    tabRootController.animateTo(positionCate);
    update();
    setChildrenTabs(positionCate);
  }

  void setChildrenTabs(int index) async {
    loading.value = true;
    positionItem = index;
    childrenTabs.value.clear();
    Items item = items.value[index];
    item.children!.forEach((child) async {
      Tab tab = new Tab(
        text: child.name ?? 'tab2',
      );
      childrenTabs.value.add(tab);
    });
    childrenItems.value = item.children!;
    tabChildrenController = null;
    tabChildrenController =
        new TabController(length: childrenItems.value.length, vsync: this);
    update();
    getListProductByCategory(0);
  }

  Future<void> getListProductByCategory(int index) async {
    String category_id = '';
    if (childrenItems.value.isEmpty) {
      category_id = '${items.value[positionItem].id}';
    } else {
      category_id = childrenItems.value[index].id!;
    }
    page = 1;
    _count = index;
    var response = await MyRepository.instance
        .getListProductByCategory(category_id, 50, 1, '-price');
    Products? products = Products.fromJson(json.decode(response));
    if (products.data == null ||
        products.data!.items == null ||
        products.data!.items!.isEmpty) {
      listProduct.value = [];
      _loadMoreReady = false;
    } else {
      if (rootController.isLogged) {
        if (rootController.userDeviceModel.typeUser!.contains('vip1')) {
          for (int i = 0; i < products.data!.items!.length; i++) {
            if (int.parse(products.data!.items![i].price_vip1!) > 0) {
              listProduct.value.add(products.data!.items![i]);
            }
          }
          page = ++page;
          _loadMoreReady = true;
        } else if (rootController.userDeviceModel.typeUser!.contains('vip2')) {
          for (int i = 0; i < products.data!.items!.length; i++) {
            if (int.parse(products.data!.items![i].price_vip2!) > 0) {
              listProduct.value.add(products.data!.items![i]);
            }
          }
          page = ++page;
          _loadMoreReady = true;
        } else {
          listProduct.value = products.data!.items!;
          page = ++page;
          _loadMoreReady = true;
        }
      } else {
        listProduct.value = products.data!.items!;
        page = ++page;
        _loadMoreReady = true;
      }
    }
    loading.value = false;
    update();
  }

  Future<void> getListProductByCategoryMore() async {
    _isLoadingMore = true;
    update();
    String category_id = '';
    if (childrenItems.value.isEmpty) {
      category_id = '${items.value[positionItem].id}';
    } else {
      category_id = childrenItems.value[_count].id!;
    }
    var response = await MyRepository.instance
        .getListProductByCategory(category_id, 50, page, '-price');
    Products? products = Products.fromJson(json.decode(response));
    // products.data == null
    //     ? listProduct.value = []
    //     : listProduct.value = products.data!.items!;

    if (products.data == null ||
        products.data!.items == null ||
        products.data!.items!.isEmpty) {
      // listProduct.value = [];
      _loadMoreReady = false;
    } else {
      if (rootController.isLogged) {
        if (rootController.userDeviceModel.typeUser!.contains('vip1')) {
          for (int i = 0; i < products.data!.items!.length; i++) {
            if (int.parse(products.data!.items![i].price_vip1!) > 0 &&
                !listProduct.toList().contains(products.data!.items![i])) {
              listProduct.value.add(products.data!.items![i]);
            }
          }
          page = ++page;
          _loadMoreReady = true;
        } else if (rootController.userDeviceModel.typeUser!.contains('vip2')) {
          for (int i = 0; i < products.data!.items!.length; i++) {
            if (int.parse(products.data!.items![i].price_vip2!) > 0 &&
                !listProduct.toList().contains(products.data!.items![i])) {
              listProduct.value.add(products.data!.items![i]);
            }
          }
          page = ++page;
          _loadMoreReady = true;
        } else {
          products.data!.items!.forEach((element) {
            if (!listProduct.toList().contains(element)) {
              listProduct.add(element);
            }
          });
          page = ++page;
          _loadMoreReady = true;
        }
      } else {
        products.data!.items!.forEach((element) {
          if (!listProduct.toList().contains(element)) {
            listProduct.add(element);
          }
        });
        page = ++page;
        _loadMoreReady = true;
      }
    }
    _isLoadingMore = false;
    update();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
