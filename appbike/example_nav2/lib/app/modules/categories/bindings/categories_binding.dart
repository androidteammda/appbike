
import 'package:example_nav2/app/modules/categories/controllers/categories_controller.dart';
import 'package:get/get.dart';

class CategoriesBinding extends Bindings{
  @override
  void dependencies() {
    Get.put<CategoriesController>(CategoriesController(), permanent: false);
  }

}