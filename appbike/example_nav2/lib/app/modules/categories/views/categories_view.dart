import 'package:example_nav2/app/models/product/products_model.dart';
import 'package:example_nav2/app/modules/categories/controllers/categories_controller.dart';
import 'package:example_nav2/app/utils/app_color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../widget/component_item_product.dart';
import '../../../widget/customWidget.dart';

class CategoriesView extends GetView<CategoriesController> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<CategoriesController>(
      builder: (_) => Scaffold(
        appBar: AppBar(
          toolbarHeight: 56,
          elevation: 0,
          backgroundColor: AppColor.primary_second,
          leading: Builder(
            builder: (BuildContext context) {
              return IconButton(
                icon: Icon(
                  Icons.arrow_back_ios,
                  color: AppColor.white,
                ),
                onPressed: () {
                  Get.back();
                },
                tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
              );
            },
          ),
          // leading: IconButton(
          //   padding: const EdgeInsets.all(0),
          //   onPressed: () {},
          //   icon: AssetContentApp.ic_menu,
          // ),
          title: Text(
            'Category',
            style: FONT_CONST.REGULAR.copyWith(
              color: AppColor.white,
              fontSize: 24,
            ),
          ),

          centerTitle: true,
        ),
        backgroundColor: AppColor.gray_bg_main,
        body: _body(),
      ),
    );
  }

  Widget _body() {
    return controller.loading.value
        ? loadingView()
        : controller.rootTabs.value.length == 0
            ? emptyView()
            : SingleChildScrollView(
                physics: const BouncingScrollPhysics(),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  // shrinkWrap: true,
                  children: [
                    TabBar(
                      indicatorSize: TabBarIndicatorSize.label,
                      controller: controller.tabRootController,
                      isScrollable: true,
                      unselectedLabelColor: AppColor.black,
                      labelColor: AppColor.primary_second,
                      indicatorWeight: 4,
                      indicatorColor: AppColor.primary_second,
                      labelStyle:
                          TextStyle(fontSize: 14, color: AppColor.black),
                      onTap: (value) => {controller.setChildrenTabs(value)},
                      tabs: controller.rootTabs.value,
                    ),
                    Divider(thickness: 1),
                    controller.childrenTabs.value.isEmpty
                        ? const SizedBox()
                        : SizedBox(
                            child: TabBar(
                            tabs: controller.childrenTabs.value,
                            controller: controller.tabChildrenController,
                            indicatorSize: TabBarIndicatorSize.label,
                            isScrollable: true,
                            unselectedLabelColor: AppColor.black,
                            labelColor: AppColor.primary_second,
                            indicatorColor: AppColor.primary_second,
                            labelStyle:
                                TextStyle(fontSize: 14, color: AppColor.black),
                            onTap: (value) {
                              controller.getListProductByCategory(value);
                            },
                          )),
                    _listProduct(),
                    controller.loadMoreReady
                        ? Container(
                            child: controller.isLoadingMore
                                ? loadingView()
                                : Center(
                                  child: TextButton(
                                      child: Text(
                                        'Xem thêm',
                                        style: FONT_CONST.REGULAR.copyWith(
                                          color: AppColor.blue,
                                          fontSize: 14,
                                        ),
                                      ),
                                      onPressed: () {
                                        controller.getListProductByCategoryMore();
                                      },
                                    ),
                                ),
                          )
                        : const SizedBox()
                  ],
                ),
              );
  }

  Widget _listProduct() {
    return controller.listProduct.value.length == 0
        ? emptyView()
        : Container(
            margin: EdgeInsets.only(top: 16, left: 16, bottom: 16),
            child: GridView.builder(
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  crossAxisSpacing: 6,
                  mainAxisSpacing: 6,
                  childAspectRatio: 0.7,
                ),
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                padding: const EdgeInsets.all(0),
                physics: const BouncingScrollPhysics(),
                itemCount: controller.listProduct.value.length,
                itemBuilder: (BuildContext context, int index) {
                  ItemsProduct itemsProduct =
                      controller.listProduct.value[index];
                  return Container(
                      margin: EdgeInsets.only(bottom: 16),
                      child: ItemProductView(itemsProduct: itemsProduct));
                }));
  }
}
