import 'package:example_nav2/app/modules/cart/controllers/cart_controller.dart';
import 'package:get/get.dart';

class CartBinding extends Bindings {
  @override
  void dependencies() {
    print('BookBinding');
    Get.put<CartController>(CartController());
  }
}
