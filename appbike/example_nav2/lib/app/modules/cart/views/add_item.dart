import 'package:example_nav2/app/models/product/products_model.dart';
import 'package:example_nav2/app/modules/cart/controllers/cart_controller.dart';
import 'package:example_nav2/app/utils/app_color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

class AddItemWidget extends StatelessWidget {
  final ItemsProduct itemsProduct;

  const AddItemWidget({
    required this.itemsProduct,
  });

  @override
  Widget build(BuildContext context) {
    CartController cartController = Get.find<CartController>();
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        IconButton(
          onPressed: () {
            cartController.countMinus(itemsProduct);
          },
          padding: const EdgeInsets.all(0),
          icon: Icon(
            Icons.remove,
            color: AppColor.normalGrey,
          ),
        ),
        const SizedBox(
          width: 2,
        ),
        Container(
          padding: const EdgeInsets.symmetric(vertical: 6, horizontal: 10),
          decoration: BoxDecoration(
            border: Border.all(
              color: Colors.grey,
              width: 1,
            ),
          ),
          child: Text(
            '${itemsProduct.qty ?? 1}',
            style: FONT_CONST.REGULAR.copyWith(
              color: AppColor.blackText,
              fontSize: 14,
            ),
          ),
        ),
        const SizedBox(
          width: 2,
        ),
        IconButton(
          padding: const EdgeInsets.all(0),
          onPressed: () {
            cartController.countPlus(itemsProduct);
          },
          icon: Icon(
            CupertinoIcons.add,
            color: AppColor.normalGrey,
          ),
        ),
      ],
    );
  }
}
