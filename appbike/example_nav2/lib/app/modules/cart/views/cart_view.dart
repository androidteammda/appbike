import 'package:example_nav2/app/models/product/products_model.dart';
import 'package:example_nav2/app/modules/cart/controllers/cart_controller.dart';
import 'package:example_nav2/app/modules/root/controllers/root_controller.dart';
import 'package:example_nav2/app/routes/app_pages.dart';
import 'package:example_nav2/app/utils/app_color.dart';
import 'package:example_nav2/app/utils/assets.dart';
import 'package:example_nav2/app/utils/utility.dart';
import 'package:example_nav2/app/widget/button_linear_full_widget.dart';
import 'package:example_nav2/app/widget/component_item_product.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_view.dart';

import '../../../../main.dart';

class CartView extends GetView<CartController> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<CartController>(builder: (_) {
      return Scaffold(
        backgroundColor: AppColor.white,
        appBar: AppBar(
          backgroundColor: AppColor.white,
          toolbarHeight: 56,
          title: Text(
            'Giỏ hàng',
            style: FONT_CONST.REGULAR.copyWith(
              color: AppColor.blackText,
              fontSize: 22,
            ),
          ),
          leading: IconButton(
            onPressed: () {
              Get.back();
            },
            icon: Icon(
              Icons.arrow_back_ios_outlined,
              color: AppColor.primary,
            ),
          ),
          centerTitle: true,
          actions: [
            IconButton(
              padding: const EdgeInsets.all(0),
              onPressed: null,
              icon: AssetContentApp.ic_cart(
                  controller.rootController.countProductOrder.value.toString()),
            ),
          ],
        ),
        body: Container(
          // padding: const EdgeInsets.all(12),
          child: Column(
            children: [
              Expanded(
                child: controller.listProductOrder.value.length == 0
                    ? SizedBox()
                    : ListView.builder(
                        padding: const EdgeInsets.symmetric(
                            vertical: 12, horizontal: 0),
                        physics: const BouncingScrollPhysics(),
                        itemCount: controller.listProductOrder.value.length,
                        itemBuilder: (c, i) {
                          ItemsProduct itemProduct =
                              controller.listProductOrder.value[i];
                          return ItemProductCartView(
                            itemsProduct: itemProduct,
                          );
                        },
                      ),
              ),
              _bottomButton(context),
            ],
          ),
        ),
      );
    });
  }

  Widget _bottomButton(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        const SizedBox(
          height: 16,
        ),
        Container(
          margin: const EdgeInsets.symmetric(horizontal: 16),
          child: Row(
            children: [
              Expanded(
                child: Text(
                  'Tổng tiền:',
                  style: FONT_CONST.SEMIBOLD.copyWith(
                    fontSize: 16,
                    color: AppColor.blackText,
                  ),
                ),
              ),
              Text(
                '${Utility.getValuePrice(controller.sumPrice.toString())}',
                style: FONT_CONST.SEMIBOLD.copyWith(
                  fontSize: 20,
                  color: AppColor.primary,
                ),
              ),
            ],
          ),
        ),
        const SizedBox(
          height: 16,
        ),
        // Container(
        //     margin: const EdgeInsets.symmetric(horizontal: 16),
        //     child: inputFormCode(
        //       'Nhập mã khuyến mãi',
        //     )),
        ButtonLinearApp(
            title: 'Mua ngay',
            onTap: () {
              actionBuy(context);
            }),
      ],
    );
  }

  void actionBuy(BuildContext context) {
    RootController rootController = Get.find();
    if (rootController.isLogged) {
      Get.rootDelegate.toNamed(Routes.BOOK);
    } else {
      showToast(context, 'Bạn cần đăng nhập để mua.');
    }
  }
}
