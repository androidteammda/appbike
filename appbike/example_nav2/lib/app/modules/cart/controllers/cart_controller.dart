import 'dart:convert';
import 'package:example_nav2/app/data/repository.dart';
import 'package:example_nav2/app/models/product/products_model.dart';
import 'package:example_nav2/app/modules/root/controllers/root_controller.dart';
import 'package:example_nav2/app/utils/utility.dart';
import 'package:get/get.dart';

class CartController extends GetxController with SingleGetTickerProviderMixin {
  RootController rootController = Get.find<RootController>();
  var listProductOrder = <ItemsProduct>[].obs;
  var sumPrice = 0.0.obs;
  var countProduct = 0.obs;

  @override
  void onInit() {
    super.onInit();
    print('CartController :: onInit()');
    getListProducts();
  }

  Future<void> getListProducts() async {
    var stringJson = await MyRepository.instance.getListOrderLocalStorage();
    if (stringJson.toString().isNotEmpty) {
      List<dynamic> list = json.decode(stringJson);
      listProductOrder.value =
          list.map((item) => ItemsProduct.fromJson(item)).toList();
    }
    rootController.setCountProductOrder(listProductOrder.value.length);
    calculatorSumPrice();
    update();
  }

  void countPlus(ItemsProduct itemsProduct) {
    itemsProduct.qty = (itemsProduct.qty ?? 1) + 1;
    listProductOrder.value[listProductOrder.value.indexWhere((element) =>
        element.id!.toUpperCase().contains(itemsProduct.id!))] = itemsProduct;
    MyRepository.instance
        .saveProductOrderStorage(listProductOrder.value)
        .whenComplete(() => {
              calculatorSumPrice(),
              update(),
            });
  }

  void countMinus(ItemsProduct itemsProduct) {
    itemsProduct.qty = (itemsProduct.qty ?? 1) - 1;
    if (itemsProduct.qty == 0) {
      listProductOrder.value.remove(itemsProduct);
      rootController.setCountProductOrder(rootController.countProductOrder.value -1);
      update();
    } else {
      listProductOrder.value[listProductOrder.value.indexWhere((element) =>
          element.id!.toUpperCase().contains(itemsProduct.id!))] = itemsProduct;
    }
    MyRepository.instance
        .saveProductOrderStorage(listProductOrder.value)
        .whenComplete(() => {
              calculatorSumPrice(),
              update(),
            });
  }



  void calculatorSumPrice() {
    RootController rootController = Get.find();
    sumPrice.value = 0.0;
    listProductOrder.value.forEach((itemsProduct) {
      String price = Utility.getPrice(rootController, itemsProduct);
      sumPrice.value = sumPrice.value +
          (itemsProduct.qty ?? 1) * double.parse(price);
    });
    update();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
