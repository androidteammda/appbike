import 'package:example_nav2/app/models/product/products_model.dart';
import 'package:example_nav2/app/modules/favorites/controllers/favorite_controller.dart';
import 'package:example_nav2/app/utils/app_color.dart';
import 'package:example_nav2/app/utils/enum.dart';
import 'package:example_nav2/app/widget/component_item_product.dart';
import 'package:example_nav2/app/widget/customWidget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_view.dart';

class FavoriteView extends GetView<FavoriteController> {
  @override
  Widget build(BuildContext context) {
    if (controller.rootController.authStatus.value == AuthStatus.LOGGED_IN)
      controller.getListFavorite();
    return GetBuilder<FavoriteController>(builder: (_) {
      return Scaffold(
        backgroundColor: AppColor.background_app,
        body: !controller.rootController.isLogged
            ? notLoginView()
            : controller.loading.value
                ? loadingView()
                : controller.productModel.data == null ||
                        controller.productModel.data!.items == null
                    ? emptyView()
                    : Container(
                        padding: const EdgeInsets.all(16),
                        child: Column(
                          children: [
                            Expanded(
                                child: ListView.builder(
                              padding: const EdgeInsets.all(0),
                              physics: const BouncingScrollPhysics(),
                              itemCount:
                                  controller.productModel.data!.items!.length,
                              itemBuilder: (c, i) {
                                ItemsProduct item =
                                    controller.productModel.data!.items![i];
                                return ItemProductFavoriteView(
                                    itemsProduct: item);
                              },
                            )),
                            controller.loadMoreReady
                                ? Container(
                                    child: controller.isLoadingMore
                                        ? loadingView()
                                        : TextButton(
                                            child: Text(
                                              'Xem thêm',
                                              style:
                                                  FONT_CONST.REGULAR.copyWith(
                                                color: AppColor.blue,
                                                fontSize: 14,
                                              ),
                                            ),
                                            onPressed: () {
                                              controller.getListFavoriteMore();
                                            },
                                          ),
                                  )
                                : const SizedBox()
                          ],
                        ),
                      ),
      );
    });
  }
}
