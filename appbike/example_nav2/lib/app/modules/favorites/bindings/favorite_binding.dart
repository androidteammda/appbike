import 'package:get/get.dart';
import '../controllers/favorite_controller.dart';

class FavoriteBinding extends Bindings {
  @override
  void dependencies() {
    print('BookBinding');
    Get.put<FavoriteController>(FavoriteController(), permanent: true);
  }
}
