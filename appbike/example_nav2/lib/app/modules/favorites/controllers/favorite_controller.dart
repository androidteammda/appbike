import 'package:example_nav2/app/controller/app_controller.dart';
import 'package:example_nav2/app/data/haibh_repository.dart';
import 'package:example_nav2/app/models/product/product_detail_model.dart';
import 'package:example_nav2/app/models/product/products_model.dart';
import 'package:example_nav2/app/modules/root/controllers/root_controller.dart';
import 'package:example_nav2/app/utils/enum.dart';
import 'package:get/get.dart';

class FavoriteController extends AppController
    with SingleGetTickerProviderMixin {
  RootController rootController = Get.find<RootController>();
  Products _productModel = Products();

  Products get productModel => _productModel;

  int page = 1;
  bool _isLoadingMore = false;

  bool get isLoadingMore => _isLoadingMore;

  bool _loadMoreReady = false;

  bool get loadMoreReady => _loadMoreReady;

  @override
  void onInit() {
    super.onInit();
    print('onInit');
  }

  @override
  void onReady() {
    super.onReady();
    print('onReady');
  }

  @override
  void onClose() {
    print('onClose');
    super.onClose();
  }

  Future<void> getListFavorite() async {
    try {
      page = 1;
      loading.value = true;
      Products _product = await HaiBHRepository.instance.getListFavorite(
          token: rootController.userDeviceModel.token!, countPage: page);
      if (_product != null &&
          _product.data != null &&
          _product.data!.items != null &&
          _product.data!.items!.isNotEmpty) {
        _productModel = _product;
        page = ++page;
        _loadMoreReady = true;
      } else {
        _loadMoreReady = false;
      }
    } catch (e) {}
    loading.value = false;
    update();
  }

  Future<void> getListFavoriteMore() async {
    try {
      _isLoadingMore = true;
      update();
      Products _product = await HaiBHRepository.instance.getListFavorite(
          token: rootController.userDeviceModel.token!, countPage: page);
      if (_product != null &&
          _product.data != null &&
          _product.data!.items != null &&
          _product.data!.items!.isNotEmpty) {
        page = ++page;
        _loadMoreReady = true;
        _product.data!.items!.forEach((element) {
          if (!_productModel.data!.items!.contains(element)) {
            _productModel.data!.items!.add(element);
          }
        });
      } else {
        _loadMoreReady = false;
      }
    } catch (e) {}

    _isLoadingMore = false;
    update();
  }
}
