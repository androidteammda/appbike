import 'dart:async';

import 'package:example_nav2/app/models/product/products_model.dart';
import 'package:example_nav2/app/modules/home/controllers/home_controller.dart';
import 'package:example_nav2/app/routes/app_pages.dart';
import 'package:example_nav2/app/utils/app_color.dart';
import 'package:example_nav2/app/utils/assets.dart';
import 'package:example_nav2/app/widget/component_item_product.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

class FlashSaleWidget extends GetView<HomeController> {
  HomeController homeController = Get.find<HomeController>();

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      builder: (_) => Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetContentApp.flash_sale,
            fit: BoxFit.fill,
          ),
          // color: AppColor.white,
        ),
        padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              children: [
                Expanded(
                  child: Text(
                    'Flash sale',
                    style: FONT_CONST.SEMIBOLD.copyWith(
                      color: AppColor.blackText,
                      fontSize: 20,
                    ),
                    maxLines: 1,
                  ),
                ),
                TextButton(
                  onPressed: () {
                    Get.rootDelegate.toNamed(Routes.FLASHSALES);
                  },
                  child: Text(
                    'Xem thêm',
                    style: FONT_CONST.REGULAR.copyWith(
                      color: AppColor.primary,
                      fontSize: 12,
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 4,
            ),
            _listFlashSale(),
            const SizedBox(
              height: 16,
            ),
            CountTimeFlashSaleView(startTime: homeController.timeCountSale),
            const SizedBox(
              height: 12,
            ),
          ],
        ),
      ),
    );
  }

  Widget _listFlashSale() {
    return homeController.listProductSales.value == 0
        ? SizedBox()
        : SizedBox(
            height: 240,
            child: ListView.builder(
              shrinkWrap: true,
              padding: const EdgeInsets.all(0),
              itemCount: homeController.listProductSales.value.length,
              physics: const BouncingScrollPhysics(),
              scrollDirection: Axis.horizontal,
              itemBuilder: (c, i) {
                ItemsProduct itemsProduct =
                    homeController.listProductSales.value[i];
                return ItemProductView(
                  itemsProduct: itemsProduct,
                );
              },
            ),
          );
  }
}

class CountTimeFlashSaleView extends StatefulWidget {
  final int startTime;

  const CountTimeFlashSaleView({Key? key, required this.startTime})
      : super(key: key);

  _CountTimeFlashSaleViewState createState() => _CountTimeFlashSaleViewState();
}

class _CountTimeFlashSaleViewState extends State<CountTimeFlashSaleView> {
  int _start = 0;
  late Timer _timer;
  List<String> _countDate = ['0', '0', '0', '0'];

  void startTimer() {
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) {
        if (_start == 0) {
          // controller.clearRequestOtp();
          setState(() {
            timer.cancel();
          });
        } else {
          setState(() {
            _start--;
            getCountDate(_start);
          });
        }
      },
    );
  }

  void getCountDate(int difDays) {
    int day = 0;
    int hour = 0;
    int minute = 0;
    int seconds = 0;
    if (difDays < 60) {
      seconds = difDays;
    } else if (difDays >= 60 && difDays < 3600) {
      minute = difDays ~/ 60;
      int reSeconds = difDays - minute * 60;
      seconds = reSeconds;
    } else if (difDays >= 3600 && difDays < 86400) {
      hour = difDays ~/ 3600;
      int reSeconds = difDays - hour * 3600;
      minute = reSeconds ~/ 60;
      int reSeconds2 = reSeconds - minute * 60;
      seconds = reSeconds2;
      // result = '${difMinutes ~/ 1440} Ngày trước';
    } else {
      day = difDays ~/ 86400;
      int reSeconds = difDays - day * 86400;

      hour = reSeconds ~/ 3600;
      int reSeconds2 = reSeconds - hour * 3600;

      minute = reSeconds2 ~/ 60;

      int reSeconds3 = reSeconds2 - minute * 60;
      seconds = reSeconds3;
      // result = formattedDate;
    }
    _countDate = ['0', '0', '0', '0'];
    _countDate.insert(0, day < 10 ? '0$day' : '$day');
    _countDate.insert(1, hour < 10 ? '0$hour' : '$hour');
    _countDate.insert(2, minute < 10 ? '0$minute' : '$minute');
    _countDate.insert(3, seconds < 10 ? '0$seconds' : '$seconds');
  }

  @override
  void initState() {
    super.initState();
    _start = widget.startTime;
    startTimer();
  }

  @override
  void dispose() {
    if (_timer != null) _timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return RichText(
      textAlign: TextAlign.start,
      maxLines: 1,
      text: TextSpan(
        text: 'Còn ',
        style: FONT_CONST.REGULAR.copyWith(
          color: AppColor.blackText,
          fontSize: 20,
        ),
        children: <TextSpan>[
          TextSpan(
            text: '${_countDate[0]}',
            style: FONT_CONST.REGULAR.copyWith(
              color: AppColor.primary_second,
              fontSize: 20,
            ),
          ),
          TextSpan(
            text: ' Ngày ',
            style: FONT_CONST.REGULAR.copyWith(
              color: AppColor.blackText,
              fontSize: 20,
            ),
          ),
          TextSpan(
            text: '${_countDate[1]}',
            style: FONT_CONST.REGULAR.copyWith(
              color: AppColor.primary_second,
              fontSize: 20,
            ),
          ),
          TextSpan(
            text: ' Giờ ',
            style: FONT_CONST.REGULAR.copyWith(
              color: AppColor.blackText,
              fontSize: 20,
            ),
          ),
          TextSpan(
            text: '${_countDate[2]}',
            style: FONT_CONST.REGULAR.copyWith(
              color: AppColor.primary_second,
              fontSize: 20,
            ),
          ),
          TextSpan(
            text: ' Phút ',
            style: FONT_CONST.REGULAR.copyWith(
              color: AppColor.blackText,
              fontSize: 20,
            ),
          ),
          TextSpan(
            text: '${_countDate[3]}',
            style: FONT_CONST.REGULAR.copyWith(
              color: AppColor.primary_second,
              fontSize: 20,
            ),
          ),
          TextSpan(
            text: ' Giây ',
            style: FONT_CONST.REGULAR.copyWith(
              color: AppColor.blackText,
              fontSize: 20,
            ),
          ),
        ],
      ),
    );
  }
}
