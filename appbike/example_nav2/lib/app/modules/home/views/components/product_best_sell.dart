import 'package:example_nav2/app/modules/home/controllers/home_controller.dart';
import 'package:example_nav2/app/routes/app_pages.dart';
import 'package:example_nav2/app/utils/app_color.dart';
import 'package:example_nav2/app/widget/component_tile_grid_product.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ProductBestSellWidget extends GetView<HomeController> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      builder: (_) => Container(
        margin: const EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              children: [
                Expanded(
                  child: Text(
                    'Sản phẩm bán chạy',
                    style: FONT_CONST.REGULAR.copyWith(
                      color: AppColor.blackText,
                      fontSize: 18,
                    ),
                    maxLines: 1,
                  ),
                ),
                TextButton(
                  onPressed: () {
                    Get.rootDelegate.toNamed(Routes.CATEGORIES, arguments: 0);
                  },
                  child: Text(
                    'Xem tất cả',
                    style: FONT_CONST.REGULAR.copyWith(
                      color: AppColor.primary,
                      fontSize: 12,
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 4,
            ),
            TileGridProduct(),
          ],
        ),
      ),
    );
  }
}
