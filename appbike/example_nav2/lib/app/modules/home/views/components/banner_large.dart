import 'package:example_nav2/app/modules/root/controllers/root_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

class BannerLargeWidget extends GetView {
  BannerLargeWidget();

  @override
  Widget build(BuildContext context) {
    RootController rootController = Get.find();
    String srcImage = '';
    if (rootController.configAppModel.listImageHome != null &&
        rootController.configAppModel.listImageHome!.isNotEmpty) {
      srcImage =
          rootController.configAppModel.listImageHome![0].image_url ?? '';
    }
    return Container(
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(20)),
      ),
      margin: const EdgeInsets.symmetric(horizontal: 16),
      padding: const EdgeInsets.all(0),
      child: srcImage.isEmpty
          ? const SizedBox()
          : ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(20)),
              child: Image.network(
                srcImage,
                fit: BoxFit.cover,
              ),
            ),
    );
  }
}
