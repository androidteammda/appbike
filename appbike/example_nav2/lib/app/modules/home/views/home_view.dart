import 'package:example_nav2/app/modules/home/controllers/home_controller.dart';
import 'package:example_nav2/app/modules/home/views/components/banner_large.dart';
import 'package:example_nav2/app/modules/home/views/components/flash_sale_widget.dart';
import 'package:example_nav2/app/modules/home/views/components/product_best_sell.dart';
import 'package:example_nav2/app/modules/root/controllers/root_controller.dart';
import 'package:example_nav2/app/routes/app_pages.dart';
import 'package:example_nav2/app/utils/app_color.dart';
import 'package:example_nav2/app/utils/assets.dart';
import 'package:example_nav2/app/utils/text_styles.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomeView extends GetView<HomeController> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      builder: (_) => Scaffold(
        body: _body(),
      ),
    );
  }

  Widget _body() {
    RootController rootController = Get.find();
    return Container(
      child: Stack(
        children: [
          Container(
            color: AppColor.primary_second,
          ),
          Container(
            margin: const EdgeInsets.only(top: 64),
            decoration: BoxDecoration(
              color: AppColor.gray_bg_main,
              borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(30),
                topRight: Radius.circular(30),
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.symmetric(vertical: 16),
            child: ListView(
              physics: const BouncingScrollPhysics(),
              shrinkWrap: true,
              children: [
                Container(
                  padding: EdgeInsets.only(bottom: 10),
                  alignment: Alignment.center,
                  child: Text(
                    'Bách hóa phụ tùng xe máy',
                    style: TextStyles.textBoldStyles(AppColor.white, 16),
                  ),
                ),
                BannerLargeWidget(),
                const SizedBox(
                  height: 16,
                ),
                _rowCategory(),
                SizedBox(
                  height: controller.listProductSales.value.isEmpty ? 0 : 16,
                ),
                controller.listProductSales.value.isEmpty ||
                        controller.timeCountSale <= 0
                    ? const SizedBox()
                    : FlashSaleWidget(),
                const SizedBox(height: 8),
                ProductBestSellWidget(),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _rowCategory() {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 12),
      decoration: BoxDecoration(
        color: AppColor.white,
        borderRadius: BorderRadius.all(
          Radius.circular(12),
        ),
      ),
      height: 72,
      child: Row(
        children: [
          _itemCateRow(
            AssetContentApp.logo_honda,
          ),
          const SizedBox(
            width: 4,
          ),
          _itemCateRow(
            AssetContentApp.logo_yamaha,
          ),
          const SizedBox(
            width: 4,
          ),
          _itemCateRow(
            AssetContentApp.logo_piaggio,
          ),
          const SizedBox(
            width: 4,
          ),
          _itemCateRow(
            AssetContentApp.logo_sym,
          ),
          const SizedBox(
            width: 4,
          ),
          _itemCateRow(
            AssetContentApp.logo_suzuki,
          ),
        ],
      ),
    );
  }

  Widget _itemCateRow(Widget icon) {
    return Expanded(
      flex: 1,
      child: InkWell(
        onTap: () {
          // Get.rootDelegate.toNamed(Routes.CATEGORIES);
        },
        child: Center(child: icon),
      ),
    );
  }
}
