import 'dart:convert';

import 'package:example_nav2/app/data/repository.dart';
import 'package:example_nav2/app/models/categories/categories.dart';
import 'package:example_nav2/app/models/product/products_model.dart';
import 'package:example_nav2/app/modules/root/controllers/root_controller.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class HomeController extends GetxController {
  HomeController();

  RootController rootController = Get.find();
  var listProductSales = <ItemsProduct>[].obs;
  var listProductBest = <ItemsProduct>[].obs;
  Categories _categories = Categories(data: DataCate(items: []));

  Categories get categories => _categories;

  int _timeCountSale = 0;

  int get timeCountSale => _timeCountSale;

  @override
  void onInit() {
    super.onInit();
    print('HomeController :: onInit');
    getListProductSales();
    // getListProductByCategory();
    getCategories();
  }

  Future<void> getListProductSales() async {
    listProductSales.clear();
    RootController rootController = Get.find();
    await rootController.getConfigApp();
    initDaySale();
    var response = await MyRepository.instance.getListProductSales();
    if (response == null) {
      print('response getListProductSales null');
      return;
    }
    Products? products = Products.fromJson(json.decode(response));

    if (rootController.isLogged) {
      if (rootController.userDeviceModel.typeUser!.contains('vip1')) {
        for (int i = 0; i < products.data!.items!.length; i++) {
          if (int.parse(products.data!.items![i].price_vip1!) > 0) {
            listProductSales.add(products.data!.items![i]);
          }
        }
      } else if (rootController.userDeviceModel.typeUser!.contains('vip2')) {
        for (int i = 0; i < products.data!.items!.length; i++) {
          if (int.parse(products.data!.items![i].price_vip2!) > 0) {
            listProductSales.add(products.data!.items![i]);
          }
        }
      } else {
        listProductSales.value = products.data!.items!;
      }
    } else {
      listProductSales.value = products.data!.items!;
    }
    await getListProductByCategory();
    update();
  }

  Future<void> getListProductByCategory() async {
    listProductBest.clear();
    var response = await MyRepository.instance
        .getListProductByCategory('2', 500, 1, '-price');
    if (response == null) return;
    Products? products = await Products.fromJson(json.decode(response));
    if (rootController.isLogged) {
      if (rootController.userDeviceModel.typeUser!.contains('vip1')) {
        for (int i = 0; i < products.data!.items!.length; i++) {
          if (int.parse(products.data!.items![i].price_vip1!) > 0) {
            listProductBest.add(products.data!.items![i]);
          }
        }
      } else if (rootController.userDeviceModel.typeUser!.contains('vip2')) {
        for (int i = 0; i < products.data!.items!.length; i++) {
          if (int.parse(products.data!.items![i].price_vip2!) > 0) {
            listProductBest.add(products.data!.items![i]);
          }
        }
      } else {
        listProductBest.value = products.data!.items!;
      }
    } else {
      listProductBest.value = products.data!.items!;
    }
    update();
  }

  Future<void> getCategories() async {
    var response = await MyRepository.instance.getCategories(1);
    if (response == null) return;
    _categories = Categories.fromJson(json.decode(response));
  }

  void initDaySale() {
    // String date = DateFormat("yyyy-MM-dd hh:mm:sss").format(birthday);
    // String dateShow = DateFormat("dd/MM/yyyy").format(birthday);

    // String date = '20180626170555';
    // String dateWithT = date.substring(0, 8) + 'T' + date.substring(8);
    // DateTime dateTime = DateTime.parse(dateWithT);
    // "2022-04-30 00:00:00"
    // var endSaleDay = new DateFormat("dd/MM/yyyy HH:mm:ss").parse("10/02/2000 15:13:09")
    DateTime current = DateTime.now();
    RootController rootController = Get.find();

    String dateTo =
        rootController.configAppModel.flashSaleConfig!.date_to ?? '';
    if (dateTo.isEmpty) {
      return;
    }

    String timeNowS = DateFormat('yyyy-MM-dd HH:mm:ss').format(current);
    var currentDay = new DateFormat("yyyy-MM-dd HH:mm:ss").parse("$timeNowS");

    var endSaleDay = new DateFormat("yyyy-MM-dd HH:mm:ss").parse("$dateTo");

    final difSeconds = secondsBetween(currentDay, endSaleDay);
    _timeCountSale = difSeconds;
    print('timeCountSale: $_timeCountSale');
  }

  int secondsBetween(DateTime from, DateTime to) {
    from = DateTime(
      from.year,
      from.month,
      from.day,
      from.hour,
      from.minute,
      from.second,
    );
    to = DateTime(
      to.year,
      to.month,
      to.day,
      to.hour,
      to.minute,
      to.second,
    );
    return (to.difference(from).inSeconds).round();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
