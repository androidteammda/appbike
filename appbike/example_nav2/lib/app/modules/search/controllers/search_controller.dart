import 'package:example_nav2/app/controller/app_controller.dart';
import 'package:example_nav2/app/data/haibh_repository.dart';
import 'package:example_nav2/app/models/product/products_model.dart';
import 'package:example_nav2/app/modules/root/controllers/root_controller.dart';
import 'package:example_nav2/app/utils/ad_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SearchController extends AppController {
  RootController rootController = Get.find<RootController>();
  TextEditingController textEditingController = TextEditingController();

  // Products productList = Products();
  var productList = <ItemsProduct>[].obs;

  int page = 1;
  bool _isLoadingMore = false;

  bool get isLoadingMore => _isLoadingMore;
  String _key = '';

  bool _loadMoreReady = true;

  bool get loadMoreReady => _loadMoreReady;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onClose() {
    super.onClose();
  }

  Future<void> getSearch({required String key}) async {
    productList.clear();
    _key = key;
    loading.value = true;
    update();
    var products =
        await HaiBHRepository.instance.getSearch(key: key, page: 1);
    if (products != null &&
        products.data != null &&
        products.data!.items != null &&
        products.data!.items!.isNotEmpty) {
      if (rootController.isLogged) {
        if (rootController.userDeviceModel.typeUser!.contains('vip1')) {
          for (int i = 0; i < products.data!.items!.length; i++) {
            if (int.parse(products.data!.items![i].price_vip1!) > 0) {
              productList.add(products.data!.items![i]);
            }
          }
        } else if (rootController.userDeviceModel.typeUser!.contains('vip2')) {
          for (int i = 0; i < products.data!.items!.length; i++) {
            if (int.parse(products.data!.items![i].price_vip2!) > 0) {
              productList.add(products.data!.items![i]);
            }
          }
        } else {
          productList.value = products.data!.items!;
        }
      } else {
        productList.value = products.data!.items!;
      }
      // page = ++page;
    } else {
      _loadMoreReady = false;
    }
    loading.value = false;
    update();
  }

  Future<void> getSearchMore() async {
    page = ++page;
    try {
      _isLoadingMore = true;
      update();
      Products products =
          await HaiBHRepository.instance.getSearch(key: _key, page: page);
      if (products != null &&
          products.data != null &&
          products.data!.items != null &&
          products.data!.items!.isNotEmpty) {
        if (rootController.isLogged) {
          if (rootController.userDeviceModel.typeUser!.contains('vip1')) {
            for (int i = 0; i < products.data!.items!.length; i++) {
              if (int.parse(products.data!.items![i].price_vip1!) > 0 &&
                  !productList.contains(products.data!.items![i])) {
                productList.add(products.data!.items![i]);
              }
            }
          } else if (rootController.userDeviceModel.typeUser!
              .contains('vip2')) {
            for (int i = 0; i < products.data!.items!.length; i++) {
              if (int.parse(products.data!.items![i].price_vip2!) > 0 &&
                  !productList.contains(products.data!.items![i])) {
                productList.add(products.data!.items![i]);
              }
            }
          } else {
            products.data!.items!.forEach((element) {
              if (!productList.contains(element)) {
                productList.add(element);
              }
            });
            page = ++page;
          }
        } else {
          products.data!.items!.forEach((element) {
            if (!productList.contains(element)) {
              productList.add(element);
            }
          });
        }
        page = ++page;
      } else {
        _loadMoreReady = false;
      }
    } catch (e) {}
    _isLoadingMore = false;
    update();
  }
}
