import 'package:example_nav2/app/models/product/products_model.dart';
import 'package:example_nav2/app/modules/root/controllers/root_controller.dart';
import 'package:example_nav2/app/routes/app_pages.dart';
import 'package:example_nav2/app/utils/app_color.dart';
import 'package:example_nav2/app/utils/utility.dart';
import 'package:example_nav2/app/widget/customWidget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_view.dart';

import '../../../../main.dart';
import '../controllers/search_controller.dart';

class SearchView extends GetView<SearchController> {
  final TextEditingController controllerText = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    print('build ProductView:');
    return GetBuilder<SearchController>(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          leading: BackButton(),
          backgroundColor: AppColor.primary_second,
          title: inputForm(
            'Tìm kiếm sản phẩm, thương hiệu...',
            controller: controllerText,
            onSubmitted: (value) {
              print('onSearch');
              if (value.isEmpty) {
                showToast(context, 'Vui lòng nhập thông tin tìm kiếm');
                return;
              }
              controller.getSearch(key: value);
            },
          ),
        ),
        backgroundColor: AppColor.background_app,
        body: controller.loading.value
            ? loadingView()
            : checkData()
                ? _body()
                : emptyView(),
      );
    });
  }

  bool checkData() {
    return controller.productList.isNotEmpty;
  }

  Widget _body() {
    return Container(
      padding: const EdgeInsets.all(16),
      child: Column(
        children: [
          Expanded(
              child: ListView.builder(
            padding: const EdgeInsets.all(0),
            physics: const BouncingScrollPhysics(),
            itemBuilder: (c, i) {
              ItemsProduct product = controller.productList[i];
              return _itemProduct(product);
            },
            itemCount: controller.productList.length,
          )),
          const SizedBox(height: 16),
          controller.loadMoreReady
              ? Container(
                  child: controller.isLoadingMore
                      ? loadingView()
                      : TextButton(
                          child: Text(
                            'Xem thêm',
                            style: FONT_CONST.REGULAR.copyWith(
                              color: AppColor.blue,
                              fontSize: 14,
                            ),
                          ),
                          onPressed: () {
                            controller.getSearchMore();
                          },
                        ),
                )
              : const SizedBox()
        ],
      ),
    );
  }

  Widget _itemProduct(ItemsProduct product) {
    RootController rootController = Get.find();
    String price = Utility.getPrice(rootController, product);
    return ListTile(
      // leading: Image.network(product.imageMainUrl!) ,
      leading: Icon(
        CupertinoIcons.shopping_cart,
        color: AppColor.red,
      ),
      onTap: () {
        print('Select Items');
        Get.rootDelegate.toNamed(Routes.PRODUCT_DETAIL, arguments: product);
      },
      title: Text(
        product.name!,
        style: FONT_CONST.REGULAR.copyWith(
          color: AppColor.blackText,
          fontSize: 16,
        ),
      ),
      subtitle: Text(
        Utility.getValuePrice(price),
        style: FONT_CONST.SEMIBOLD.copyWith(
          color: AppColor.red,
          fontSize: 14,
        ),
      ),
    );
  }
}
