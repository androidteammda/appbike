import 'dart:convert';

import 'package:example_nav2/app/data/repository.dart';
import 'package:example_nav2/app/models/favorite/product_favorite.dart';
import 'package:example_nav2/app/models/product/product_detail_model.dart';
import 'package:example_nav2/app/models/product/products_model.dart';
import 'package:example_nav2/app/modules/root/controllers/root_controller.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:share_plus/share_plus.dart';

import '../../../../main.dart';

const platform = MethodChannel('com.sotavn.appbike');

class ProductDetailController extends GetxController {
  var listProductRelated = <ItemsProduct>[].obs;
  var itemProduct = ItemsProduct().obs;
  var productDetail = ProductDetail().obs;
  RootController rootController = Get.find<RootController>();

  var isLike = false.obs;

  bool _loading = false;

  bool get loading => _loading;

  @override
  void onInit() {
    super.onInit();
    initData();
  }

  Future<void> initData() async {
    _loading = true;
    update();
    itemProduct.value = Get.rootDelegate.arguments();
    String product_id = itemProduct.value.id ?? '';
    await getProductDetail(product_id);
    await getListProductRelated();
    _loading = false;
    update();
  }

  Future<void> refreshData({required ItemsProduct item}) async {
    _loading = true;
    update();
    itemProduct.value = item;
    String product_id = itemProduct.value.id ?? '';
    await getProductDetail(product_id);
    await getListProductRelated();
    _loading = false;
    update();
  }

  Future<void> shareUrl(DataProductDetail dataProductDetail) async {
    String title = dataProductDetail.name ?? 'Bách hoá phụ tùng xe máy';
    String url = dataProductDetail.linkWeb ?? '';
    try {
      Share.share('{$title} ${url}');
    } on Exception catch (_) {}
  }

  Future<void> rateProduct(DataProductDetail dataProductDetail) async {
    if (rootController.userDeviceModel.token!.isEmpty) {
      showToast(Get.context!, 'Thao tác yêu cầu đăng nhập');
      return;
    }
    if (isLike.value) {
      showToast(Get.context!, 'Bạn đã đánh giá sản phẩm này');
      return;
    }
    await MyRepository.instance.likeProduct(
        dataProductDetail.id ?? '', rootController.userDeviceModel.token!);
    this.isLike.value = true;
    showToast(Get.context!, 'Đánh giá sản phẩm thành công');
    update();
  }

  Future<void> getProductDetail(String product_id) async {
    var response = await MyRepository.instance.getProductDetail(product_id);
    productDetail.value = ProductDetail.fromJson(json.decode(response));
    getFavorite(productDetail.value.data!.id!);
    update();
  }

  Future<void> getFavorite(String product_id) async {
    print('getFavorite :: ${product_id}');
    var responseFavorite = await MyRepository.instance
        .likeProduct(product_id, rootController.userDeviceModel.token!);
    if (responseFavorite == null) {
      return;
    }
    ProductFavorite productFavorite =
        ProductFavorite.fromJson(json.decode(responseFavorite));
    print('getFavorite :: ${productFavorite.status}');

    this.isLike.value = (productFavorite.status == 0);
    update();
  }

  Future<void> getListProductRelated() async {
    var response = await MyRepository.instance.getListProductRelated();
    Products? products = Products.fromJson(json.decode(response));

    if (rootController.isLogged) {
      if (rootController.userDeviceModel.typeUser!.contains('vip1')) {
        for (int i = 0; i < products.data!.items!.length; i++) {
          if (int.parse(products.data!.items![i].price_vip1!) > 0) {
            listProductRelated.add(products.data!.items![i]);
          }
        }
      } else if (rootController.userDeviceModel.typeUser!.contains('vip2')) {
        for (int i = 0; i < products.data!.items!.length; i++) {
          if (int.parse(products.data!.items![i].price_vip2!) > 0) {
            listProductRelated.add(products.data!.items![i]);
          }
        }
      } else {
        listProductRelated.value = products.data!.items!;
      }
    } else {
      listProductRelated.value = products.data!.items!;
    }
    update();
  }

  Future<void> addProductToCart() async {
    bool save = await MyRepository.instance
        .addProductToStorage(itemProduct.value, (isExists) {
      if (!isExists) {
        rootController
            .setCountProductOrder(rootController.countProductOrder.value + 1);
      }
    });
    if (save) {
      showToast(Get.context!, 'Thêm vào giỏ hàng');
      update();
    }
  }
}
