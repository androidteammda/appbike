import 'package:example_nav2/app/models/product/product_detail_model.dart';
import 'package:example_nav2/app/models/product/products_model.dart';
import 'package:example_nav2/app/modules/product_details/controllers/product_detail_controller.dart';
import 'package:example_nav2/app/modules/product_details/views/view_page_image.dart';
import 'package:example_nav2/app/modules/root/controllers/root_controller.dart';
import 'package:example_nav2/app/routes/app_pages.dart';
import 'package:example_nav2/app/utils/app_color.dart';
import 'package:example_nav2/app/utils/assets.dart';
import 'package:example_nav2/app/utils/text_styles.dart';
import 'package:example_nav2/app/utils/utility.dart';
import 'package:example_nav2/app/widget/component_item_product.dart';
import 'package:example_nav2/app/widget/customWidget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:get/get.dart';

class ProductDetailView extends GetView<ProductDetailController> {
  RootController rootController = Get.find<RootController>();

  @override
  Widget build(BuildContext context) {
    print('RootProductDetailView');
    return GetRouterOutlet.builder(builder: (context, delegate, currentRoute) {
      return GetBuilder<ProductDetailController>(builder: (_) {
        return Scaffold(
          backgroundColor: AppColor.background_app,
          appBar: AppBar(
            backgroundColor: AppColor.white,
            toolbarHeight: 56,
            title: Text(
              'Chi tiết sản phẩm',
              style: FONT_CONST.REGULAR.copyWith(
                color: AppColor.blackText,
                fontSize: 22,
              ),
            ),
            leading: IconButton(
              onPressed: () {
                Get.back();
              },
              icon: Icon(
                Icons.arrow_back_ios_outlined,
                color: AppColor.primary,
              ),
            ),
            centerTitle: true,
            actions: [
              IconButton(
                padding: const EdgeInsets.all(0),
                onPressed: () {
                  Get.rootDelegate.toNamed(Routes.CART);
                },
                icon: AssetContentApp.ic_cart(
                    rootController.countProductOrder.value.toString()),
              ),
            ],
          ),
          body: controller.loading ? loadingView() : _body(context),
        );
      });
    });
  }

  Widget _body(BuildContext context) {
    DataProductDetail dataProductDetail =
        controller.productDetail.value.data ?? DataProductDetail();
    return Container(
      color: AppColor.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: ListView(
              shrinkWrap: true,
              children: [
                _thumbnail(context),
                const SizedBox(height: 10),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: Text(
                    dataProductDetail.name ?? '',
                    style: TextStyles.textBoldStyles2(AppColor.red, 18),
                  ),
                ),
                const SizedBox(height: 8),
                const Divider(
                  thickness: 1,
                  color: AppColor.lightGrey,
                ),
                const SizedBox(height: 8),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: _price(),
                ),
                const SizedBox(height: 8),
                _description(),
                // SizedBox(
                //   height: 8,
                // ),
                // _help(),
                const SizedBox(
                  height: 8,
                ),
                _comment(),
                const SizedBox(
                  height: 8,
                ),
                controller.listProductRelated.value.length == 0
                    ? const SizedBox()
                    : _suggestion(),
              ],
            ),
          ),
          _buyButton()
        ],
      ),
    );
  }

  Widget _thumbnail(BuildContext context) {
    DataProductDetail dataProductDetail =
        controller.productDetail.value.data ?? DataProductDetail();
    double height = MediaQuery.of(context).size.height;
    return Container(
      child: Stack(
        alignment: Alignment.center,
        children: [
          SizedBox(
            height: height *0.3,
            child: ViewPageImage(),
            // ImageViewContent(
            //     src: dataProductDetail.imageMainUrl ?? '',
            //     place: AssetContentApp.ic_item),
          ),
          Positioned.fill(
              child: Align(
            alignment: Alignment.bottomRight,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                IconButton(
                  iconSize: 44,
                  onPressed: () {
                    controller.rateProduct(dataProductDetail);
                  },
                  icon: Icon(
                    Icons.favorite_border, size: 30,
                    color: controller.isLike.value
                        ? AppColor.red_second
                        : AppColor.grayText,
                    // width: 44,
                    // height: 44,
                  ),
                ),
                IconButton(
                  iconSize: 44,
                  onPressed: () {
                    controller.shareUrl(dataProductDetail);
                    // controller.shareUrl(dataProduct.linkWeb ?? '');
                  },
                  icon: Icon(
                    Icons.share, size: 30,
                    color: AppColor.grayText,
                    // width: 44,
                    // height: 44,
                  ),
                )
              ],
            ),
          ))
        ],
      ),
    );
  }

  Widget _price() {
    DataProductDetail dataProduct =
        controller.productDetail.value.data ?? DataProductDetail();
    String price = '${dataProduct.price}';
    if (rootController.isLogged) {
      if (rootController.userDeviceModel.typeUser!.contains('vip1')) {
        if (dataProduct.price_vip1 != null &&
            dataProduct.price_vip1!.isNotEmpty) {
          price = '${dataProduct.price_vip1}';
        }
      } else if (rootController.userDeviceModel.typeUser!.contains('vip2')) {
        if (dataProduct.price_vip2 != null &&
            dataProduct.price_vip2!.isNotEmpty) {
          price = '${dataProduct.price_vip2}';
        }
      }
    }
    int discount = 0;
    if(price != '0' && dataProduct.priceBeforeDiscount != '0') {
      discount = 100 - int.parse(price)*100~/int.parse(dataProduct.priceBeforeDiscount!);
    }
    return Column(
      children: [
        Container(
          child: Row(
            children: [
              Text(
                Utility.getValuePrice(price),
                style: FONT_CONST.SEMIBOLD.copyWith(
                  color: AppColor.primary,
                  fontSize: 20,
                ),
              ),
              const SizedBox(width: 12),
              Text(
                Utility.getValuePrice(dataProduct.priceBeforeDiscount ?? '0'),
                maxLines: 1,
                style: FONT_CONST.MEDIUM.copyWith(
                  color: AppColor.grayText,
                  fontSize: 14,
                  decoration: TextDecoration.lineThrough,
                ),
              ),
              const SizedBox(width: 12),
              const Divider(
                thickness: 1,
                color: AppColor.grayText,
              ),
              Container(
                padding:
                    const EdgeInsets.only(left: 8, right: 8, top: 2, bottom: 2),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(2)),
                  color: AppColor.blue,
                ),
                child: Text(
                  '$discount%',
                  style: TextStyles.textNormStyles(AppColor.white, 14),
                ),
              )
            ],
          ),
        ),
        SizedBox(height: 8),
        Container(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Expanded(
                      child: Text(
                        'Khuyến mãi',
                        style: TextStyles.textBoldStyles(AppColor.black, 16),
                      )),
                  IconButton(
                      onPressed: () {},
                      icon: Icon(
                        Icons.arrow_forward_ios_outlined,
                        color: AppColor.red,
                        size: 18,
                      ))
                ],
              ),
              Container(
                  margin: const EdgeInsets.all(6.0),
                  padding: const EdgeInsets.all(10.0),
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.red, //                   <--- border color
                      width: 1.0,
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(
                        5.0) //                 <--- border radius here
                    ),
                  ),
                  child: Text(
                    'Giảm giá đến $discount %',
                    style: TextStyles.textNormStyles(AppColor.red, 14),
                  ))
            ],
          ),
        ),
      ],
    );
  }

  Widget _description() {
    DataProductDetail dataProduct =
        controller.productDetail.value.data ?? DataProductDetail();
    return dataProduct.description!.isEmpty
        ? const SizedBox()
        : Container(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Expanded(
                      child: Text(
                        'Mô tả sản phẩm',
                        style: TextStyles.textBoldStyles(AppColor.black, 16),
                      ),
                    ),
                    IconButton(
                      onPressed: () {},
                      icon: Icon(
                        Icons.arrow_forward_ios_outlined,
                        color: AppColor.red,
                        size: 18,
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 300,
                  child: SingleChildScrollView(
                    child: SizedBox(
                        child: Html(
                      data: dataProduct.description ?? '',
                    )),
                  ),
                ),
                const SizedBox(height: 6),
                Container(
                    margin: EdgeInsets.only(right: 10),
                    alignment: Alignment.centerRight,
                    child: Text(
                      'Xem thêm',
                      style: TextStyles.textNormStyles(AppColor.blue, 14),
                    ))
              ],
            ),
          );
  }

  Widget _help() {
    return Container(
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                  child: Text(
                'Hướng dẫn sử dụng',
                style: TextStyles.textBoldStyles(AppColor.black, 16),
              )),
              IconButton(
                  onPressed: () {},
                  icon: Icon(
                    Icons.arrow_forward_ios_outlined,
                    color: AppColor.red,
                    size: 18,
                  ))
            ],
          ),
          const SizedBox(height: 6),
          Text(
            '',
            textAlign: TextAlign.start,
            style: TextStyles.textNormStyles(AppColor.black, 14),
          ),
        ],
      ),
    );
  }

  Widget _comment() {
    DataProductDetail dataProduct =
        controller.productDetail.value.data ?? DataProductDetail();
    return (dataProduct.rate != null && dataProduct.rate!.length != 0)
        ? Container(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Expanded(
                        child: Text(
                      'Đánh giá sản phẩm',
                      style: TextStyles.textBoldStyles(AppColor.black, 16),
                    )),
                    IconButton(
                        onPressed: () {},
                        icon: Icon(
                          Icons.arrow_forward_ios_outlined,
                          color: AppColor.red,
                          size: 18,
                        ))
                  ],
                ),
                const SizedBox(height: 6),
                Text(
                  dataProduct.rate!.length != 0
                      ? dataProduct.rate![0].customerName ?? ''
                      : '',
                  style: TextStyles.textBoldStyles(AppColor.red, 15),
                ),
                const SizedBox(height: 6),
                Text(
                  dataProduct.rate!.length != 0
                      ? dataProduct.rate![0].content ?? ''
                      : '',
                  textAlign: TextAlign.start,
                  style: TextStyles.textNormStyles(AppColor.black, 14),
                ),
              ],
            ),
          )
        : const SizedBox();
  }

  Widget _suggestion() {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                  child: Text(
                'GỢI Ý DÀNH CHO BẠN'.toUpperCase(),
                style: TextStyles.textBoldStyles(AppColor.red, 18),
              )),
              IconButton(
                  onPressed: () {},
                  icon: Icon(
                    Icons.arrow_forward_ios_outlined,
                    color: AppColor.red,
                    size: 18,
                  ))
            ],
          ),
          const SizedBox(height: 6),
          Container(
            height: 240,
            child: _listSuggestion(),
          ),
          const SizedBox(height: 8),
        ],
      ),
    );
  }

  Widget _listSuggestion() {
    return controller.listProductRelated.length == 0
        ? SizedBox()
        : ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: controller.listProductRelated.length,
            itemBuilder: (context, index) {
              ItemsProduct itemsProduct =
                  controller.listProductRelated[index];
              return ItemProductView(
                itemsProduct: itemsProduct,
                isInDetails: true,
              );
            });
  }

  Widget _buyButton() {
    return Container(
      height: 56,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Flexible(
              flex: 1,
              child: InkWell(
                  onTap: () {
                    controller.addProductToCart();
                  },
                  child: Container(
                    alignment: Alignment.center,
                    color: AppColor.color_orange,
                    height: 56,
                    child: Text(
                      'Giỏ hàng'.toUpperCase(),
                      style: TextStyles.textBoldStyles(AppColor.white, 14),
                    ),
                  ))),
          Flexible(
              flex: 1,
              child: InkWell(
                  onTap: () {
                    controller.addProductToCart();
                    Get.rootDelegate.toNamed(Routes.CART);
                  },
                  child: Container(
                    alignment: Alignment.center,
                    color: AppColor.red,
                    height: 56,
                    child: Text(
                      'Mua ngay'.toUpperCase(),
                      style: TextStyles.textBoldStyles(AppColor.white, 14),
                    ),
                  ))),
        ],
      ),
    );
  }
}
