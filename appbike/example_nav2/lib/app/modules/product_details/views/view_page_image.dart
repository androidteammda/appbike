import 'package:example_nav2/app/modules/product_details/controllers/product_detail_controller.dart';
import 'package:example_nav2/app/utils/app_color.dart';
import 'package:example_nav2/app/utils/assets.dart';
import 'package:example_nav2/app/widget/image_view_content.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:photo_view/photo_view.dart';

class ViewPageImage extends StatefulWidget {
  _ViewPageImageState createState() => _ViewPageImageState();
}

class _ViewPageImageState extends State<ViewPageImage> {
  ProductDetailController detailController = Get.find();
  PageController _pageController = PageController(
    initialPage: 0,
  );
  double currentPage = 0;

  @override
  void initState() {
    super.initState();
    _pageController.addListener(() {
      setState(() {
        currentPage = _pageController.page!;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return PageView.builder(
        scrollDirection: Axis.horizontal,
        controller: _pageController,
        physics: const BouncingScrollPhysics(),
        itemCount:
            detailController.productDetail.value.data!.imageDetailUrls!.length,
        itemBuilder: (cP, iP) {
          String image =
              detailController.productDetail.value.data!.imageDetailUrls![iP];
          return Stack(
            alignment: Alignment.bottomCenter,
            children: [
              GestureDetector(
                  onTap: () {
                    Get.dialog(
                        Scaffold(
                          body: Stack(
                            children: [
                              Container(
                                // color: Colors.black,
                                child: PhotoView(
                                  initialScale: 1.0,
                                  minScale: 0.5,
                                  maxScale: 1.8,
                                  // loadingBuilder: (_, __) {
                                  //   return Center(
                                  //     child: CircularProgressIndicator(),
                                  //   );
                                  // },
                                  imageProvider: NetworkImage(image),
                                ),
                              ),
                              IconButton(
                                  onPressed: () {
                                    Navigator.of(Get.overlayContext!, rootNavigator: true).pop();
                                  },
                                  icon: Icon(Icons.arrow_back_ios_new),
                                  color: AppColor.white),
                            ],
                          ),
                        ),
                    );
                    // showDialog(
                    //   context: context,
                    //   builder: (_) {
                    //     return Scaffold(
                    //       body: Stack(
                    //         children: [
                    //           Container(
                    //             child: PhotoView(
                    //               initialScale: 1.0,
                    //               minScale: 0.5,
                    //               maxScale: 1.8,
                    //               loadingBuilder: (_, __) {
                    //                 return Center(
                    //                   child: CircularProgressIndicator(),
                    //                 );
                    //               },
                    //               imageProvider: NetworkImage(image),
                    //             ),
                    //           ),
                    //           IconButton(
                    //               onPressed: () {
                    //                 Get.back();
                    //               },
                    //               icon: Icon(Icons.arrow_back_ios_new),
                    //               color: AppColor.white),
                    //         ],
                    //       ),
                    //     );
                    //   },
                    // );
                  },
                  child: ImageViewContent(
                      src: image, place: AssetContentApp.ic_item)),
              Positioned(
                  bottom: 8,
                  child: Container(
                    height: 10,
                    child: ListView.builder(
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        physics: const BouncingScrollPhysics(),
                        itemCount: detailController
                            .productDetail.value.data!.imageDetailUrls!.length,
                        itemBuilder: (c, i) {
                          bool isChoose = currentPage == i ? true : false;
                          return Container(
                            margin: const EdgeInsets.symmetric(horizontal: 4),
                            child: Icon(
                              CupertinoIcons.circle_fill,
                              size: 8,
                              color:
                                  isChoose ? AppColor.primary : AppColor.white,
                            ),
                          );
                        }),
                  )),
            ],
          );
        });
  }
}
