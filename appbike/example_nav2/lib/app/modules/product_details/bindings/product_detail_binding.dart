import 'package:example_nav2/app/modules/product_details/controllers/product_detail_controller.dart';
import 'package:get/get.dart';

class ProductDetailBinding extends Bindings {
  @override
  void dependencies() {
    // Get.put<ProductDetailController>(ProductDetailController());
    Get.lazyPut(() => ProductDetailController(), fenix: true);
  }
}
