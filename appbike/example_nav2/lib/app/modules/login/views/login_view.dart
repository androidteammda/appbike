import 'package:example_nav2/app/modules/home/controllers/home_controller.dart';
import 'package:example_nav2/app/modules/root/controllers/root_controller.dart';
import 'package:example_nav2/app/routes/app_pages.dart';
import 'package:example_nav2/app/utils/app_color.dart';
import 'package:example_nav2/app/utils/assets.dart';
import 'package:example_nav2/app/utils/enum.dart';
import 'package:example_nav2/app/utils/text_styles.dart';
import 'package:example_nav2/app/utils/utility.dart';
import 'package:example_nav2/app/widget/button_linear_full_widget_primary.dart';
import 'package:example_nav2/app/widget/customLoader.dart';
import 'package:example_nav2/app/widget/customWidget.dart';
import 'package:example_nav2/app/widget/facebookLoginButton.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_view.dart';

import '../../../../main.dart';
import '../controllers/login_controller.dart';

class LoginView extends GetWidget<LoginController> {
  final CustomLoader loader = CustomLoader();
  final TextEditingController _mobileController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.white,
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            CupertinoIcons.left_chevron,
            color: AppColor.primary,
          ),
        ),
        elevation: 0,
        backgroundColor: Colors.transparent,
        title: Text(
          'Đăng nhập',
          style: TextStyles.textNormStyles(AppColor.primary, 20),
        ),
        centerTitle: true,
        // leading: IconButton(
        //   icon: Icon(Icons.arrow_back, color: Colors.white),
        //   onPressed: () => Get.getDelegate()?.toNamed(Routes.BOOK),
        // ),
      ),
      body: Container(
        decoration: BoxDecoration(
          color: AppColor.white,
        ),
        child: Column(
          children: [
            Expanded(
              child: Container(
                alignment: Alignment.center,
                child: ListView(
                  shrinkWrap: true,
                  physics: const BouncingScrollPhysics(),
                  padding: const EdgeInsets.all(24),
                  children: [
                    AssetContentApp.logo_ba_vuong,
                    const SizedBox(
                      height: 32,
                    ),
                    entryField(
                      'Tên đăng nhập',
                      controller: _mobileController,
                    ),
                    entryField('Mật khẩu',
                        controller: _passwordController, isPassword: true),
                    Align(
                      alignment: Alignment.centerRight,
                      child: TextButton(
                        onPressed: () {
                          Get.rootDelegate.toNamed(Routes.FORGOT_PASS);
                        },
                        child: Text(
                          'Quên mật khẩu?',
                          style: FONT_CONST.SEMIBOLD.copyWith(
                            color: AppColor.blackText,
                            fontSize: 14,
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(height: 16),
                    ButtonLinearAppPrimary(
                      title: 'Đăng nhập',
                      color: AppColor.primary,
                      onTap: () {
                        _emailLogin(context);
                      },
                    ),
                    const SizedBox(height: 8),
                  ],
                ),
              ),
            ),
            const Divider(),
            // const SizedBox(height: 30),
            // Padding(
            //   padding: const EdgeInsets.only(left: 16, right: 16),
            //   child: FacebookLoginButton(
            //     loader: loader,
            //   ),
            // ),
            const SizedBox(height: 24),
            InkWell(
              onTap: () {
                Get.rootDelegate.toNamed(Routes.REGISTER);
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 4),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Chưa có tài khoản?',
                      style: FONT_CONST.REGULAR.copyWith(
                        color: AppColor.blackText,
                        fontSize: 16,
                      ),
                    ),
                    const SizedBox(
                      width: 4,
                    ),
                    Text(
                      'Đăng ký',
                      style: FONT_CONST.MEDIUM.copyWith(
                        color: AppColor.button_green,
                        fontSize: 16,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            const SizedBox(height: 32),
          ],
        ),
      ),
    );
  }

  void _emailLogin(BuildContext context) {
    loader.showLoader(context);

    var isValid = Utility.validateCredentials(context,
        phone: _mobileController.text, password: _passwordController.text);

    if (isValid) {
      RootController rootController = Get.find();
      rootController
          .login(context,
              username: _mobileController.text, pass: _passwordController.text)
          .whenComplete(() {
        loader.hideLoader();
        if (rootController.authStatus.value == AuthStatus.LOGGED_IN) {
          Get.find<HomeController>().getListProductSales();
          Navigator.pop(context);
          rootController.userDeviceModel.password = _passwordController.text;
          // Future.delayed(const Duration(milliseconds: 300), () {
          //   rootController.isRegisterBook
          //       ? Get.getDelegate()?.toNamed(Routes.BOOK)
          //       : Get.getDelegate()?.toNamed(Routes.PROFILE);
          // });
        } else {
          showToast(context, 'Đăng nhập thất bại');
        }
      });
    } else {
      loader.hideLoader();
    }
  }
}
