import 'package:get/get.dart';

class LoginController extends GetxController {
  final indexSelected = 0.obs;

  void setSelected(int i) {
    indexSelected.value = i;
  }

  // Future<String?> login(BuildContext context,
  //     {String? phone, String? pass}) async {
  //   _userDeviceModel = null;
  //   _userDeviceModel = new UserDeviceModel(
  //     username: phone,
  //     password: pass,
  //   );
  //   String? token = await repository.login(_userDeviceModel!);
  //   print('token: $token');
  //   if (token != null) {
  //     authStatus.value = AuthStatus.LOGGED_IN;
  //     _userDeviceModel!.token = token;
  //
  //     //-- Save Object to Box
  //     var box = Hive.box(Constants.keyBoxUserDeviceModel);
  //     box.add(_userDeviceModel!);
  //
  //     getInfoUser(isRefreshUser);
  //     // update();
  //     return _userDeviceModel!.id.toString();
  //   } else {
  //     authStatus.value = AuthStatus.NOT_LOGGED_IN;
  //     return null;
  //   }
  // }

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void onReady() {
    super.onReady();
  }
}
