import 'package:example_nav2/app/modules/sales/controllers/flash_sale_controllers.dart';
import 'package:example_nav2/app/utils/app_color.dart';
import 'package:example_nav2/app/widget/component_item_product.dart';
import 'package:example_nav2/app/widget/customWidget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

class FlashSaleView extends GetView<FlashSaleController> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<FlashSaleController>(
      builder: (_) => Scaffold(
        appBar: AppBar(
          toolbarHeight: 56,
          elevation: 0,
          backgroundColor: AppColor.primary_second,
          leading: Builder(
            builder: (BuildContext context) {
              return IconButton(
                icon: Icon(
                  Icons.arrow_back_ios,
                  color: AppColor.white,
                ),
                onPressed: () {
                  Get.back();
                },
                tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
              );
            },
          ),
          // leading: IconButton(
          //   padding: const EdgeInsets.all(0),
          //   onPressed: () {},
          //   icon: AssetContentApp.ic_menu,
          // ),
          title: Text(
            'Flash Sale',
            style: FONT_CONST.REGULAR.copyWith(
              color: AppColor.white,
              fontSize: 24,
            ),
          ),

          centerTitle: true,
        ),
        backgroundColor: AppColor.gray_bg_main,
        body: _body(),
      ),
    );
  }

  Widget _body() {
    return Container(
      child: _listProduct(),
    );
  }

  Widget _listProduct() {
    return controller.listProductSales.value.length == 0
        ? emptyView()
        : Container(
            margin: EdgeInsets.only(left: 16, bottom: 16, top: 16),
            child: GridView.builder(
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  crossAxisSpacing: 5,
                  mainAxisSpacing: 5,
                  childAspectRatio: 0.7,
                ),
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                padding: const EdgeInsets.all(0),
                physics: const BouncingScrollPhysics(),
                itemCount: controller.listProductSales.value.length,
                itemBuilder: (BuildContext context, int index) => Container(
                      margin: const EdgeInsets.only(bottom: 16),
                      child: ItemProductView(
                        itemsProduct: controller.listProductSales.value[index],
                      ),
                    )));
  }
}
