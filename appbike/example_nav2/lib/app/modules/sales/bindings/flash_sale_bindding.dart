
import 'package:example_nav2/app/modules/sales/controllers/flash_sale_controllers.dart';
import 'package:get/get.dart';

class FlashBindings extends Bindings{
  @override
  void dependencies() {
    // TODO: implement dependencies
    Get.lazyPut<FlashSaleController>(
          () => FlashSaleController(),
    );
  }

}