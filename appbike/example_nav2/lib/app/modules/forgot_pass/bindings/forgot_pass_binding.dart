import 'package:example_nav2/app/modules/register/controller/register_controller.dart';
import 'package:get/get.dart';

class ForgotPassBinding extends Bindings {
  @override
  void dependencies() {
    print('RegisterBinding');
    Get.lazyPut<RegisterController>(
      () => RegisterController(),
    );
  }
}
