import 'package:example_nav2/app/modules/register/controller/register_controller.dart';
import 'package:example_nav2/app/modules/root/controllers/root_controller.dart';
import 'package:example_nav2/app/utils/app_color.dart';
import 'package:example_nav2/app/utils/assets.dart';
import 'package:example_nav2/app/utils/text_styles.dart';
import 'package:example_nav2/app/utils/utility.dart';
import 'package:example_nav2/app/widget/button_linear_full_widget_primary.dart';
import 'package:example_nav2/app/widget/customLoader.dart';
import 'package:example_nav2/app/widget/customWidget.dart';
import 'package:example_nav2/main.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ForgotPassView extends GetWidget<RegisterController> {
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _mobileController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _confirmController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final CustomLoader loader = CustomLoader();

  @override
  Widget build(BuildContext context) {
    print('forgot build:');
    return GetBuilder<RegisterController>(builder: (_) {
      return Scaffold(
        backgroundColor: AppColor.white,
        appBar: AppBar(
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(
              CupertinoIcons.left_chevron,
              color: AppColor.primary,
            ),
          ),
          elevation: 0,
          backgroundColor: Colors.transparent,
          title: Text(
            'Quên mật khẩu',
            style: TextStyles.textNormStyles(AppColor.primary, 20),
          ),
          centerTitle: true,
          // leading: IconButton(
          //   icon: Icon(Icons.arrow_back, color: Colors.white),
          //   onPressed: () => Get.getDelegate()?.toNamed(Routes.BOOK),
          // ),
        ),
        body: Column(
          children: [
            Expanded(
              child: ListView(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                physics: const BouncingScrollPhysics(),
                children: [
                  SizedBox(
                    height: 16,
                  ),
                  AssetContentApp.logo_ba_vuong,
                  const SizedBox(
                    height: 32,
                  ),
                  entryField('Số điện thoại',
                      controller: _mobileController, isPhone: true),
                  entryField('Mật khẩu',
                      controller: _passwordController, isPassword: true),
                  entryField('Nhập lại mật khẩu',
                      controller: _confirmController, isPassword: true),
                  const SizedBox(
                    height: 32,
                  ),
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 16),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  ButtonLinearAppPrimary(
                    title: 'Thiết lập lại mật khẩu',
                    color: AppColor.button_green,
                    onTap: () {
                      _handleForgotPass(context);
                    },
                  ),
                  const SizedBox(
                    height: 32,
                  ),
                ],
              ),
            ),
          ],
        ),
      );
    });
  }

  void _handleForgotPass(BuildContext context) {
    RootController controller = Get.find();
    if (_mobileController.text.isEmpty ||
        _passwordController.text.isEmpty ||
        _confirmController.text.isEmpty) {
      showToast(context, 'Vui lòng điền đầy đủ thông tin vào mẫu');
      return;
    } else if (_passwordController.text != _confirmController.text) {
      showToast(context, 'Mật khẩu và mật khẩu xác nhận không khớp');
      return;
    }

    loader.showLoader(context);
    var isValid = Utility.validateMobile(_mobileController.text);

    if (isValid) {
      controller
          .forgotPass(
              phone: _mobileController.text,
              pass: _passwordController.text,
              rePass: _confirmController.text)
          .then((value) {
        loader.hideLoader();
        if (value) {
          showToast(context, 'Lấy lại mật khẩu thành công');
          Get.back();
        } else {
          showToast(context, 'Lấy lại mật khẩu thất bại');
        }
      });
    } else {
      showToast(context, 'Số điện thoại không hợp lệ');
      loader.hideLoader();
    }
  }
}
