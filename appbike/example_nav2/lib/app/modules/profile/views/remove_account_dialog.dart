import 'package:example_nav2/app/modules/root/controllers/root_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../utils/app_color.dart';

class RemoveAccountDialog extends GetView<RootController> {
  const RemoveAccountDialog({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<RootController>(
      builder: (_) => Scaffold(
        backgroundColor: Colors.transparent,
        body: SingleChildScrollView(
          child: Container(
            width: double.infinity,
            margin: const EdgeInsets.all(20) + EdgeInsets.only(top: 50),
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(15),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                    margin: EdgeInsets.only(top: 15, bottom: 10),
                    alignment: Alignment.center,
                    child: Text(
                      'XOÁ TÀI KHOẢN',
                      style: TextStyle(
                          fontSize: 16,
                          color: AppColor.primary,
                          fontWeight: FontWeight.bold),
                    )),
                Text(
                  'Lưu ý:',
                  style: TextStyle(
                      fontSize: 16,
                      height: 1.5,
                      fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.italic),
                ),
                Text(
                  'Việc xoá tài khoản sẽ xoá tất cả thông tin cá nhân của '
                  'bạn khỏi hệ thống. Dữ liệu đã xoá không thể khôi phục.',
                  style: TextStyle(fontSize: 16, height: 1.5),
                ),
                BottomButtonField(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class BottomButtonField extends GetView<RootController> {
  const BottomButtonField({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 35,
      margin: EdgeInsets.only(top: 15, bottom: 10),
      decoration: BoxDecoration(
          color: AppColor.primary, borderRadius: BorderRadius.circular(5)),
      child: Row(
        children: [
          Expanded(
            child: GestureDetector(
              onTap: () async {
                // String userToken =
                //     await controller.storage.read(key: controller.tokenKey) ??
                //         '';
                // String removeAccountStatus =
                //     await ProductServices.removeAccount(
                //   token: userToken
                // );
                // await ProductServices.changePassword(
                //     userId: controller.userId, newPassword: '@bCd###888');
                // String removeAccountStatus = 'Xóa tài khoản thành công';
                // showToast(context, removeAccountStatus);
                // await controller.storage
                //     .write(key: tokenStorageKey, value: '');
                // Get.find<OrderController>().orderList.clear();
                // Get.find<OrderController>().userToken.value = '';
                controller.changePassWord(
                    context,
                    controller.userDeviceModel.password ?? '',
                    'abcd@123',
                    'abcd@123');
                controller.logOut();
                Navigator.pop(context, 'DELETE');
                // Get.back(result: 'DELETE');
              },
              child: Text(
                'XÁC NHẬN XOÁ',
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 14,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
          VerticalDivider(color: Colors.white, width: 1),
          Expanded(
            child: GestureDetector(
              onTap: () {
                Navigator.pop(context);
                // Get.back();
              },
              child: Text(
                'HUỶ',
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 14,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class OldPasswordInputField extends GetView<RootController> {
  const OldPasswordInputField({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(30),
        boxShadow: [
          BoxShadow(
              color: AppColor.primary,
              spreadRadius: 0,
              blurRadius: 5,
              offset: Offset(2, 2))
        ],
      ),
      // child: TextFormField(
      //   obscureText: true,
      //   onSaved: (newValue) => controller.oldPassword = newValue!,
      //   validator: (value) {
      //     if (value != controller.loggedUserData.value.password) {
      //       controller.addError(wrongOldPasswordError);
      //     }
      //     return null;
      //   },
      //   onChanged: (value) {
      //     if (value == controller.loggedUserData.value.password) {
      //       controller.removeError(wrongOldPasswordError);
      //     }
      //     controller.oldPassword = value;
      //   },
      //   decoration: InputDecoration(
      //     hintText: 'Mật khẩu cũ',
      //     hintStyle: TextStyle(color: mTextGreyColor, fontSize: 14),
      //     contentPadding: EdgeInsets.only(left: 20),
      //     filled: true,
      //     fillColor: Colors.white,
      //     prefixIcon: ImageIcon(
      //       AssetImage('images/icon/ic_sign_pass.png'),
      //       color: mPrimaryColor,
      //     ),
      //     suffixIcon: ImageIcon(
      //       AssetImage('images/icon/ic_required.png'),
      //       color: mLightIconColor,
      //     ),
      //     enabledBorder: OutlineInputBorder(
      //       borderRadius: BorderRadius.circular(30),
      //       borderSide: BorderSide(color: Colors.white),
      //     ),
      //     focusedBorder: OutlineInputBorder(
      //         borderRadius: BorderRadius.circular(30),
      //         borderSide: BorderSide(color: Colors.lightBlueAccent)),
      //   ),
      // ),
    );
  }
}
