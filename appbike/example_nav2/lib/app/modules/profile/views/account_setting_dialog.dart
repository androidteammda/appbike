import 'package:example_nav2/app/utils/app_color.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../main.dart';
import '../../root/controllers/root_controller.dart';
import 'remove_account_dialog.dart';

class AccountSettingDialog extends GetView<RootController> {
  const AccountSettingDialog({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // ProfileController profileController = Get.find();
    // controller.getDistrictList(
    //     profileController.loggedUserData.value.location!.regionId ??
    //         '508');
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          margin: const EdgeInsets.all(20),
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(15),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  margin: EdgeInsets.only(top: 15, bottom: 20),
                  alignment: Alignment.center,
                  child: Text(
                    'Cài đặt tài khoản',
                    style: TextStyle(
                        fontSize: 16,
                        color: AppColor.primary,
                        fontWeight: FontWeight.bold),
                  )),
              UserDataInput(isShowPhoneField: false),
              GestureDetector(
                onTap: () {
                  Get.dialog(RemoveAccountDialog()).then((value) {
                    if (value == 'DELETE') {
                      Navigator.pop(context);
                    }
                  });
                },
                child: Text(
                  'Xoá tài khoản',
                  style: TextStyle(
                      color: AppColor.primary,
                      fontSize: 16,
                      decoration: TextDecoration.underline),
                ),
              ),
              Container(
                width: double.infinity,
                height: 35,
                margin: EdgeInsets.only(top: 15, bottom: 10),
                decoration: BoxDecoration(
                    color: AppColor.primary,
                    borderRadius: BorderRadius.circular(5)),
                child: Row(
                  children: [
                    Expanded(
                      child: GestureDetector(
                        onTap: () async {
                          // if (controller.userData.value.name == '' ||
                          //     controller.userData.value.location!.region ==
                          //         '' ||
                          //     controller.userData.value.location!.district ==
                          //         '' ||
                          //     controller.userData.value.location!.address ==
                          //         '')
                          //   Get.snackbar('Lỗi!!!',
                          //       'Vui lòng bổ sung các trường thông tin cần thiết (có dấu *)',
                          //       duration: Duration(seconds: 2),
                          //       backgroundColor: Colors.black87,
                          //       colorText: Colors.white,
                          //       snackPosition: SnackPosition.BOTTOM);
                          // } else {
                          //   controller.userData.value.token =
                          //       await profileController.storage.read(
                          //               key: tokenStorageKey) ??
                          //           '';
                          //   String updateStatus =
                          //       await ProductServices.userUpdate(
                          //           user: controller.userData.value);
                          //   profileController.setUserData(
                          //       await ProductServices.getUserData(
                          //           token:
                          //               '${controller.userData.value.token}'));
                          showToast(context, 'Đã cập nhật thông tin tài khoản');
                            controller.setUserModel(controller.userDeviceModel);
                            Navigator.pop(context);
                          // }
                        },
                        child: Text(
                          'XÁC NHẬN',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 14,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                    VerticalDivider(color: Colors.white, width: 1),
                    Expanded(
                      child: GestureDetector(
                        onTap: () async {
                          // await controller.updateUserData();
                          // Get.back();
                          Navigator.pop(context);
                        },
                        child: Text(
                          'THOÁT',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 14,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class UserDataInput extends GetView<RootController> {
  const UserDataInput({
    required this.isShowPhoneField,
    Key? key,
  }) : super(key: key);
  final bool isShowPhoneField;

  @override
  Widget build(BuildContext context) {
    return GetBuilder<RootController>(
      builder: (_) => Column(
        children: [
          InputFieldWithName(
            text: 'Tên *',
            hintText: 'Điền tên',
            defaultValue: '${controller.userDeviceModel.display_name}',
            onChange: (value) {
              controller.userDeviceModel.display_name = value;
            },
          ),
          if (isShowPhoneField)
            InputFieldWithName(
              text: 'Số điện thoại *',
              hintText: 'Điền số điện thoại',
              defaultValue: '${controller.userDeviceModel.phone_number}',
              onChange: (value) {
                controller.userDeviceModel.phone_number = value;
              },
            ),
          InputFieldWithName(
            text: 'Tỉnh/Thành phố *',
            hintText: 'Chọn Tỉnh/Thành phố',
            defaultValue: '${controller.userDeviceModel.regionName}',
            onChange: (value) {
              controller.userDeviceModel.regionName = value;
            },
          ),
          InputFieldWithName(
            text: 'Quận/Huyện *',
            hintText: 'Chọn Quận/huyện',
            defaultValue: '${controller.userDeviceModel.districtName}',
            onChange: (value) {
              controller.userDeviceModel.districtName = value;
            },
          ),
          // PickFieldWithName(
          //   press: () {
          //     Get.dialog(PickCityDialog()).then((value) {
          //       controller.pickCity(value);
          //     });
          //   },
          //   text: 'Tỉnh/Thành phố *',
          //   hintText: 'Chọn Tỉnh/Thành phố',
          //   pickText: '${controller.userData.value.location!.region}',
          // ),
          // PickFieldWithName(
          //   press: () {
          //     Get.dialog(PickDistrictDialog()).then((value) {
          //       controller.pickDistrict(value);
          //       // Get.find<CartController>().getShipFee(
          //       //     regionId: '${controller.locationValue.value.regionId}',
          //       //     districtId: '${controller.locationValue.value.districtId}');
          //     });
          //   },
          //   text: 'Quận/Huyện *',
          //   hintText: 'Chọn Quận/Huyện',
          //   pickText: '${controller.userData.value.location!.district}',
          // ),
          Container(
            padding: const EdgeInsets.only(top: 5, left: 5, right: 5),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Địa chỉ cụ thể *'),
                ('${controller.userDeviceModel.address}' == '')
                    ? TextFormField(
                        onChanged: (value) {
                          controller.userDeviceModel.address = value;
                        },
                        maxLines: 2,
                        maxLength: 200,
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.only(bottom: 0),
                          hintText:
                              'Số nhà, tên toà nhà, tên đường, tên khu vực',
                          border: InputBorder.none,
                        ),
                      )
                    : TextFormField(
                        onChanged: (value) {
                          controller.userDeviceModel.address = value;
                        },
                        initialValue: '${controller.userDeviceModel.address}',
                        maxLines: null,
                        maxLength: 200,
                        decoration: InputDecoration(
                          hintStyle: TextStyle(fontStyle: FontStyle.italic),
                          contentPadding: EdgeInsets.only(bottom: 0),
                          hintText:
                              'Số nhà, tên toà nhà, tên đường, tên khu vực',
                          border: InputBorder.none,
                        ),
                      ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class InputFieldWithName extends StatelessWidget {
  const InputFieldWithName(
      {Key? key,
      required this.text,
      required this.hintText,
      required this.defaultValue,
      required this.onChange})
      : super(key: key);

  final String text, hintText, defaultValue;
  final void Function(String) onChange;

  // final bool isRequire;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40,
      padding: const EdgeInsets.only(left: 3, right: 3),
      decoration: const BoxDecoration(
        border: Border(
          bottom: BorderSide(color: Colors.black, width: 0.7),
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(text),
          Expanded(
            child: SizedBox(
              width: MediaQuery.of(context).size.width * 0.5,
              child: (defaultValue == '')
                  ? TextFormField(
                      onChanged: onChange,
                      textAlign: TextAlign.right,
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.only(bottom: 10),
                        hintText: hintText,
                        border: InputBorder.none,
                      ),
                    )
                  : TextFormField(
                      onChanged: onChange,
                      initialValue: defaultValue,
                      textAlign: TextAlign.right,
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.only(bottom: 10),
                        hintText: hintText,
                      ),
                    ),
            ),
          ),
        ],
      ),
    );
  }
}

class PickFieldWithName extends GetView<RootController> {
  const PickFieldWithName(
      {Key? key,
      required this.pickText,
      required this.text,
      required this.hintText,
      required this.press})
      : super(key: key);

  final String text, hintText, pickText;
  final VoidCallback press;

  @override
  Widget build(BuildContext context) {
    return GetBuilder<RootController>(builder: (_) {
      return Container(
        height: 40,
        padding: const EdgeInsets.only(left: 3),
        decoration: const BoxDecoration(
          border: Border(
            bottom: BorderSide(color: Colors.black),
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(text),
            Expanded(
              child: GestureDetector(
                onTap: press,
                child: (pickText == '')
                    ? Row(
                        children: [
                          Expanded(
                            child: TextField(
                              enabled: false,
                              textAlign: TextAlign.right,
                              decoration: InputDecoration(
                                hintText: hintText,
                                contentPadding: EdgeInsets.only(bottom: 17),
                                border: InputBorder.none,
                              ),
                            ),
                          ),
                          SizedBox(width: 3),
                          Icon(
                            Icons.arrow_forward_ios,
                            color: Colors.black54,
                            size: 14,
                          ),
                        ],
                      )
                    : Text(
                        pickText,
                        textAlign: TextAlign.right,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
              ),
            ),
          ],
        ),
      );
    });
  }
}
