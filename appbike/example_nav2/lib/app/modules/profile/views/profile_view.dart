import 'package:example_nav2/app/modules/root/controllers/root_controller.dart';
import 'package:example_nav2/app/routes/app_pages.dart';
import 'package:example_nav2/app/utils/app_color.dart';
import 'package:example_nav2/app/utils/enum.dart';
import 'package:example_nav2/app/widget/button_linear_full_widget_primary.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
// import 'package:flutter_html/shims/dart_ui_real.dart';
import 'package:get/get.dart';

import '../../../../main.dart';
import 'account_setting_dialog.dart';

class ProfileView extends GetView {
  @override
  Widget build(BuildContext context) {
    RootController rootController = Get.find();
    return GetBuilder<RootController>(builder: (_) {
      return Scaffold(
        backgroundColor: AppColor.background_app,
        // appBar: CustomAppBarOther(
        //   title: 'Tài khoản'.tr,
        //   isSearch: false,
        // ),
        body: Container(
          color: AppColor.gray_bg_main,
          child: Column(
            children: [
              Expanded(
                child: ListView(
                  physics: const BouncingScrollPhysics(),
                  padding: const EdgeInsets.all(0),
                  children: [
                    _profileInfo(),
                    const SizedBox(
                      height: 32,
                    ),
                    _itemSetting(
                      Icon(
                        CupertinoIcons.settings,
                        color: AppColor.primary,
                      ),
                      'Cài đặt tài khoản',
                      false,
                      () {
                        Get.dialog(AccountSettingDialog());
                      },
                    ),
                    _itemSetting(
                      Icon(
                        CupertinoIcons.phone_solid,
                        color: AppColor.primary,
                      ),
                      'Liên hệ',
                      false,
                      () {
                        rootController.callPhone();
                      },
                    ),
                    _itemSetting(
                      Icon(
                        CupertinoIcons.bell,
                        color: AppColor.primary,
                      ),
                      'Nhận thông báo',
                      true,
                      () {},
                    ),
                  ],
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                child: Text(
                  'Cứu hộ xe 24/7: Hotline 0971826611 Tư vấn sửa chữa và thay thế phụ tùng tại nhà',
                  textAlign: TextAlign.center,
                  style: FONT_CONST.REGULAR.copyWith(
                    color: AppColor.blackText,
                    fontSize: 16,
                  ),
                ),
              ),
              _viewMesseger(),
              ButtonLinearAppPrimary(
                  title: 'Đăng xuất',
                  color: rootController.isLogged
                      ? AppColor.primary
                      : AppColor.gray_icon,
                  onTap: () async {
                    if (rootController.authStatus.value !=
                        AuthStatus.LOGGED_IN) {
                      return;
                    }
                    await rootController.logOut();
                    showToast(context, 'Đăng xuất thành công');
                  }),
              const SizedBox(height: 8),
            ],
          ),
        ),
      );
    });
  }

  Widget _profileInfo() {
    RootController rootController = Get.find();
    return Card(
      color: AppColor.white,
      child: Container(
        padding: const EdgeInsets.all(16),
        child: rootController.isLogged
            ? Row(
                children: [
                  CircleAvatar(
                    radius: 50,
                    backgroundColor: AppColor.primary,
                    child: rootController.userDeviceModel.avatar!.isEmpty
                        ? CircleAvatar(
                            radius: 46,
                            backgroundImage: AssetImage('images/user.png'),
                            backgroundColor: AppColor.white,
                          )
                        : CircleAvatar(
                            radius: 46,
                            backgroundColor: AppColor.white,
                            backgroundImage: NetworkImage(
                                rootController.userDeviceModel.avatar!),
                          ),
                  ),
                  Expanded(
                    child: Container(
                      margin: const EdgeInsets.symmetric(horizontal: 16),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            '${rootController.userDeviceModel.display_name}',
                            style: FONT_CONST.REGULAR.copyWith(
                              color: AppColor.blackText,
                              fontSize: 18,
                            ),
                          ),
                          const SizedBox(height: 8),
                          Text(
                            '${rootController.userDeviceModel.email}',
                            style: FONT_CONST.REGULAR.copyWith(
                              color: AppColor.blackText,
                              fontSize: 12,
                            ),
                          ),
                          const SizedBox(height: 8),
                          Row(
                            children: [
                              Text(
                                'Số dư:',
                                style: FONT_CONST.REGULAR.copyWith(
                                  color: AppColor.blackText,
                                  fontSize: 12,
                                ),
                              ),
                              Expanded(
                                child: Text(
                                  ' ${rootController.userDeviceModel.balance} Gold',
                                  style: FONT_CONST.REGULAR.copyWith(
                                    color: AppColor.color_orange,
                                    fontSize: 12,
                                  ),
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              )
            : InkWell(
                onTap: () {
                  Get.rootDelegate.toNamed(Routes.LOGIN);
                },
                child: Container(
                  padding: const EdgeInsets.all(12),
                  child: Center(
                    child: Text(
                      'Bạn chưa đăng nhập',
                      style: FONT_CONST.REGULAR.copyWith(
                        color: AppColor.blackText,
                        fontSize: 18,
                      ),
                    ),
                  ),
                ),
              ),
      ),
      elevation: 3,
      margin: const EdgeInsets.all(16),
    );
  }

  Widget _itemSetting(
      Widget icon, String title, bool isShowButton, Function() onTap) {
    RootController rootController = Get.find();
    return InkWell(
      onTap: onTap,
      child: Container(
        padding: const EdgeInsets.only(
          left: 16,
          right: 16,
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            const SizedBox(
              height: 16,
            ),
            Row(
              children: [
                const SizedBox(
                  width: 16,
                ),
                icon,
                const SizedBox(
                  width: 16,
                ),
                Expanded(
                  child: Text('$title'),
                ),
                isShowButton
                    ? CupertinoSwitch(
                        activeColor: AppColor.primary,
                        value:
                            rootController.settingModel.isNotification ?? false,
                        onChanged: (v) {
                          rootController.updateSetNotification(v);
                        },
                      )
                    : const SizedBox(),
              ],
            ),
            const SizedBox(
              height: 16,
            ),
            Divider(
              height: 1,
            ),
          ],
        ),
      ),
    );
  }

  Widget _viewMesseger() {
    RootController rootController = Get.find();
    return Container(
      margin: EdgeInsets.only(bottom: 16),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          InkWell(
            onTap: () {
              rootController.pushToFB();
            },
            child: Image.asset(
              'images/export/ic_facebook.png',
              height: 50,
              width: 50,
              fit: BoxFit.cover,
            ),
          ),
          SizedBox(
            width: 10,
          ),
          InkWell(
            onTap: () {
              rootController.pushToFB();
            },
            child: Image.asset(
              'images/export/ic_zalo.png',
              height: 50,
              width: 50,
              fit: BoxFit.cover,
            ),
          ),
        ],
      ),
    );
  }
}
