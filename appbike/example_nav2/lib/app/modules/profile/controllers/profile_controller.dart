import 'dart:ui';
import 'package:example_nav2/app/controller/app_controller.dart';
import 'package:get/get.dart';

class ProfileController extends AppController {
  //TODO: Implement ProfileController

  void showInfo() {
  }

  final count = 0.obs;

  final List locale =[
    {'name':'ENGLISH','locale': Locale('en','US')},
    {'name':'BRAZIL','locale': Locale('pt','BR')},
    {'name':'VIETNAMESE','locale': Locale('vi','VN')},
  ];


  void updateLanguage(Locale locale){
    Get.updateLocale(locale);
  }


  @override
  void onInit() {
    super.onInit();
    print('ProfileController onInit :');
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  void increment() => count.value++;
}
