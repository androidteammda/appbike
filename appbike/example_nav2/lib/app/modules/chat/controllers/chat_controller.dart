
import 'package:example_nav2/app/controller/app_controller.dart';
import 'package:example_nav2/app/modules/root/controllers/root_controller.dart';
import 'package:example_nav2/app/utils/ad_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ChatController extends GetxController {
  RootController rootController = Get.find<RootController>();
  TextEditingController textEditingController = TextEditingController();

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onClose() {
    super.onClose();
  }

}
