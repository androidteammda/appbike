import 'package:example_nav2/app/utils/app_color.dart';
import 'package:example_nav2/app/utils/text_styles.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_view.dart';
import '../controllers/chat_controller.dart';

class ChatView extends GetView<ChatController> {
  final TextEditingController controllerText = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    print('build ChatView:');
    return GetRouterOutlet.builder(builder: (context, delegate, currentRoute) {
      return GetBuilder<ChatController>(builder: (_) {
        return Scaffold(
          backgroundColor: AppColor.background_app,
          body: Container(),
        );
      });
    });
  }
}
