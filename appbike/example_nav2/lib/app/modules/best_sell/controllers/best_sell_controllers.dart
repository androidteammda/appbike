
import 'dart:convert';

import 'package:example_nav2/app/data/repository.dart';
import 'package:example_nav2/app/models/product/products_model.dart';
import 'package:get/get.dart';

class BestSellController extends GetxController{

  var listProductSales = <ItemsProduct>[].obs;

  @override
  void onInit() {
    super.onInit();
    getListProductToSales();
  }

  Future<void> getListProductToSales() async{
    var response  = await MyRepository.instance.getListProductSales();
    Products? products = Products.fromJson(json.decode(response));
    listProductSales.value = products.data!.items!;
    update();
  }

}