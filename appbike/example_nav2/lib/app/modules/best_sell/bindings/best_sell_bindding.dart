import 'package:example_nav2/app/modules/best_sell/controllers/best_sell_controllers.dart';
import 'package:get/get.dart';

class BestSellBindings extends Bindings {
  @override
  void dependencies() {
    // TODO: implement dependencies
    Get.lazyPut<BestSellController>(
      () => BestSellController(),
    );
  }
}
