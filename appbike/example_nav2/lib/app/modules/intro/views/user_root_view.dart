import 'package:example_nav2/app/modules/intro/controllers/user_controller.dart';
import 'package:example_nav2/app/modules/intro/views/user_splash.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

class UserView extends GetWidget<UserController> {
  var viewIntro;

  @override
  Widget build(BuildContext context) {
    return GetRouterOutlet.builder(builder: (context, delegate, currentRoute) {
      return GetBuilder<UserController>(builder: (_) {
        return UserSplash();
      });
    });

  }
}
