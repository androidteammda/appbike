
import 'package:example_nav2/app/controller/app_controller.dart';
import 'package:example_nav2/app/routes/app_pages.dart';
import 'package:example_nav2/app/utils/enum.dart';
import 'package:get/get.dart';

class UserController extends AppController {
  final authStatus = AuthStatus.NOT_DETERMINED.obs;

  @override
  void onInit() {
    super.onInit();
    loadSplash();
  }

  Future<void> loadSplash() async {
    await new Future.delayed(const Duration(seconds: 2));
    Get.rootDelegate.toNamed(Routes.ROOT);
  }

  @override
  void onClose() {
    super.onClose();
  }
}
