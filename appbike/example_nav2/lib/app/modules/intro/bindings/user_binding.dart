
import 'package:example_nav2/app/modules/intro/controllers/user_controller.dart';
import 'package:get/get.dart';

class UserBinding extends Bindings{

  @override
  void dependencies() {
    Get.put<UserController>(UserController(), permanent: true);
  }
}