import 'dart:ui';

import 'package:example_nav2/app/models/config/config_app_model.dart';
import 'package:example_nav2/app/models/product/products_model.dart';
import 'package:example_nav2/app/modules/book/controllers/book_controller.dart';
import 'package:example_nav2/app/modules/root/controllers/root_controller.dart';
import 'package:example_nav2/app/utils/app_color.dart';
import 'package:example_nav2/app/utils/assets.dart';
import 'package:example_nav2/app/utils/text_styles.dart';
import 'package:example_nav2/app/utils/utility.dart';
import 'package:example_nav2/app/widget/customWidget.dart';
import 'package:example_nav2/app/widget/image_view_content.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

class BookView extends GetView<BookController> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return GetBuilder<BookController>(
        builder: (_) => Scaffold(
            appBar: AppBar(
              backgroundColor: AppColor.white,
              toolbarHeight: 56,
              title: Text(
                'Đơn hàng của bạn',
                style: TextStyles.textNormStyles(AppColor.black, 16),
              ),
              leading: IconButton(
                  onPressed: () {
                    Get.back();
                  },
                  icon: Icon(
                    Icons.arrow_back_ios_outlined,
                    color: AppColor.black,
                  )),
            ),
            body: controller.loading.value ? loadingView() : _body(context)));
  }

  Widget _body(BuildContext context) {
    return ListView(
      children: [
        _rowInfoView(),
        const SizedBox(height: 16),
        _rowShipView(),
        const SizedBox(height: 16),
        _rowPayView(),
        const SizedBox(height: 16),
        _listProductOrder(),
        const SizedBox(height: 16),
        _rowPriceView(),
        _btnFinish(context),
      ],
    );
  }

  Widget _rowInfoView() => Container(
        color: AppColor.white,
        child: Column(
          children: [
            const SizedBox(
              height: 10,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.asset(
                  'images/export/ic_book_location.png',
                  width: 24,
                  height: 24,
                ),
                SizedBox(
                  width: 2,
                ),
                Text(
                  'Thông tin giao hàng',
                  style: TextStyles.textNormStyles(AppColor.black, 14),
                ),
              ],
            ),
            const SizedBox(height: 6),
            const Divider(
              thickness: 1,
            ),
            const SizedBox(
              height: 6,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const SizedBox(
                  width: 8,
                ),
                Text('Số điện thoại',
                    style: TextStyles.textNormStyles(AppColor.black, 14)),
                Expanded(
                  child: Container(
                    alignment: Alignment.centerRight,
                    child: TextField(
                      textAlign: TextAlign.end,
                      controller: controller.textPhoneEditingController,
                      decoration: InputDecoration(
                        hintStyle:
                            TextStyles.textNormStyles(AppColor.black, 14),
                        border: InputBorder.none,
                      ),
                      minLines: 1,
                      style: TextStyle(height: 1.0, color: Colors.black),
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 6,
            ),
            const Divider(
              thickness: 1,
            ),
            const SizedBox(
              height: 6,
            ),
            InkWell(
                onTap: () {
                  controller.pushRegionScreen();
                },
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(
                      width: 8,
                    ),
                    Expanded(
                      child: Text('Tỉnh/Thành Phố',
                          style: TextStyles.textNormStyles(AppColor.black, 14)),
                    ),
                    Text(controller.productOrderDetail.region,
                        style:
                            TextStyles.textNormStyles(AppColor.grayText, 14)),
                    const SizedBox(
                      width: 8,
                    ),
                  ],
                )),
            const SizedBox(
              height: 6,
            ),
            const Divider(
              thickness: 1,
            ),
            const SizedBox(
              height: 6,
            ),
            InkWell(
                onTap: () {
                  controller.pushDistrictScreen();
                },
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(
                      width: 8,
                    ),
                    Expanded(
                      child: Text('Quận/Huyện',
                          style: TextStyles.textNormStyles(AppColor.black, 14)),
                    ),
                    Text(controller.productOrderDetail.district,
                        style:
                            TextStyles.textNormStyles(AppColor.grayText, 14)),
                    const SizedBox(
                      width: 8,
                    ),
                  ],
                )),
            const SizedBox(
              height: 6,
            ),
            const Divider(
              thickness: 1,
            ),
            Container(
                padding: const EdgeInsets.only(left: 10),
                alignment: Alignment.topLeft,
                child: Text('Nhập địa chỉ cụ thể:',
                    textAlign: TextAlign.left,
                    style: TextStyles.textNormStyles(AppColor.black, 14))),
            const SizedBox(
              height: 6,
            ),
            Container(
                height: 80,
                padding: const EdgeInsets.only(left: 10),
                alignment: Alignment.topLeft,
                child: TextField(
                    controller: controller.textAddressEditingController,
                    decoration: InputDecoration(
                      hintText: 'Số nhà, toà nhà, tên phố, tên khu vực'.tr,
                      hintStyle:
                          TextStyles.textNormStyles(AppColor.grayText, 14),
                      border: InputBorder.none,
                    ),
                    style: TextStyles.textNormStyles(AppColor.black, 14))),
          ],
        ),
      );

  Widget _rowShipView() => Container(
        color: AppColor.white,
        child: Column(
          children: [
            SizedBox(
              height: 8,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  width: 6,
                ),
                Image.asset(
                  'images/export/ic_book_ship.png',
                  width: 24,
                  height: 24,
                ),
                SizedBox(
                  width: 4,
                ),
                Text(
                  'Hình thức vận chuyển',
                  style: TextStyles.textNormStyles(AppColor.black, 14),
                ),
              ],
            ),
            SizedBox(
              height: 8,
            ),
            Divider(
              thickness: 1,
            ),
            SizedBox(
              height: 8,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  width: 10,
                ),
                Radio(
                  groupValue: controller.shipMethod.value,
                  activeColor: AppColor.red,
                  value: 0,
                  onChanged: (int? value) {
                    controller.onSelectShipMethod(0);
                  },
                ),
                SizedBox(
                  width: 2,
                ),
                Text(
                  'Shop tự vận chuyển',
                  style: TextStyles.textNormStyles(AppColor.black, 14),
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  width: 10,
                ),
                Radio(
                  groupValue: controller.shipMethod.value,
                  activeColor: AppColor.red,
                  value: 1,
                  onChanged: (int? value) {
                    controller.onSelectShipMethod(1);
                  },
                ),
                SizedBox(
                  width: 2,
                ),
                Text(
                  'Lấy sản phẩm từ cửa hàng',
                  style: TextStyles.textNormStyles(AppColor.black, 14),
                ),
              ],
            ),
            SizedBox(
              height: 8,
            ),
          ],
        ),
      );

  Widget _rowPayView() => Container(
        color: AppColor.white,
        child: Column(
          children: [
            const SizedBox(
              height: 8,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const SizedBox(
                  width: 8,
                ),
                Image.asset(
                  'images/export/ic_book_pay.png',
                  width: 24,
                  height: 24,
                ),
                const SizedBox(
                  width: 6,
                ),
                Text(
                  'Hình thức thanh toán',
                  style: TextStyles.textNormStyles(AppColor.black, 14),
                ),
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const SizedBox(
                  width: 10,
                ),
                Radio(
                  groupValue: controller.paymentMethod.value,
                  activeColor: AppColor.red,
                  onChanged: (int? value) {
                    controller.onSelectPaymentMethod(1);
                  },
                  value: 1,
                ),
                const SizedBox(width: 2),
                Text(
                  'Thanh toán COD',
                  style: TextStyles.textNormStyles(AppColor.black, 14),
                ),
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const SizedBox(
                  width: 10,
                ),
                Radio(
                  groupValue: controller.paymentMethod.value,
                  activeColor: AppColor.red,
                  value: 0,
                  onChanged: (int? value) {
                    controller.onSelectPaymentMethod(0);
                  },
                ),
                const SizedBox(
                  width: 2,
                ),
                Text(
                  'Thanh toán chuyển khoản',
                  style: TextStyles.textNormStyles(AppColor.black, 14),
                ),
              ],
            ),
            const SizedBox(height: 10),
            controller.paymentMethod.value == 0
                ? _listBank()
                : const SizedBox(),
          ],
        ),
      );

  Widget _listBank() {
    RootController rootController = Get.find();
    return ListView.builder(
        shrinkWrap: true,
        padding: const EdgeInsets.only(left: 16, right: 16),
        physics: const BouncingScrollPhysics(),
        // itemCount: rootController.configAppModel.listBankAccount!.length,
        itemCount: 1,
        itemBuilder: (c, i) {
          // RecievedBankAccounts model =
          //     rootController.configAppModel.listBankAccount![i];
          return Container(
            margin: const EdgeInsets.only(bottom: 16),
            child: Column(
              children: [
                Row(
                  children: [
                    Text(
                      'Tên ngân hàng',
                      style: FONT_CONST.REGULAR.copyWith(
                        fontSize: 14,
                        color: AppColor.grayText,
                      ),
                    ),
                    const SizedBox(width: 8),
                    Expanded(
                      child: Text(
                        // '${model.bankName}',
                        'Techcombank',
                        style: FONT_CONST.MEDIUM.copyWith(
                          fontSize: 16,
                          color: AppColor.primary,
                        ),
                        textAlign: TextAlign.right,
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 4,
                ),
                Row(
                  children: [
                    Text(
                      'Chi nhánh',
                      style: FONT_CONST.REGULAR.copyWith(
                        fontSize: 14,
                        color: AppColor.grayText,
                      ),
                    ),
                    const SizedBox(width: 8),
                    Expanded(
                      child: Text(
                        // '${model.bankBranch}',
                        'Nhuệ Giang',
                        style: FONT_CONST.MEDIUM.copyWith(
                          fontSize: 16,
                          color: AppColor.primary,
                        ),
                        textAlign: TextAlign.right,
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 4,
                ),
                Row(
                  children: [
                    Text(
                      'Tên chủ tài khoản',
                      style: FONT_CONST.REGULAR.copyWith(
                        fontSize: 14,
                        color: AppColor.grayText,
                      ),
                    ),
                    const SizedBox(width: 8),
                    Expanded(
                      child: SelectableText(
                        // '${model.holderName}',
                        'Nguyễn Chính Đức',
                        style: FONT_CONST.MEDIUM.copyWith(
                          fontSize: 16,
                          color: AppColor.primary,
                        ),
                        textAlign: TextAlign.right,
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 4,
                ),
                Row(
                  children: [
                    Text(
                      'Số tài khoản',
                      style: FONT_CONST.REGULAR.copyWith(
                        fontSize: 14,
                        color: AppColor.grayText,
                      ),
                    ),
                    const SizedBox(width: 8),
                    Expanded(
                      child: SelectableText(
                        // '${model.accountNumber}',
                        '19029741269693',
                        style: FONT_CONST.MEDIUM.copyWith(
                          fontSize: 16,
                          color: AppColor.primary,
                        ),
                        textAlign: TextAlign.right,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          );
        });
  }

  Widget _listProductOrder() => Container(
        color: AppColor.white,
        child: controller.listProductOrder.value.length == 0
            ? const SizedBox()
            : ListView.builder(
                padding:
                    const EdgeInsets.symmetric(vertical: 12, horizontal: 0),
                physics: const BouncingScrollPhysics(),
                shrinkWrap: true,
                itemCount: controller.listProductOrder.value.length,
                itemBuilder: (c, i) {
                  return _itemCart(i);
                },
              ),
      );

  Widget _itemCart(int index) {
    ItemsProduct itemProduct = controller.listProductOrder.value[index];
    RootController rootController = Get.find<RootController>();
    String price = Utility.getPrice(rootController, itemProduct);
    return Container(
      alignment: Alignment.centerLeft,
      // color: Colors.amber,
      margin: const EdgeInsets.only(bottom: 8),
      child: Container(
        padding: const EdgeInsets.all(12),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 80,
              width: 80,
              child: ImageViewContent(
                  src: itemProduct.imageMainUrl ?? '',
                  place: AssetContentApp.ic_item),
            ),
            const SizedBox(
              width: 16,
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(
                    height: 12,
                  ),
                  Text(
                    itemProduct.name ?? '',
                    style: FONT_CONST.REGULAR.copyWith(
                      color: AppColor.blackText,
                      fontSize: 15,
                    ),
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: Text(
                          Utility.getValuePrice(price),
                          style: FONT_CONST.SEMIBOLD.copyWith(
                            color: AppColor.primary,
                            fontSize: 15,
                          ),
                        ),
                      ),
                      Text(
                        '(Số lượng ${itemProduct.qty ?? 1})',
                        style: FONT_CONST.SEMIBOLD.copyWith(
                          color: AppColor.red,
                          fontSize: 15,
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _rowPriceView() => Container(
        color: AppColor.lightGrey,
        child: Column(
          children: [
            SizedBox(
              height: 16,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  width: 8,
                ),
                Expanded(
                  child: Text('Tổng giá sản phẩm',
                      style: TextStyles.textNormStyles(AppColor.black, 14)),
                ),
                Text(
                    Utility.getValuePrice(
                        controller.productPrice.value.toString()),
                    style: TextStyles.textNormStyles(AppColor.red, 14)),
                SizedBox(
                  width: 8,
                ),
              ],
            ),
            SizedBox(
              height: 16,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  width: 8,
                ),
                Expanded(
                  child: Text('Phí vận chuyển',
                      style: TextStyles.textNormStyles(AppColor.black, 14)),
                ),
                Text(
                    Utility.getValuePrice(
                        controller.shipFreePrice.value.toString()),
                    style: TextStyles.textNormStyles(AppColor.red, 14)),
                SizedBox(
                  width: 8,
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  child: Container(
                      padding: EdgeInsets.only(left: 10),
                      color: AppColor.white,
                      alignment: Alignment.centerLeft,
                      height: 40,
                      child: TextField(
                          controller: controller.textPromotionEditingController,
                          decoration: InputDecoration(
                            hintText: 'Nhập mã giảm giá'.tr,
                            hintStyle: TextStyles.textNormStyles(
                                AppColor.grayText, 14),
                            border: InputBorder.none,
                          ),
                          style:
                              TextStyles.textNormStyles(AppColor.black, 14))),
                ),
                SizedBox(
                  width: 2,
                ),
                InkWell(
                  onTap: () {
                    controller.verifyPromotionCode();
                  },
                  child: Image.asset(
                    'images/export/ic_book_check2.png',
                    width: 45,
                    height: 45,
                  ),
                ),
                SizedBox(
                  width: 8,
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            controller.promotionPrice.value == 0.0
                ? SizedBox()
                : SizedBox(
                    child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        width: 8,
                      ),
                      Expanded(
                        child: Text('Khuyến mãi',
                            style:
                                TextStyles.textNormStyles(AppColor.black, 14)),
                      ),
                      Text(
                          Utility.getValuePrice(
                              controller.promotionPrice.value.toString()),
                          style: TextStyles.textNormStyles(AppColor.red, 14)),
                      SizedBox(
                        width: 8,
                      ),
                    ],
                  )),
            SizedBox(
              height: 10,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  width: 8,
                ),
                Expanded(
                  child: Text('Thành tiền',
                      style: TextStyles.textNormStyles(AppColor.black, 14)),
                ),
                Text(
                    Utility.getValuePrice(
                        controller.totalPrice.value.toString()),
                    style: TextStyles.textNormStyles(AppColor.red, 14)),
                SizedBox(
                  width: 8,
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
          ],
        ),
      );

  Widget _btnFinish(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        left: 0.0,
        right: 0.0,
        top: 16,
      ),
      width: double.infinity,
      height: 50,
      child: ElevatedButton(
          style: ButtonStyle(
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(0)))),
              backgroundColor: MaterialStateProperty.resolveWith<Color>(
                (Set<MaterialState> states) {
                  if (states.contains(MaterialState.pressed) ||
                      states.contains(MaterialState.disabled)) {
                    return AppColor.lightGrey!;
                  }
                  return AppColor.red;
                },
              ),
              elevation: MaterialStateProperty.all<double>(0)),
          onPressed: () {
            controller.createOrder();
          },
          child: Text(
            'Thanh toán'.toUpperCase(),
            style: TextStyles.textNormStyles(AppColor.white, 18),
          )),
    );
  }
}
