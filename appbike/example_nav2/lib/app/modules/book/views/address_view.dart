import 'package:example_nav2/app/models/location/location_district.dart';
import 'package:example_nav2/app/models/location/location_region.dart';
import 'package:example_nav2/app/modules/book/controllers/book_controller.dart';
import 'package:example_nav2/app/utils/app_color.dart';
import 'package:example_nav2/app/utils/text_styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

class AddressView extends GetView<BookController> {
  BookController controller = Get.find<BookController>();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColor.white,
        toolbarHeight: 56,
        title: Text(
          controller.isRegion.value ? 'Chọn khu vực' : 'Chọn quận huyện',
          style: TextStyles.textNormStyles(AppColor.black, 16),
        ),
        leading: IconButton(
            onPressed: () {
              Get.back();
            },
            icon: Icon(
              Icons.arrow_back_ios_outlined,
              color: AppColor.black,
            )),
      ),
      body: controller.isRegion.value ? _bodyRegion() : _bodyDistrict(),
    );
  }

  Widget _bodyRegion() {
    return controller.listLocationRegion.value.length == 0
        ? SizedBox()
        : Container(
            child: ListView.builder(
              physics: BouncingScrollPhysics(),
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              itemCount: controller.listLocationRegion.value.length,
              itemBuilder: (context, index) {
                ItemsRegion itemsRegion =
                    controller.listLocationRegion.value[index];
                return InkWell(
                    onTap: () {
                      controller.onSelectRegion(itemsRegion);
                    },
                    child: Container(
                        margin: EdgeInsets.only(left: 10),
                        alignment: Alignment.centerLeft,
                        child: Column(
                          children: <Widget>[
                            ListTile(
                              title: Text(itemsRegion.name ?? 'region'),
                            ),
                            Divider(), //                           <-- Divider
                          ],
                        )));
              },
            ),
          );
  }

  Widget _bodyDistrict() {
    return controller.listLocationDistrict.value.length == 0
        ? SizedBox()
        : Container(
            child: ListView.builder(
              physics: BouncingScrollPhysics(),
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              itemCount: controller.listLocationDistrict.value.length,
              itemBuilder: (context, index) {
                ItemsDistrict itemsDistrict =
                    controller.listLocationDistrict.value[index];
                return InkWell(
                    onTap: () {
                      controller.onSelectDistrict(itemsDistrict);
                    },
                    child: Container(
                        margin: EdgeInsets.only(left: 10),
                        alignment: Alignment.centerLeft,
                        child: Column(
                          children: <Widget>[
                            ListTile(
                              title: Text(itemsDistrict.name ?? 'distrist'),
                            ),
                            Divider(), //                           <-- Divider
                          ],
                        )));
              },
            ),
          );
  }
}
