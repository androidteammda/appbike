import 'package:example_nav2/app/routes/app_pages.dart';
import 'package:example_nav2/app/utils/app_color.dart';
import 'package:example_nav2/app/utils/text_styles.dart';
import 'package:example_nav2/app/widget/button_linear_full_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
// import 'package:flutter_html/shims/dart_ui_real.dart';
import 'package:get/get.dart';

class BookDoneView extends GetView {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          backgroundColor: AppColor.white,
          toolbarHeight: 56,
          title: Text(
            'Đơn hàng của bạn',
            style: TextStyles.textNormStyles(AppColor.black, 16),
          ),
          leading: IconButton(
              onPressed: () {
                Get.back();
              },
              icon: Icon(
                Icons.arrow_back_ios_outlined,
                color: AppColor.black,
              )),
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: [
            Expanded(
              child: Container(
                alignment: Alignment.center,
                child: Text(
                  'Chúc mừng bạn đã thanh toán thành công đơn hàng.'
                  ' Hãy tiếp tục mua sắm',
                  textAlign: TextAlign.center,
                  style: TextStyles.textNormStyles(AppColor.black, 16),
                ),
              ),
            ),
            SizedBox(height: 80, child: _bottomButton())
          ],
        ));
  }

  Widget _bottomButton() {
    return ButtonLinearApp(
        title: 'Tiếp tục',
        onTap: () {
          Get.rootDelegate.toNamed(Routes.ROOT);
        });
  }
}
