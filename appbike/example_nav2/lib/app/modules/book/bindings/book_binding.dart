import 'package:example_nav2/app/modules/book/controllers/book_controller.dart';
import 'package:get/get.dart';

class BookBindings extends Bindings {
  @override
  void dependencies() {
    Get.put<BookController>(BookController());
  }
}
