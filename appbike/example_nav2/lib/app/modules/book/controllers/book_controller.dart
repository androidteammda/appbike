import 'dart:convert';

import 'package:example_nav2/app/controller/app_controller.dart';
import 'package:example_nav2/app/data/repository.dart';
import 'package:example_nav2/app/models/location/location_district.dart';
import 'package:example_nav2/app/models/location/location_region.dart';
import 'package:example_nav2/app/models/order/product_order_detail.dart';
import 'package:example_nav2/app/models/order/promotion_model.dart';
import 'package:example_nav2/app/models/order/ship_order.dart';
import 'package:example_nav2/app/models/product/products_model.dart';
import 'package:example_nav2/app/models/user/userDeviceModel.dart';
import 'package:example_nav2/app/modules/root/controllers/root_controller.dart';
import 'package:example_nav2/app/routes/app_pages.dart';
import 'package:example_nav2/app/utils/utility.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../main.dart';

class BookController extends AppController {
  var textPhoneEditingController = TextEditingController();
  var textAddressEditingController = TextEditingController();
  var textPromotionEditingController = TextEditingController();
  var textNoteEditingController = TextEditingController();

  var listLocationRegion = <ItemsRegion>[].obs;
  var listLocationDistrict = <ItemsDistrict>[].obs;
  var listProductOrder = <ItemsProduct>[].obs;
  var productPrice = 0.0.obs;
  var shipFreePrice = 0.obs;
  var promotionPrice = 0.obs;
  var totalPrice = 0.0.obs;

  // var regionSelect = ItemsRegion().obs;
  // var districtSelect = ItemsDistrict().obs;
  var isRegion = false.obs;
  var paymentMethod = 0.obs;
  var shipMethod = 1.obs;

  ProductOrderDetail _productOrderDetail = ProductOrderDetail();

  ProductOrderDetail get productOrderDetail => _productOrderDetail;

  RootController rootController = Get.find<RootController>();

  @override
  void onInit() {
    super.onInit();
    loading.value = true;
    print('BookController :: onInit');
    textPhoneEditingController.text =
        rootController.userDeviceModel.phone_number!.isEmpty
            ? '097182661'
            : rootController.userDeviceModel.phone_number!;
    getListProducts();
  }

  Future<void> getListProducts() async {
    String phone = _productOrderDetail.phone;
    if (phone.isEmpty) {
      phone = rootController.userDeviceModel.phone_number ?? "";
    }
    _productOrderDetail =
        await MyRepository.instance.getInfoOrderFromLocalStorage();
    if (_productOrderDetail.regionId != null &&
        _productOrderDetail.regionId.isNotEmpty) {
      getLocationDistrict(_productOrderDetail.regionId);
    }
    textAddressEditingController..text = _productOrderDetail.address;
    textPhoneEditingController..text = phone;
    var stringJson = await MyRepository.instance.getListOrderLocalStorage();
    if (stringJson.toString().isNotEmpty) {
      List<dynamic> list = json.decode(stringJson);
      List<ItemsProduct> listOrder =
          list.map((item) => ItemsProduct.fromJson(item)).toList();
      listProductOrder.value = listOrder;
    }
    calculatorSumPrice();
    getLocationRegion();
    loading.value = false;
    update();
  }

  Future<void> getLocationRegion() async {
    var response = await MyRepository.instance.getLocationRegion();
    Region region = Region.fromJson(json.decode(response));
    listLocationRegion.value = region.data!.items!;
    update();
  }

  void pushRegionScreen() {
    this.isRegion.value = true;
    Get.rootDelegate.toNamed(Routes.ADDRESS);
  }

  void pushDistrictScreen() {
    this.isRegion.value = false;
    Get.rootDelegate.toNamed(Routes.ADDRESS);
  }

  void pushPromotionScreen() {}

  void onSelectRegion(ItemsRegion itemsRegion) {
    getLocationDistrict(itemsRegion.id!);
    // regionSelect.value = itemsRegion;
    _productOrderDetail.districtId = '';
    _productOrderDetail.district = '';
    _productOrderDetail.regionId = itemsRegion.id!;
    _productOrderDetail.region = itemsRegion.name!;
    Get.back();
    update();
    getShipFree();
  }

  void onSelectDistrict(ItemsDistrict itemsDistrict) {
    // districtSelect.value = itemsDistrict;
    _productOrderDetail.districtId = itemsDistrict.id!;
    _productOrderDetail.district = itemsDistrict.name!;
    Get.back();
    update();
    getShipFree();
  }

  void onSelectShipMethod(int method) {
    this.shipMethod.value = method;
    getShipFree();
    print('SHIP METHOD: ${shipMethod.value}');
  }

  void onSelectPaymentMethod(int paymentMethod) {
    this.paymentMethod.value = paymentMethod;
    update();
  }

  void onSelectPromotionCode() {
    update();
  }

  Future<void> getShipFree() async {
    if (shipMethod.value == 1) {
      this.shipFreePrice.value = 0;
      setTotalPrice();
      // update();
      return;
    }
    String region_id = _productOrderDetail.regionId;
    String district_id = _productOrderDetail.districtId;
    var response =
        await MyRepository.instance.getShipFee(region_id, district_id);
    ShipFee shipFee = ShipFee.fromJson(json.decode(response));
    this.shipFreePrice.value = shipFee.data!.fee ?? 0;
    setTotalPrice();
    // update();
  }

  Future<void> getLocationDistrict(String region_id) async {
    var response = await MyRepository.instance.getLocationDistrict(region_id);
    District district = District.fromJson(json.decode(response));
    listLocationDistrict.value = district.data!.items!;
    update();
  }

  void calculatorSumPrice() {
    RootController rootController = Get.find();
    productPrice.value = 0.0;
    listProductOrder.value.forEach((itemsProduct) {
      String price = Utility.getPrice(rootController, itemsProduct);
      productPrice.value =
          productPrice.value + (itemsProduct.qty ?? 1) * double.parse(price);
    });
    setTotalPrice();
    // update();
  }

  Future<void> verifyPromotionCode() async {
    String code = textPromotionEditingController.text.toString();
    var response = await MyRepository.instance
        .getPromotion(code, totalPrice.value.toString());
    if (response == null) {
      showToast(
          Get.context!, 'Mã khuyến mãi không tồn tại hoặc đã hết hiệu lực');
      return;
    }
    Promotion promotion = Promotion.fromJson(json.decode(response));
    if (promotion.message!.contains('FAIL')) {
      showToast(
          Get.context!, 'Mã khuyến mãi không tồn tại hoặc đã hết hiệu lực');
      return;
    } else {
      _productOrderDetail.promotion_code = code;
      showToast(Get.context!, 'Áp dụng thành công');
      promotionPrice.value = int.parse(promotion.data!.discount ?? '0');
    }
    setTotalPrice();
    // update();
  }

  Future<void> setTotalPrice() async {
    totalPrice.value =
        productPrice.value + shipFreePrice.value - promotionPrice.value;
    update();
  }

  Future<void> createOrder() async {
    UserDeviceModel userDeviceModel = rootController.userDeviceModel;
    _productOrderDetail.payment_method = paymentMethod == 1 ? 'COD' : 'ATM';
    _productOrderDetail.ship_method = shipMethod == 1 ? 'NO_SHIP' : 'SHOP_SHIP';
    // _productOrderDetail.promotion_code = '';
    _productOrderDetail.address = textAddressEditingController.text;
    // productOrderDetail.region = regionSelect.value.name ?? 'Tỉnh/Thành phố';
    // productOrderDetail.district = districtSelect.value.name ?? 'Quận/Huyện';
    _productOrderDetail.name = userDeviceModel.display_name ?? '';
    _productOrderDetail.customer_name = userDeviceModel.display_name ?? '';
    _productOrderDetail.phone = textPhoneEditingController.text;
    _productOrderDetail.note = '';
    _productOrderDetail.ship_fee = shipFreePrice.value;
    _productOrderDetail.products = listProductOrder.value;
    List<ItemsProduct> listProduct = [];
    bool? saveSuccess =
        await MyRepository.instance.saveProductOrderStorage(listProduct);

    await MyRepository.instance
        .saveInfoOrderToLocalStorage(_productOrderDetail);

    if (rootController.userDeviceModel.token!.isEmpty) {
      showToast(Get.context!, 'Thao tác yêu cầu đăng nhập');
      return;
    }
    MyRepository.instance
        .createOrder(userDeviceModel.token!, productOrderDetail)
        .then((isSuccess) => {
              if (isSuccess)
                {
                  showToast(Get.context!, 'Đặt hàng thành công'),
                  Get.rootDelegate.toNamed(Routes.BOOK_DONE)
                }
              else
                {
                  showToast(Get.context!,
                      'Hệ thống đang bảo trì hoặc quá tải. Vui lòng thử lại sau ít phút'),
                }
            });
  }

  @override
  void onClose() {
    // TODO: implement onClose
    super.onClose();
  }
}
