import 'dart:convert';

import 'package:example_nav2/app/controller/app_controller.dart';
import 'package:example_nav2/app/data/haibh_repository.dart';
import 'package:example_nav2/app/data/repository.dart';
import 'package:example_nav2/app/models/config/config_app_model.dart';
import 'package:example_nav2/app/models/location/location_ip_model.dart';
import 'package:example_nav2/app/models/product/products_model.dart';
import 'package:example_nav2/app/models/setting/setting_model.dart';
import 'package:example_nav2/app/models/user/userDeviceModel.dart';
import 'package:example_nav2/app/modules/chat/views/chat_view.dart';
import 'package:example_nav2/app/modules/favorites/views/favorite_view.dart';
import 'package:example_nav2/app/modules/home/views/home_view.dart';
import 'package:example_nav2/app/modules/notification/views/notification_view.dart';
import 'package:example_nav2/app/modules/profile/views/profile_view.dart';
import 'package:example_nav2/app/utils/enum.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../../main.dart';
import '../../../models/user/user_model.dart';

class RootController extends AppController {
  RootController();

  final authStatus = AuthStatus.NOT_DETERMINED.obs;

  var bannerView = Container().obs;

  static RootController get to => Get.find();

  bool get isLogged => authStatus.value == AuthStatus.LOGGED_IN;

  var tabIndex = 0.obs;

  var titleAppBar = ''.obs;

  // UserModel? _userDeviceModel;
  //
  // UserModel get userModel => _userDeviceModel ?? UserModel.empty;

  UserDeviceModel? _userDeviceModel;

  UserDeviceModel get userDeviceModel =>
      _userDeviceModel ?? new UserDeviceModel();

  SettingModel? _settingModel;

  SettingModel get settingModel => _settingModel ?? new SettingModel();

  ConfigAppModel _configAppModel =
      ConfigAppModel(flashSaleConfig: FlashSaleConfig());

  ConfigAppModel get configAppModel => _configAppModel;

  List<Widget> listWidgetRoot = [
    HomeView(),
    ChatView(),
    NotificationView(),
    FavoriteView(),
    ProfileView(),
  ];

  void setUserModel(UserDeviceModel userModel) {
    _userDeviceModel = userModel;
    update();
  }

  var countProductOrder = 0.obs;

  void setCountProductOrder(int count) {
    countProductOrder.value = count;
    update();
  }

  @override
  void onInit() {
    super.onInit();
    initData();
    getCountProductOrder();
    timerSplash();
  }

  Future<void> getCountProductOrder() async {
    var stringJson = await MyRepository.instance.getListOrderLocalStorage();
    if (stringJson.toString().isNotEmpty) {
      List<dynamic> list = json.decode(stringJson);
      List<ItemsProduct> listProductOrder =
          list.map((item) => ItemsProduct.fromJson(item)).toList();
      setCountProductOrder(listProductOrder.length);
    }
    update();
  }

  Future<void> initData() async {
    UserDeviceModel user =
        await HaiBHRepository.instance.getUserFromLocalStorage();
    if (user.token != null && user.token!.isNotEmpty) {
      _userDeviceModel = user;
      authStatus.value = AuthStatus.LOGGED_IN;
      await getInfoUser();
    } else {
      authStatus.value = AuthStatus.NOT_LOGGED_IN;
    }

    _settingModel = await HaiBHRepository.instance.getSettingModelStorage();
    print('load ${_settingModel!.isNotification}');

    await getLocationIp();
    update();
  }

  Future<void> updateSetNotification(bool isNotification) async {
    print('updateSetNotification $isNotification');
    _settingModel!.isNotification = isNotification;
    await HaiBHRepository.instance.saveSettingModelStorage(_settingModel!);
    update();
  }

  Future<void> getLocationIp() async {
    try {
      var response = await HaiBHRepository.instance.getLocationIp();
      LocationIpModel locationIpModel =
          LocationIpModel.fromJson(jsonDecode(response));
      String? addressName =
          locationIpModel.regionName! + ', ' + locationIpModel.country!;

      if (_userDeviceModel == null) _userDeviceModel = UserDeviceModel();
      _userDeviceModel!.regionName = '${locationIpModel.regionName!}';
      _userDeviceModel!.regionId = '34';
      _userDeviceModel!.districtId = '34';
      _userDeviceModel!.address = addressName;
    } catch (e) {}
  }

  void timerSplash() async {
    var response = await MyRepository.instance.getCategories(1);
    print(response);
  }

  Future<String?> register(
    BuildContext context, {
    String? username,
    String? displayName,
    String? email,
    String? phone,
    String? pass,
    String? rePass,
  }) async {
    if (_userDeviceModel == null ||
        _userDeviceModel!.address == null ||
        _userDeviceModel!.address!.isEmpty) {
      await getLocationIp();
    }
    _userDeviceModel!.display_name = displayName;
    _userDeviceModel!.phone_number = phone;
    _userDeviceModel!.username = phone;
    // _userDeviceModel!.email = email;
    _userDeviceModel!.password = pass;
    _userDeviceModel!.re_password = rePass;

    bool isRegister =
        await HaiBHRepository.instance.register(context, _userDeviceModel!);

    print('isRegister: $isRegister');
    if (isRegister) {
      showToast(context, 'Đăng ký thành công. Đang tiến hành đăng nhập');
      await login(context,
          username: _userDeviceModel!.username,
          pass: _userDeviceModel!.password,
          isLoginFB: false);
    } else {
      showToast(context, 'Đăng ký thất bại');
    }
    return _userDeviceModel!.id.toString();
  }

  Future<String?> login(BuildContext context,
      {String? username, String? pass, bool? isLoginFB = false}) async {
    print('login:');
    if (_userDeviceModel == null ||
        _userDeviceModel!.address == null ||
        _userDeviceModel!.address!.isEmpty) {
      await getLocationIp();
    }
    _userDeviceModel!.phone_number = username;
    _userDeviceModel!.username = username;
    _userDeviceModel!.password = pass;
    String? token = await HaiBHRepository.instance
        .login(_userDeviceModel!, isLoginFB ?? false);
    print('token: $token');

    if (token != null) {
      _userDeviceModel!.token = token;

      //-- Save Object to Box
      // var box = Hive.box(Constants.keyBoxUserDeviceModel);
      // box.add(_userDeviceModel!);
      await HaiBHRepository.instance.saveUserToLocalStorage(_userDeviceModel!);
      await getInfoUser();
      authStatus.value = AuthStatus.LOGGED_IN;
      update();
      return _userDeviceModel!.id.toString();
    } else {
      authStatus.value = AuthStatus.NOT_LOGGED_IN;
      showToast(context, 'Đăng nhập thất bại');
      return null;
    }
  }

  Future<void> getInfoUser() async {
    print('getInfoUser:');
    UserDeviceModel userServer =
        await HaiBHRepository.instance.getInfoUser(_userDeviceModel!);
    _userDeviceModel!.id = userServer.id;
    _userDeviceModel!.email = userServer.email;
    _userDeviceModel!.avatar = userServer.avatar;
    _userDeviceModel!.phone_number = userServer.phone_number;
    _userDeviceModel!.birthDay = userServer.birthDay;
    _userDeviceModel!.display_name = userServer.display_name;
    _userDeviceModel!.enable_notification = userServer.enable_notification;
    _userDeviceModel!.typeUser = userServer.typeUser;
    _userDeviceModel!.balance = userServer.balance;
    await HaiBHRepository.instance.saveUserToLocalStorage(_userDeviceModel!);
  }

  Future<bool> forgotPass({String? phone, String? pass, String? rePass}) async {
    if (_userDeviceModel == null) {
      return false;
    }
    UserDeviceModel model = UserDeviceModel();
    model.phone_number = phone ?? '';
    model.password = pass ?? '';
    model.re_password = rePass ?? '';
    String token = await HaiBHRepository.instance.forgotPass(model);

    if (token.isNotEmpty) {
      print('Token: $token');
      _userDeviceModel!.token = token;
      _userDeviceModel!.password = pass;
      return true;
    } else
      return false;
  }

  Future<bool> changePassWord(BuildContext context, String oldPassWord,
      String newPass, String reNewPass) async {
    if (oldPassWord.isEmpty || newPass.isEmpty || reNewPass.isEmpty) {
      showToast(context, 'Vui lòng điền đầy đủ thông tin vào mẫu');
      return false;
    } else if (newPass != reNewPass) {
      showToast(context, 'Mật khẩu và mật khẩu xác nhận không khớp');
      return false;
    }
    print('TOKEN USEr: ${userDeviceModel.token}');
    await HaiBHRepository.instance.changePassWord(oldPassWord, newPass, userDeviceModel.phone_number ?? '');
    return true;
  }

  Future<void> logOut() async {
    print('logOut');
    _userDeviceModel = UserDeviceModel();
    await HaiBHRepository.instance.saveUserToLocalStorage(_userDeviceModel!);
    authStatus.value = AuthStatus.NOT_LOGGED_IN;
    update();
  }

  Future<void> handleFacebookSignIn(BuildContext context) async {}

  Future<void> pushMessenger() async {
    final Uri url = Uri.parse('https://m.me/102557775717514');
    try {
      await launchUrl(url, mode: LaunchMode.externalApplication);
    } catch (e) {
      print('Could not launch $url');
    }
  }

  Future<void> pushToFB() async {
    final Uri url = Uri.parse('https://www.facebook.com/Bh-PHU-TUNG-XE-MAY-102557775717514');
    try {
      await launchUrl(url, mode: LaunchMode.externalApplication);
    } catch (e) {
      print('Could not launch $url');
    }
  }

  Future<void> callPhone() async {
    // String url = 'tel://0971826611';
    final Uri launchUri = Uri(
      scheme: 'tel',
      path: '0971826611',
    );
    await launchUrl(launchUri);
  }

  Future<void> getConfigApp() async {
    _configAppModel = await HaiBHRepository.instance.getConfig();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
}
