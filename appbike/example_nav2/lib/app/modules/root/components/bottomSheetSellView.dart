import 'package:dotted_border/dotted_border.dart';
import 'package:example_nav2/app/utils/app_color.dart';
import 'package:example_nav2/app/utils/text_styles.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

class BottomSheetSellView extends GetWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: MediaQuery.of(context).viewInsets,
      child: Container(
        padding: const EdgeInsets.only(
          bottom: 100,
          top: 32,
          left: 24,
          right: 24,
        ),
        decoration: new BoxDecoration(
          // color: AppColor.background_blur.withOpacity(0.7),
          gradient: LinearGradient(
            colors: [
              AppColor.background_blur.withOpacity(0.7),
              AppColor.background_blur.withOpacity(0.7),
              AppColor.background_blur.withOpacity(0.7),
              AppColor.background_blur.withOpacity(0.1)
            ],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ),
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(30),
          ),
        ),
        child: Stack(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(
                  'Đăng sản phẩm',
                  style: TextStyles.textBoldStyles(
                    AppColor.blackText,
                    24,
                  ),
                ),
                _entryField(context, 'Nhập tên sản phẩm'),
                _entryField(context, 'Mô tả sản phẩm'),
                _entryField(context, 'Danh mục sản phẩm'),
                _entryField(context, 'Giá sản phẩm sản phẩm'),
                const SizedBox(
                  height: 16,
                ),
                Container(
                  height: 82,
                  child: _listImage(),
                ),
                const SizedBox(
                  height: 24,
                ),
                Align(
                  alignment: Alignment.center,
                  child: TextButton(
                    style: ButtonStyle(
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30),
                      )),
                    ),
                    onPressed: () {},
                    child: DottedBorder(
                        borderType: BorderType.RRect,
                        radius: Radius.circular(20),
                        color: AppColor.normalGrey,
                        child: Container(
                          padding: const EdgeInsets.symmetric(
                            vertical: 4,
                            horizontal: 12,
                          ),
                          decoration: BoxDecoration(
                            color: AppColor.white,
                            borderRadius: BorderRadius.all(Radius.circular(20)),
                          ),
                          child: Text(
                            'Đăng bán',
                            style: TextStyles.textNormStyles(
                              AppColor.blackText,
                              24,
                            ),
                          ),
                        )),
                  ),
                ),
                const SizedBox(
                  height: 16,
                ),
              ],
            ),
            Positioned(
              right: 0,
              top: 0,
              child: IconButton(
                  onPressed: () {
                    Get.delegate()?.popRoute();
                  },
                  icon: Icon(
                    CupertinoIcons.clear_thick,
                    color: AppColor.blackText,
                  )),
            ),
          ],
        ),
      ),
    );
  }

  Widget _listImage() {
    return Row(
      children: [
        Align(
          alignment: Alignment.bottomLeft,
          child: InkWell(
              onTap: () {},
              child: DottedBorder(
                borderType: BorderType.RRect,
                radius: Radius.circular(20),
                color: AppColor.normalGrey,
                child: Container(
                  decoration: BoxDecoration(
                    color: AppColor.white,
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                  ),
                  height: 64,
                  width: 64,
                  child: Icon(
                    CupertinoIcons.plus,
                    color: AppColor.background_app,
                  ),
                ),
              )),
        ),
        SizedBox(
          width: 8,
        ),
        Expanded(
          child: ListView.builder(
            padding: EdgeInsets.all(0),
            // shrinkWrap: true,
            physics: BouncingScrollPhysics(),
            scrollDirection: Axis.horizontal,
            itemBuilder: (context, index) {
              return Container(
                margin: EdgeInsets.only(
                  left: 8,
                ),
                height: 82,
                width: 82,
                child: Image.asset(
                  'images/sp_2.png',
                  fit: BoxFit.cover,
                ),
              );
            },
            itemCount: 5,
          ),
        ),
      ],
    );
  }

  Widget _entryField(
    BuildContext context,
    String hint, {
    TextEditingController? controller,
    bool isPassword = false,
    bool isPhone = false,
  }) {
    return Container(
      margin: EdgeInsets.only(top: 16),
      child: TextField(
        controller: controller,
        keyboardType: isPhone ? TextInputType.phone : TextInputType.text,
        style: TextStyle(
          fontStyle: FontStyle.normal,
          fontWeight: FontWeight.normal,
          fontSize: 13,
          fontFamily: appFontFamily,
        ),
        obscureText: isPassword,
        decoration: InputDecoration(
          hintText: hint,
          // border: InputBorder.none,
          // focusedBorder: OutlineInputBorder(
          //   borderRadius: BorderRadius.all(
          //     Radius.circular(10),
          //   ),
          //   borderSide: BorderSide(color: Colors.blue),
          // ),
          contentPadding: EdgeInsets.symmetric(vertical: 6, horizontal: 4),
        ),
      ),
    );
  }
}
