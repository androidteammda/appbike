import 'dart:ui';

import 'package:example_nav2/app/modules/favorites/views/favorite_view.dart';
import 'package:example_nav2/app/modules/root/controllers/root_controller.dart';
import 'package:example_nav2/app/utils/app_color.dart';
import 'package:example_nav2/app/utils/assets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/instance_manager.dart';

class BottomBar extends StatelessWidget {
  final int currentIndex;
  final int onTap;

  const BottomBar({
    Key? key,
    required this.currentIndex,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    RootController controller = Get.find<RootController>();
    return Container(
        height: GetPlatform.isIOS ? Get.mediaQuery.viewPadding.bottom + 55 : 70,
        // margin: EdgeInsets.only(bottom: 20),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              // topLeft: const Radius.circular(20),
              // topRight: const Radius.circular(20),
              ),
          color: AppColor.white,
          // boxShadow: [
          //   BoxShadow(color: Colors.black38, spreadRadius: 0, blurRadius: 10),
          // ],
        ),
        child: ClipRRect(
          // borderRadius: BorderRadius.only(
          //   topLeft: Radius.circular(20),
          //   topRight: Radius.circular(20),
          // ),
          child: Column(
            children: [
              Container(
                height: GetPlatform.isIOS ? Get.mediaQuery.viewPadding.bottom + 55 : 70,
                margin: EdgeInsets.only(bottom: 0),
                child: BottomNavigationBar(
                  backgroundColor: AppColor.white,
                  type: BottomNavigationBarType.fixed,
                  selectedItemColor: AppColor.primary,
                  unselectedItemColor: AppColor.normalGrey,
                  currentIndex: currentIndex,
                  showSelectedLabels: true,
                  showUnselectedLabels: true,
                  onTap: (index) {
                    controller.tabIndex.value = index;
                    switch (index) {
                      case 0:
                        controller.titleAppBar.value = 'Trang chủ'.tr;
                        break;
                      case 1:
                        controller.titleAppBar.value = 'Chat'.tr;
                        controller.pushMessenger();
                        break;
                      case 2:
                        controller.titleAppBar.value = 'Thông báo'.tr;
                        break;
                      case 3:
                        controller.titleAppBar.value = 'Yêu thích'.tr;
                        controller.listWidgetRoot.removeAt(3);
                        controller.listWidgetRoot.insert(3, FavoriteView());
                        break;
                      case 4:
                        controller.titleAppBar.value = 'Tài khoản'.tr;
                        break;
                    }
                  },
                  items: <BottomNavigationBarItem>[
                    BottomNavigationBarItem(
                      icon: Icon(Icons.home_outlined),
                      label: 'Trang chủ'.tr,
                    ),
                    BottomNavigationBarItem(
                        icon: Icon(CupertinoIcons.chat_bubble_2),
                        label: 'Chat'.tr),
                    BottomNavigationBarItem(
                        icon: Icon(CupertinoIcons.bell), label: 'Thông báo'.tr),
                    BottomNavigationBarItem(
                        icon: Icon(CupertinoIcons.star), label: 'Yêu thích'.tr),
                    BottomNavigationBarItem(
                        icon: Icon(CupertinoIcons.profile_circled),
                        label: 'Tài khoản'.tr),
                  ],
                ),
              ),
            ],
          ),
        ));
  }
}
