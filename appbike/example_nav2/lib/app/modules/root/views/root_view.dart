import 'package:example_nav2/app/modules/chat/views/chat_view.dart';
import 'package:example_nav2/app/modules/favorites/views/favorite_view.dart';
import 'package:example_nav2/app/modules/home/views/home_view.dart';
import 'package:example_nav2/app/modules/menu/views/drawer_menu_view.dart';
import 'package:example_nav2/app/modules/notification/views/notification_view.dart';
import 'package:example_nav2/app/modules/profile/views/profile_view.dart';
import 'package:example_nav2/app/modules/root/views/bottom_bar.dart';
import 'package:example_nav2/app/utils/app_color.dart';
import 'package:example_nav2/app/widget/customAppBarOther.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../controllers/root_controller.dart';

class RootView extends GetWidget<RootController> {
  @override
  Widget build(BuildContext context) {
    print('root build');
    return GetRouterOutlet.builder(builder: (context, delegate, currentRoute) {
      return Obx(() {
        return Stack(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(color: AppColor.background_app),
            ),
            Scaffold(
              drawer: DrawerMenuView(),
              appBar: CustomAppBarOther(
                title: '',
                isSearch: true,
              ),
              backgroundColor: Colors.transparent,
              body: IndexedStack(
                index: controller.tabIndex.value,
                children: controller.listWidgetRoot,
              ),
              bottomNavigationBar: BottomBar(
                currentIndex: controller.tabIndex.value,
                onTap: 0,
              ),
            )
          ],
        );
      });
    });
  }
}
