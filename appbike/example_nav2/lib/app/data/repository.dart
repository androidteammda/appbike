import 'dart:convert';

import 'package:example_nav2/app/models/order/product_order_detail.dart';
import 'package:example_nav2/app/models/product/products_model.dart';
import 'package:example_nav2/app/models/user/user_model.dart';
import 'package:http/http.dart' as http;
import 'package:localstorage/localstorage.dart';

import 'api/api_helper.dart';

const String userKey = 'user';
const String orderKey = 'orderKey';
const String userOrder = 'order';

class MyRepository {
  final AppApiHelper appApiHelper = AppApiHelper(httpClient: http.Client());
  final LocalStorage _localStorage = LocalStorage(userKey);

  MyRepository._privateConstructor();

  static final MyRepository _instance = MyRepository._privateConstructor();

  static MyRepository get instance => _instance;

  Future<void> saveUserToLocalStorage(UserModel user) async {
    Map<String, dynamic> dictionary = {
      'name': user.name ?? '',
      'email': user.email ?? '',
      'phone': user.phone ?? 0,
      'activity': user.activity ?? 0.0,
      'sex': user.sex ?? '',
    };
    try {
      await _localStorage.setItem(userKey, json.encode(dictionary));
      print('Lưu thành công');
    } catch (error) {
      throw Exception(error.toString());
    }
  }

  Future<UserModel> getUserFromLocalStorage() async {
    try {
      var ready = await _localStorage.ready;
      final stringJson = _localStorage.getItem(userKey);
      if (stringJson == null || stringJson == '') {
        return UserModel.empty;
      }
      Map<String, dynamic> dictionary =
          json.decode(stringJson) ?? Map<String, dynamic>();
      return UserModel.fromJson(dictionary);
    } catch (error) {
      throw ("Không lấy được thông tin người dùng đã lưu");
    }
  }

  Future<dynamic> getListOrderLocalStorage() async {
    try {
      var ready = await _localStorage.ready;
      if (ready) {
        final stringJson = _localStorage.getItem(orderKey);
        if (stringJson == null || stringJson == '') {
          return '';
        }
        return stringJson;
      }
    } catch (error) {
      throw ("Không lấy địa chỉ đã lưu");
    }
  }

  Future<void> saveInfoOrderToLocalStorage(ProductOrderDetail order) async {
    try {
      await _localStorage.setItem(userOrder, json.encode(order.toJson()));
      print('Lưu thành công');
    } catch (error) {
      throw Exception(error.toString());
    }
  }

  Future<ProductOrderDetail> getInfoOrderFromLocalStorage() async {
    try {
      var ready = await _localStorage.ready;
      final stringJson = _localStorage.getItem(userOrder);
      if (stringJson == null || stringJson == '') {
        return ProductOrderDetail();
      }
      Map<String, dynamic> dictionary =
          json.decode(stringJson) ?? Map<String, dynamic>();
      return ProductOrderDetail.fromJson(dictionary);
    } catch (error) {
      return new ProductOrderDetail();
    }
  }

  Future<bool> saveProductOrderStorage(List<ItemsProduct> addressItems) async {
    String response =
        jsonEncode(addressItems.map((items) => items.toJson()).toList())
            .toString();
    try {
      await _localStorage.setItem(orderKey, response);
      print('Lưu thành công');
      return true;
    } catch (_) {
      return false;
    }
  }

  Future<bool> addProductToStorage(
      ItemsProduct itemsProduct, void onAdd(bool isExists)) async {
    List<ItemsProduct> listProduct = [];
    var stringJson = await getListOrderLocalStorage();
    if (stringJson.toString().isNotEmpty) {
      List<dynamic> list = json.decode(stringJson);
      listProduct = list.map((item) => ItemsProduct.fromJson(item)).toList();
    }
    var contain = listProduct.where((element) =>
        element.id!.toLowerCase().contains(itemsProduct.id!.toLowerCase()));
    if (listProduct.isNotEmpty && !contain.isEmpty) {
      print('thêm số lượng sản phẩm đã tồn tại');
      itemsProduct.qty = (itemsProduct.qty ?? 1) + 1;
      listProduct[listProduct.indexWhere((element) => element.id!
          .toLowerCase()
          .contains(itemsProduct.id!.toLowerCase()))] = itemsProduct;
      onAdd(true);
    } else {
      listProduct.add(itemsProduct);
      onAdd(false);
    }
    bool? saveSuccess = await saveProductOrderStorage(listProduct);
    return saveSuccess;
  }

  Future<dynamic> getCategories(int? page) async {
    return await appApiHelper.getCategories(page);
  }

  Future<dynamic> getListProductByCategory(
      String category_id, int per_page, int page, String sort) async {
    return await appApiHelper.getListProductByCategory(
        category_id, per_page, page, sort);
  }

  Future<dynamic> getListProductSales() async {
    return await appApiHelper.getListProductFlashSale(1, 10, 1);
  }

  Future<dynamic> getListProductRelated() async {
    return await appApiHelper.getListProductRelated(14, 10, 1);
  }

  Future<dynamic> getProductDetail(String product_id) async {
    return await appApiHelper.getProductDetail(product_id);
  }

  Future<dynamic> getLocationRegion() async {
    return await appApiHelper.getLocationRegionList(1, 100);
  }

  Future<dynamic> getLocationDistrict(String region_id) async {
    return await appApiHelper.getLocationRegionDistrictList(region_id, 1, 100);
  }

  Future<dynamic> getShipFee(String region_id, String district_id) async {
    return await appApiHelper.getShipFee(region_id, district_id);
  }

  Future<dynamic> getPromotion(String code,String total_price) async {
    return await appApiHelper.getPromotionCode(code,total_price);
  }

  Future<bool> createOrder(
      String token, ProductOrderDetail productOrderDetail) async {
    return await appApiHelper.createProductOrder(token, productOrderDetail);
  }

  Future<dynamic> likeProduct(String product_id, String token) async {
    return await appApiHelper.likeProduct(product_id, token);
  }
}
