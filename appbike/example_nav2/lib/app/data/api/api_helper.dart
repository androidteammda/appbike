import 'dart:convert';

import 'package:example_nav2/app/models/order/order_state.dart';
import 'package:example_nav2/app/models/order/product_order_detail.dart';
import 'package:example_nav2/app/utils/app_crypto.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

const baseUrl = 'https://bike.sotavn.com';
// const baseUrl = 'https://paint.sotavn.com';
const baseNailUrl = 'http://nail-api.sotavn.com/';

class AppApiHelper {
  final http.Client httpClient;

  AppApiHelper({required this.httpClient});

  Future<bool> changePassWord(
      String oldPassWord, String newPassWord, String token) async {
    print('changePassWord');
    try {
      var map = new Map<String, dynamic>();
      map['app_id'] = AppCypto.appId_nail;
      map['old_password'] = oldPassWord;
      map['new_password'] = newPassWord;
      map['re_new_password'] = newPassWord;
      var response = await post(
        Uri.parse(baseNailUrl + AppCypto.pathChangePassWord),
        headers: {'Authorization': 'Bearer $token'},
        body: map,
      );

      print('changePassWord response statusCode ${response.statusCode}');
      if (response.statusCode == 200) {
        print('changePassWord statusCode 200: ${response.body}');
        var json = jsonDecode(response.body);
        int code = json['code'];
        if (code == 200) {
          return true;
        }
        return false;
      } else {
        print('changePassWord statusCode !=200');
        return false;
      }
    } on Exception catch (_) {
      print('login fail: ${_.toString()}');
      return false;
    }
  }

  Future<dynamic> getCategories(int? page) async {
    try {
      var response = await get(
        Uri.parse(baseUrl + AppCypto.pathCategories + 'page=${page}'),
      );
      if (response.statusCode == 200) {
        return response.body;
      } else {
        return null;
      }
    } catch (_) {}
  }

  Future<dynamic> getListProductByCategory(
      String category_id, int per_page, int page, String sort) async {
    try {
      var url = Uri.parse(baseUrl +
          AppCypto.pathCategoriesProduct +
          'category_id=${category_id}&page=${page}&per_page=${per_page}&sort=${sort}');
      print('getListProductByCategory: $url');
      var response = await get(url);
      if (response.statusCode == 200) {
        return response.body;
      } else {
        return null;
      }
    } catch (_) {}
  }

  Future<dynamic> getProductDetail(String product_id) async {
    try {
      var url = Uri.parse(
          baseUrl + AppCypto.pathProductDetail + 'product_id=${product_id}');
      print('getProductDetail: $url');
      var response = await get(url);
      if (response.statusCode == 200) {
        return response.body;
      } else {
        return null;
      }
    } catch (_) {}
  }

  Future<bool?> setProductFavorite(int product_id, String token) async {
    try {
      var url = Uri.parse(
          baseUrl + AppCypto.pathProductFavorite + 'product_id=${product_id}');
      var response = await get(
        url,
        headers: {'Authorization': 'Bearer $token'},
      );
      if (response.statusCode == 200) {
        return true;
      } else {
        return false;
      }
    } catch (_) {
      throw ('Lỗi yêu thích sản phẩm:: ${_.toString()}');
    }
  }

  Future<dynamic> getListProductRelated(
      int category_id, int per_page, int page) async {
    try {
      var url = Uri.parse(baseUrl +
          AppCypto.pathListProductRelated +
          'id=${category_id}&page=${page}&per_page=${per_page}');
      var response = await get(url);
      if (response.statusCode == 200) {
        return response.body;
      } else {
        return null;
      }
    } catch (_) {
      throw ('Không lấy được danh sách sản phẩm liên quan:: ${_.toString()}');
    }
  }

  Future<dynamic> getListProductHot(int per_page, int page) async {
    try {
      var url = Uri.parse(baseUrl +
          AppCypto.pathListProductHot +
          'page=${page}&per_page=${per_page}');
      var response = await get(url);
      if (response.statusCode == 200) {
        return response.body;
      } else {
        return null;
      }
    } catch (_) {
      throw ('Không lấy được danh sách sản phẩm hot:: ${_.toString()}');
    }
  }

  Future<dynamic> getListProductFlashSale(
      int flash_sale_id, int per_page, int page) async {
    try {
      var url = Uri.parse(baseUrl +
          AppCypto.pathListProductFlashSale +
          'flash_sale_id=${flash_sale_id}&page=${page}&per_page=${per_page}');
      print('Sale: $url');
      var response = await get(url);
      if (response.statusCode == 200) {
        return response.body;
      } else {
        return null;
      }
    } catch (_) {
      return null;
    }
  }

  Future<dynamic> getListProductFavorite(
      String token, int per_page, int page) async {
    try {
      var url = Uri.parse(baseUrl +
          AppCypto.pathListProductFavorite +
          'page=${page}&per_page=${per_page}');
      var response = await get(
        url,
        headers: {'Authorization': 'Bearer $token'},
      );
      if (response.statusCode == 200) {
        return response.body;
      } else {
        return null;
      }
    } catch (_) {
      throw ('Không lấy được danh sách sản phẩm favorite:: ${_.toString()}');
    }
  }

  Future<dynamic> getListProductSearch(
      String keyword, int per_page, int page) async {
    try {
      var url = Uri.parse(baseUrl +
          AppCypto.pathListProductSearch +
          'keyword=${keyword}&page=${page}&per_page=${per_page}');
      var response = await get(url);
      if (response.statusCode == 200) {
        return response.body;
      } else {
        return null;
      }
    } catch (_) {
      throw ('Không tìm được sản phẩm:: ${_.toString()}');
    }
  }

  Future<dynamic> getListProductOrder(
      String token, int per_page, int page) async {
    try {
      var url = Uri.parse(baseUrl +
          AppCypto.pathListProductOrder +
          'page=${page}&per_page=${per_page}');
      var response = await get(
        url,
        headers: {'Authorization': 'Bearer $token'},
      );
      if (response.statusCode == 200) {
        return response.body;
      } else {
        return null;
      }
    } catch (_) {
      throw ('Không lấy được danh sách sản phẩm order:: ${_.toString()}');
    }
  }

  Map<String, dynamic> mapDataOrder(ProductOrderDetail productOrderDetail) {
    var map = new Map<String, dynamic>();
    int count = 0;
    productOrderDetail.products.forEach((itemProduct) {
      map['products[${count}][id]'] = itemProduct.id.toString();
      map['products[${count}][qty]'] =
          itemProduct.qty == null ? '1' : itemProduct.qty.toString();
      map['products[${count}][flash_sale_id]'] =
          itemProduct.flashSaleId.toString();
      count = count + 1;
    });
    map['payment_method'] = productOrderDetail.payment_method.toString();
    map['shipping_method'] = productOrderDetail.ship_method.toString();
    map['promotion_code'] = productOrderDetail.promotion_code.toString();
    map['delivery_info[address]'] = productOrderDetail.address.toString();
    map['delivery_info[region_id]'] = productOrderDetail.region.toString();
    map['delivery_info[district_id]'] = productOrderDetail.district.toString();
    map['delivery_info[name]'] = productOrderDetail.name.toString();
    map['delivery_info[phone]'] = productOrderDetail.phone.toString();
    map['delivery_info[customer_ott_name]'] =
        productOrderDetail.customer_name.toString();
    map['note'] = productOrderDetail.note;
    map['ship_fee'] = productOrderDetail.ship_fee.toString();
    map['payment_fee'] = productOrderDetail.payment_fee;
    map['total_weight'] = productOrderDetail.total_weight;
    map['ship_date'] = productOrderDetail.ship_date;
    map['ship_hour'] = productOrderDetail.ship_hour;
    return map;
  }

  Future<bool> createProductOrder(
      String token, ProductOrderDetail productOrderDetail) async {
    try {
      var url = Uri.parse(baseUrl + AppCypto.pathOrderCreate);
      var map = mapDataOrder(productOrderDetail);
      var response = await post(
        url,
        headers: {'Authorization': 'Bearer $token'},
        body: map,
      );
      if (response.statusCode == 200) {
        OrderState orderState = OrderState.fromJson(json.decode(response.body));
        return orderState.status == 1;
      } else {
        return false;
      }
    } catch (_) {
      return false;
    }
  }

  Future<dynamic> cancelProductOrder(String token, int product_id) async {
    try {
      var url =
          Uri.parse(baseUrl + AppCypto.pathOrderCancel + 'id=${product_id}');
      var response = await get(
        url,
        headers: {'Authorization': 'Bearer $token'},
      );
      print(response.statusCode);
      if (response.statusCode == 200) {
        return response.body;
      } else {
        return null;
      }
    } catch (_) {
      throw ('Không huỷ được đơn hàng:: ${_.toString()}');
    }
  }

  Future<dynamic> getLocationRegionList(int page, int per_page) async {
    try {
      var url = Uri.parse(baseUrl +
          AppCypto.pathLocationRegion +
          'page=${page}&per_page=${per_page}');
      var response = await get(
        url,
      );
      print(response.statusCode);
      if (response.statusCode == 200) {
        return response.body;
      } else {
        return null;
      }
    } catch (_) {
      throw ('Không lấy được location:: ${_.toString()}');
    }
  }

  Future<dynamic> getLocationRegionDistrictList(
      String region_id, int page, int per_page) async {
    try {
      var url = Uri.parse(baseUrl +
          AppCypto.pathLocationDistrict +
          'region_id=${region_id}&page=${page}&per_page=${per_page}');
      var response = await get(
        url,
      );
      print(response.statusCode);
      if (response.statusCode == 200) {
        return response.body;
      } else {
        return null;
      }
    } catch (_) {
      throw ('Không lấy được location:: ${_.toString()}');
    }
  }

  Future<dynamic> getShipFee(String region_id, String district_id) async {
    try {
      var url = Uri.parse(baseUrl +
          AppCypto.pathOrderShipFee +
          'region_id=${region_id}&district_id=${district_id}');
      var response = await get(
        url,
      );
      print(response.statusCode);
      if (response.statusCode == 200) {
        return response.body;
      } else {
        return null;
      }
    } catch (_) {
      throw ('Không lấy được ship_fee:: ${_.toString()}');
    }
  }

  Future<dynamic> getPromotionCode(String code,String total_price) async {
    try {
      var url =
          Uri.parse(baseUrl + AppCypto.pathOrderPromotionCode + 'code=${code}'+ '&total_price=${total_price}');
      var response = await get(
        url,
      );
      print(response.statusCode);
      if (response.statusCode == 200) {
        return response.body;
      } else {
        return null;
      }
    } catch (_) {
      return null;
    }
  }

  Future<dynamic> likeProduct(String product_id, String token) async {
    try {
      var url = Uri.parse(
          baseUrl + AppCypto.pathLikeProduct + 'product_id=${product_id}');
      var response = await get(
        url,
        headers: {'Authorization': 'Bearer $token'},
      );
      print(response.statusCode);
      if (response.statusCode == 200) {
        return response.body;
      } else {
        return null;
      }
    } catch (_) {
      throw ('Không đánh giá được sản phẩm:: ${_.toString()}');
    }
  }
}
