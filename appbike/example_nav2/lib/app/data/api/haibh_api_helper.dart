import 'dart:convert';

import 'package:example_nav2/app/models/config/config_app_model.dart';
import 'package:example_nav2/app/models/notification/notification_model.dart';
import 'package:example_nav2/app/models/product/products_model.dart';
import 'package:example_nav2/app/models/user/userDeviceModel.dart';
import 'package:example_nav2/app/utils/app_crypto.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

import '../../../main.dart';

const baseUrl = 'https://bike.sotavn.com';
// const baseUrl = 'https://paint.sotavn.com/';
const locationIpUrl = 'http://ip-api.com/json/?fields=61439';

Future<String> getSignPaint(String path) async {
  var time = (DateTime.now().millisecondsSinceEpoch / 1000).toInt();
  var sha1 = await crypto(time.toString() +
      AppCypto.key_nail +
      AppCypto.appId_nail +
      path +
      AppCypto.key_nail +
      path +
      AppCypto.appId_nail +
      path +
      time.toString());
  String sign = time.toString() + "." + sha1.toString();
  return sign;
}

class HaiBHAppApiHelper {
  final http.Client httpClient;

  HaiBHAppApiHelper({required this.httpClient});

  Future<bool> registerUser(
      BuildContext context, UserDeviceModel userDeviceModel) async {
    print('registerUser');
    try {
      var map = new Map<String, dynamic>();

      map['name'] = userDeviceModel.display_name;
      map['phone'] = userDeviceModel.phone_number;
      map['email'] = userDeviceModel.email;
      map['password'] = userDeviceModel.password;
      map['password_confirmation'] = userDeviceModel.re_password;
      map['address'] = userDeviceModel.address;
      map['region_id'] = userDeviceModel.regionId!;
      map['district_id'] = userDeviceModel.districtId!;

      Response response = await post(
        Uri.parse(baseUrl + AppCypto.pathRegister),
        body: map,
      );
      if (response.statusCode == 200) {
        var json = jsonDecode(response.body);
        int status = json['status'];
        if (status == 1) return true;
        showToast(context, '${json['message']}');
        return false;
      } else {
        print('registerUser statusCode !=200');
        return false;
      }
    } catch (_) {
      print('registerUser fail: ${_.toString()}');
      return false;
    }
  }

  Future<String?> login(UserDeviceModel userDeviceModel, bool isLoginFb) async {
    print('login');
    try {
      var map = new Map<String, dynamic>();
      if (isLoginFb) {
        map['id'] = '${userDeviceModel.phone_number}';
        map['password'] = '${userDeviceModel.password}';
        // map['is_fb_login'] = '$isLoginFb';
      } else {
        map['id'] = '${userDeviceModel.phone_number}';
        map['password'] = '${userDeviceModel.password}';
        // map['is_fb_login'] = 'false';
      }

      Response response = await post(
        Uri.parse(baseUrl + AppCypto.pathLogin),
        body: map,
      );

      if (response.statusCode == 200) {
        var json = jsonDecode(response.body);
        int code = json['status'];
        if (code == 1) {
          return json['data']['access_token'];
        }
        return null;
      } else {
        print('login statusCode !=200');
        return null;
      }
    } catch (_) {
      print('login fail: ${_.toString()}');
      return null;
    }
  }

  Future<String> forgotPassWord(UserDeviceModel userDeviceModel) async {
    print('forgotPassWord');
    try {
      var map = new Map<String, dynamic>();
      map['phone'] = userDeviceModel.phone_number;
      map['password'] = userDeviceModel.password;
      map['password_confirmation'] = userDeviceModel.re_password;
      Response response = await post(
        Uri.parse(baseUrl + AppCypto.pathForgotPassWord),
        // headers: {'Authorization': 'Bearer ${userDeviceModel.token}'},
        body: map,
      );

      if (response.statusCode == 200) {
        var json = jsonDecode(response.body);
        int code = json['status'];
        if (code == 1) {
          return json['data']['access_token'];
        }
        return '';
      } else {
        print('forgotPassWord statusCode !=200');
        return '';
      }
    } catch (_) {
      print('forgotPassWord fail: ${_.toString()}');
      return '';
    }
  }

  Future<bool> changePassWord(
      String oldPassWord, String newPassWord, String phoneNumber) async {
    print('changePassWord');
    try {
      // String sign = await getSignPaint(AppCypto.pathChangePassWord);
      // print("changePassWord + " + sign);
      var map = new Map<String, dynamic>();
      // map['sign'] = sign;
      // map['app_id'] = AppCypto.appId_nail;
      // map['old_password'] = oldPassWord;
      // map['new_password'] = newPassWord;
      // map['re_new_password'] = newPassWord;
      map['phone'] = phoneNumber;
      map['password'] = newPassWord;
      map['password_confirmation'] = newPassWord;
      Response response = await post(
        Uri.parse(baseUrl + AppCypto.pathChangePassWord),
        // headers: {'Authorization': 'Bearer $token'},
        body: map,
      );
      if (response.statusCode == 200) {
        var json = jsonDecode(response.body);
        int status = json['status'];
        if (status == 1) {
          return true;
        }
        return false;
      } else {
        print('changePassWord statusCode !=200');
        return false;
      }
    } on Exception catch (_) {
      print('login fail: ${_.toString()}');
      return false;
    }
  }

  Future<Map<String, dynamic>?> getFavoritePosts(
      String page, String token) async {
    try {
      String sign = await getSignPaint(AppCypto.pathFavoritePosts);
      var map = new Map<String, dynamic>();
      map['sign'] = sign;
      map['app_id'] = AppCypto.appId_nail;
      map['page'] = page;
      Response response = await post(
        Uri.parse(baseUrl + AppCypto.pathFavoritePosts),
        headers: {'Authorization': 'Bearer $token'},
        body: map,
      );
      if (response.statusCode == 200) {
        var json = jsonDecode(response.body);
        return json;
      } else {
        return null;
      }
    } on Exception catch (_) {
      return null;
    }
  }

  Future<Map<String, dynamic>?> addFavoritePosts(
      int id, String token, bool isFavorite) async {
    try {
      String sign = await getSignPaint(AppCypto.pathFavoritePosts);
      var map = new Map<String, dynamic>();
      map['sign'] = sign;
      map['app_id'] = AppCypto.appId_nail;
      map['post_id'] = '$id';
      map['is_favorite'] = '$isFavorite';
      Response response = await post(
        Uri.parse(baseUrl + AppCypto.pathFavoritePosts),
        headers: {'Authorization': 'Bearer $token'},
        body: map,
      );
      if (response.statusCode == 200) {
        var json = jsonDecode(response.body);
        return json;
      } else {
        print('addFavoritePosts statusCode !=200');
        return null;
      }
    } on Exception catch (_) {
      print('addFavoritePosts fail: ${_.toString()}');
      return null;
    }
  }

  Future<UserDeviceModel> getInfoUser(UserDeviceModel userDeviceModel) async {
    print('getInfoUser');
    try {
      var map = new Map<String, dynamic>();
      var response = await post(
        Uri.parse(baseUrl + AppCypto.pathGetInfo),
        headers: {'Authorization': 'Bearer ${userDeviceModel.token}'},
        body: map,
      );
      if (response.statusCode == 200) {
        var json = jsonDecode(response.body);
        final user = UserDeviceModel.fromJson(json);
        return user;
      } else {
        print('getInfoUser statusCode !=200');
        return UserDeviceModel();
      }
    } catch (_) {
      print('getInfoUser fail: ${_.toString()}');
      return UserDeviceModel();
    }
  }

  Future<dynamic> getLocationIp() async {
    try {
      var response = await get(Uri.parse(locationIpUrl));
      if (response.statusCode == 200) {}
      return response.body;
    } catch (_) {
      return null;
    }
  }

  Future<ConfigAppModel> getConfigApp() async {
    try {
      var response = await get(Uri.parse(baseUrl + AppCypto.pathConfig));
      if (response.statusCode == 200) {
        var json = jsonDecode(response.body);
        ConfigAppModel model = ConfigAppModel.fromJson(json);
        return model;
      }
      return ConfigAppModel(flashSaleConfig: FlashSaleConfig());
    } catch (_) {
      return ConfigAppModel(flashSaleConfig: FlashSaleConfig());
    }
  }

  Future<NotificationEntity> getNotification(String token) async {
    print('getNotification');
    try {
      var response = await get(
        Uri.parse(baseUrl + AppCypto.pathNotification),
        headers: {'Authorization': 'Bearer ${token}'},
        // body: map,
      );
      if (response.statusCode == 200) {
        var json = jsonDecode(response.body);
        NotificationEntity notificationEntity =
            NotificationEntity.fromJson(json);
        return notificationEntity;
      } else {
        print('getNotification statusCode !=200');
        return NotificationEntity();
      }
    } catch (_) {
      print('getNotification fail: ${_.toString()}');
      return NotificationEntity();
    }
  }

  Future<Products> getListFavorite(
      {required String token, required int countPage}) async {
    try {
      var response = await get(
        Uri.parse(
            baseUrl + "/api/product/favorite?per_page=10&page=$countPage"),
        headers: {'Authorization': 'Bearer ${token}'},
        // body: map,
      );
      if (response.statusCode == 200) {
        var json = jsonDecode(response.body);
        Products product = Products.fromJson(json);
        return product;
      } else {
        print('getListFavorite statusCode !=200');
        return Products();
      }
    } catch (_) {
      print('getListFavorite fail: ${_.toString()}');
      return Products();
    }
  }

  Future<Products> getSearch(String key, {int page = 1}) async {
    print('getSearch');
    try {
      var response = await get(
        Uri.parse(baseUrl +
            '/api/product/search?keyword=$key&per_page=100&page=$page'),
        // headers: {'Authorization': 'Bearer ${userDeviceModel.token}'},
        // body: map,
      );
      if (response.statusCode == 200) {
        var json = jsonDecode(response.body);
        if (json['status'] == 1) {
          Products product = Products.fromJson(json);
          return product;
        }
      }
      return Products();
    } catch (_) {
      print('getNotification fail: ${_.toString()}');
      return Products();
    }
  }
}
