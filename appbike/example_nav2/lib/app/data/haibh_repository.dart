import 'dart:convert';

import 'package:example_nav2/app/data/api/haibh_api_helper.dart';
import 'package:example_nav2/app/models/config/config_app_model.dart';
import 'package:example_nav2/app/models/notification/notification_model.dart';
import 'package:example_nav2/app/models/order/product_order_detail.dart';
import 'package:example_nav2/app/models/product/products_model.dart';
import 'package:example_nav2/app/models/setting/setting_model.dart';
import 'package:example_nav2/app/models/user/userDeviceModel.dart';
import 'package:example_nav2/app/models/user/user_model.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:localstorage/localstorage.dart';

const String userKey = 'user';
const String configSettingKey = 'config_setting';

class HaiBHRepository {
  final HaiBHAppApiHelper appApiHelper =
      HaiBHAppApiHelper(httpClient: http.Client());
  final LocalStorage _localStorage = LocalStorage(userKey);

  HaiBHRepository._privateConstructor();

  static final HaiBHRepository _instance =
      HaiBHRepository._privateConstructor();

  static HaiBHRepository get instance => _instance;

  Future<SettingModel> getSettingModelStorage() async {
    try {
      var ready = await _localStorage.ready;
      if (ready) {
        final stringJson = _localStorage.getItem(configSettingKey);
        Map<String, dynamic> dictionary =
            json.decode(stringJson) ?? Map<String, dynamic>();
        return SettingModel.fromJson(dictionary);
      } else {
        return SettingModel();
      }
    } catch (error) {
      return SettingModel();
    }
  }

  Future<bool> saveSettingModelStorage(SettingModel model) async {
    String response = jsonEncode(model.toJson());
    try {
      await _localStorage.setItem(configSettingKey, response);
      print('Lưu thành công: $response');
      return true;
    } catch (_) {
      return false;
    }
  }

  Future<void> saveUserToLocalStorage(UserDeviceModel user) async {
    try {
      await _localStorage.setItem(userKey, json.encode(user.toJson()));
      print('Lưu thành công');
    } catch (error) {
      throw Exception(error.toString());
    }
  }

  Future<UserDeviceModel> getUserFromLocalStorage() async {
    try {
      var ready = await _localStorage.ready;
      final stringJson = _localStorage.getItem(userKey);
      if (stringJson == null || stringJson == '') {
        return UserModel.empty;
      }
      Map<String, dynamic> dictionary =
          json.decode(stringJson) ?? Map<String, dynamic>();
      return UserDeviceModel.getUserFromLocal(dictionary);
    } catch (error) {
      return new UserDeviceModel();
    }
  }



  Future<String> forgotPass(UserDeviceModel userDeviceModel) async {
    return await appApiHelper.forgotPassWord(userDeviceModel);
  }

  Future<bool> register(
      BuildContext context, UserDeviceModel userDeviceModel) async {
    return await appApiHelper.registerUser(context, userDeviceModel);
  }

  Future<String?> login(UserDeviceModel userDeviceModel, isLoginFb) async {
    return await appApiHelper.login(userDeviceModel, isLoginFb);
  }

  Future<UserDeviceModel> getInfoUser(UserDeviceModel userDeviceModel) async {
    return await appApiHelper.getInfoUser(userDeviceModel);
  }

  Future<ConfigAppModel> getConfig() async {
    return await appApiHelper.getConfigApp();
  }

  Future<NotificationEntity> getNotification(String token) async {
    return await appApiHelper.getNotification(token);
  }

  Future<Products> getListFavorite(
      {required String token, required int countPage}) async {
    return await appApiHelper.getListFavorite(
        token: token, countPage: countPage);
  }

  Future<Products> getSearch({required String key, required int page}) async {
    return await appApiHelper.getSearch(key, page: page);
  }

  Future<bool> changePassWord(
      String oldPass, String newPass, String phoneNumber) async {
    return await appApiHelper.changePassWord(oldPass, newPass, phoneNumber);
  }

  Future<dynamic> getLocationIp() async {
    return await appApiHelper.getLocationIp();
  }
}
