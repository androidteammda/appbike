import 'package:example_nav2/app/utils/app_color.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

class ViewHeaderScreen extends GetView {
  ViewHeaderScreen({required this.body});

  final Widget body;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: [
          Container(
            height: 64,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [AppColor.primary_second, AppColor.primary],
                begin: Alignment.centerLeft,
                end: Alignment.centerRight,
                stops: [0.1, 1.0],
                // tileMode: TileMode.clamp,
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 4),
            decoration: BoxDecoration(
              color: AppColor.background_grey,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(15),
                topRight: Radius.circular(15),
              ),
            ),
          ),
          body,
        ],
      ),
    );
  }
}
