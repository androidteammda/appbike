import 'package:example_nav2/app/models/product/products_model.dart';
import 'package:example_nav2/app/modules/home/controllers/home_controller.dart';
import 'package:example_nav2/app/modules/root/controllers/root_controller.dart';
import 'package:example_nav2/app/routes/app_pages.dart';
import 'package:example_nav2/app/utils/app_color.dart';
import 'package:example_nav2/app/utils/assets.dart';
import 'package:example_nav2/app/utils/utility.dart';
import 'package:example_nav2/app/widget/image_view_content.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

class TileGridProduct extends StatelessWidget {
  HomeController homeController = Get.find<HomeController>();

  @override
  Widget build(BuildContext context) {
    return homeController.listProductBest.length == 0
        ? const SizedBox()
        : Container(
            margin: EdgeInsets.only(right: 5, bottom: 10),
            child: GridView.builder(
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  crossAxisSpacing: 6,
                  mainAxisSpacing: 6,
                  childAspectRatio: 0.6,
                ),
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                padding: const EdgeInsets.all(0),
                physics: const BouncingScrollPhysics(),
                itemCount: (homeController.listProductBest.length > 30) ? 30 : (homeController.listProductBest.length),
                itemBuilder: (BuildContext context, int index) =>
                    _itemBestSell(index)));
  }

  Widget _itemBestSell(int index) {
    ItemsProduct itemsProduct = homeController.listProductBest.value[index];
    RootController rootController = Get.find();
    String price = '${itemsProduct.price}';

    if (rootController.isLogged) {
      if (rootController.userDeviceModel.typeUser!.contains('vip1')) {
        if (itemsProduct.price_vip1 != null &&
            itemsProduct.price_vip1!.isNotEmpty) {
          price = '${itemsProduct.price_vip1}';
        }
      } else if (rootController.userDeviceModel.typeUser!.contains('vip2')) {
        if (itemsProduct.price_vip2 != null &&
            itemsProduct.price_vip2!.isNotEmpty) {
          price = '${itemsProduct.price_vip2}';
        }
      }
    }
    return InkWell(
        onTap: () {
          Get.rootDelegate
              .toNamed(Routes.PRODUCT_DETAIL, arguments: itemsProduct);
        },
        child: Container(
          margin: EdgeInsets.only(left: 5, top: 5),
          decoration: const BoxDecoration(
            color: AppColor.white,
            borderRadius: BorderRadius.all(
              Radius.circular(20),
            ),
          ),
          padding: const EdgeInsets.all(8),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: [
              Expanded(
                child: ImageViewContent(
                    src: itemsProduct.imageMainUrl ?? '',
                    place: AssetContentApp.ic_item_180),
              ),
              const SizedBox(height: 4),
              TextField(
                enabled: false,
                decoration: InputDecoration(
                  hintText: itemsProduct.name ?? '',
                  hintStyle: FONT_CONST.REGULAR.copyWith(
                    color: AppColor.blackText,
                    fontSize: 14,
                  ),
                  hintMaxLines: 2,
                  border: InputBorder.none,
                ),
                keyboardType: TextInputType.multiline,
                maxLines: 2,
              ),
              const SizedBox(height: 4),
              Row(
                children: [
                  Expanded(
                    child: Text(
                      Utility.getValuePrice(price),
                      maxLines: 1,
                      style: FONT_CONST.REGULAR.copyWith(
                        color: AppColor.red_second,
                        fontSize: 14,
                      ),
                    ),
                  ),
                  Container(
                    padding:
                        EdgeInsets.only(left: 8, right: 8, top: 6, bottom: 6),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      color: AppColor.red_second,
                    ),
                    child: Text(
                      'Mua ngay',
                      style: FONT_CONST.REGULAR
                          .copyWith(color: AppColor.white, fontSize: 13),
                    ),
                  )
                ],
              ),
            ],
          ),
        ));
  }
}
