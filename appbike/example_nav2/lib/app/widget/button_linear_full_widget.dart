import 'package:example_nav2/app/utils/app_color.dart';
import 'package:example_nav2/app/utils/text_styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

class ButtonLinearApp extends GetView {
  final String title;
  final Function() onTap;

  ButtonLinearApp({required this.title, required this.onTap});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        alignment: Alignment.center,
        margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
        padding: EdgeInsets.all(12),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: <Color>[AppColor.primary, AppColor.primary_second],
          ),
          borderRadius: BorderRadius.circular(10),
        ),
        child: Text(
          '$title'.toUpperCase(),
          style: TextStyles.textBoldStyles2(AppColor.white, 15),
        ),
      ),
    );
  }
}
