import 'package:example_nav2/app/models/product/products_model.dart';
import 'package:example_nav2/app/modules/cart/views/add_item.dart';
import 'package:example_nav2/app/modules/product_details/controllers/product_detail_controller.dart';
import 'package:example_nav2/app/modules/root/controllers/root_controller.dart';
import 'package:example_nav2/app/routes/app_pages.dart';
import 'package:example_nav2/app/utils/app_color.dart';
import 'package:example_nav2/app/utils/assets.dart';
import 'package:example_nav2/app/utils/utility.dart';
import 'package:example_nav2/app/widget/image_view_content.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

class ItemProductView extends GetView {
  final ItemsProduct itemsProduct;
  bool isInDetails = false;

  ItemProductView({required this.itemsProduct, this.isInDetails = false});

  @override
  Widget build(BuildContext context) {
    return _itemFlashSale();
  }

  Widget _itemFlashSale() {
    RootController rootController = Get.find();
    String price = Utility.getPrice(rootController, itemsProduct);
    return InkWell(
      onTap: () {
        if (!isInDetails) {
          Get.rootDelegate
              .toNamed(Routes.PRODUCT_DETAIL, arguments: itemsProduct);
        } else {
          ProductDetailController detailController = Get.find();
          detailController.refreshData(item: itemsProduct);
        }
      },
      child: Container(
        margin: const EdgeInsets.only(right: 16),
        padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 8),
        decoration: BoxDecoration(
          color: AppColor.white,
          borderRadius: BorderRadius.all(
            Radius.circular(12),
          ),
        ),
        width: 140,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Expanded(
              child: Container(
                alignment: Alignment.center,
                child: ImageViewContent(
                    src: itemsProduct.imageMainUrl ?? '',
                    place: AssetContentApp.ic_item),
              ),
            ),
            // const SizedBox(height: 8),

            TextField(
              enabled: false,
              decoration: InputDecoration(
                hintText: itemsProduct.name ?? '',
                hintStyle: FONT_CONST.REGULAR.copyWith(
                  color: AppColor.blackText,
                  fontSize: 12,
                ),
                hintMaxLines: 2,
                border: InputBorder.none,
              ),
              keyboardType: TextInputType.multiline,
              maxLines: 2,
            ),
            // Text(
            //   itemsProduct.name ?? '',
            //   maxLines: 1,
            //   style: FONT_CONST.REGULAR.copyWith(
            //     color: AppColor.primary_second,
            //     fontSize: 12,
            //   ),
            // ),
            // const SizedBox(height: 4),
            Row(
              children: [
                Expanded(
                  flex: 1,
                  child: Text(
                    Utility.getValuePrice(price),
                    maxLines: 1,
                    style: FONT_CONST.MEDIUM.copyWith(
                      color: AppColor.red,
                      fontSize: 12,
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Text(
                    Utility.getValuePrice(
                        itemsProduct.priceBeforeDiscount ?? '0'),
                    maxLines: 1,
                    style: FONT_CONST.MEDIUM.copyWith(
                      color: AppColor.grayText,
                      fontSize: 12,
                      decoration: TextDecoration.lineThrough,
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(height: 4),
            Row(
              children: [
                AssetContentApp.ic_dat_hang,
                const SizedBox(width: 4),
                Expanded(
                  flex: 1,
                  child: Text(
                    'Đặt Hàng',
                    style: FONT_CONST.REGULAR.copyWith(
                      color: AppColor.blackText,
                      fontSize: 14,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class ItemProductFavoriteView extends GetView {
  final ItemsProduct itemsProduct;

  ItemProductFavoriteView({required this.itemsProduct});

  @override
  Widget build(BuildContext context) {
    return _itemFavorite();
  }

  Widget _itemFavorite() {
    RootController rootController = Get.find();
    String price = Utility.getPrice(rootController, itemsProduct);
    return Card(
      margin: const EdgeInsets.only(bottom: 16),
      child: InkWell(
        onTap: () {
          Get.rootDelegate
              .toNamed(Routes.PRODUCT_DETAIL, arguments: itemsProduct);
        },
        child: Container(
          padding: const EdgeInsets.all(12),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 1,
                child: SizedBox(
                  height: 150,
                  child: ImageViewContent(
                      src: itemsProduct.imageMainUrl ?? '',
                      place: AssetContentApp.ic_item),
                ),
              ),
              const SizedBox(width: 16),
              Expanded(
                flex: 2,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      '${itemsProduct.name}',
                      style: FONT_CONST.REGULAR.copyWith(
                        color: AppColor.blackText,
                        fontSize: 18,
                      ),
                    ),
                    const SizedBox(
                      height: 32,
                    ),
                    Row(
                      children: [
                        Text(
                          Utility.getValuePrice(price),
                          style: FONT_CONST.REGULAR.copyWith(
                            color: AppColor.primary,
                            fontSize: 20,
                          ),
                        ),
                        const SizedBox(
                          width: 4,
                        ),
                        Expanded(
                          child: Text(
                            Utility.getValuePrice(
                                itemsProduct.priceBeforeDiscount ?? '0'),
                            style: FONT_CONST.REGULAR.copyWith(
                              color: AppColor.grayText,
                              fontSize: 14,
                              decoration: TextDecoration.lineThrough,
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class ItemProductCartView extends GetView {
  final ItemsProduct itemsProduct;

  ItemProductCartView({required this.itemsProduct});

  @override
  Widget build(BuildContext context) {
    return _itemCart();
  }

  Widget _itemCart() {
    RootController rootController = Get.find();
    String price = Utility.getPrice(rootController, itemsProduct);
    return Container(
      alignment: Alignment.centerLeft,
      // color: Colors.amber,
      margin: const EdgeInsets.only(bottom: 12),
      child: Stack(
        children: [
          Container(
            padding: const EdgeInsets.all(12),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 100,
                  width: 100,
                  child: ImageViewContent(
                      src: itemsProduct.imageMainUrl ?? '',
                      place: AssetContentApp.ic_item),
                ),
                const SizedBox(width: 16),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(
                        height: 12,
                      ),
                      Text(
                        itemsProduct.name ?? '',
                        style: FONT_CONST.REGULAR.copyWith(
                          color: AppColor.blackText,
                          fontSize: 18,
                        ),
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: Text(
                              Utility.getValuePrice(price),
                              style: FONT_CONST.SEMIBOLD.copyWith(
                                color: AppColor.primary,
                                fontSize: 20,
                              ),
                            ),
                          ),
                          const SizedBox(
                            width: 4,
                          ),
                          AddItemWidget(
                            itemsProduct: itemsProduct,
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          Positioned(
            right: 8,
            top: 8,
            child: InkWell(
              onTap: () {},
              child: Padding(
                padding: const EdgeInsets.all(4),
                child: Icon(
                  CupertinoIcons.clear,
                  color: AppColor.normalGrey,
                  size: 20,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class ItemProductSearchView extends GetView {
  final ItemsProduct product;

  ItemProductSearchView({required this.product});

  Widget _itemProduct() {
    RootController rootController = Get.find();
    String price = Utility.getPrice(rootController, product);
    return ListTile(
      leading: Icon(
        CupertinoIcons.shopping_cart,
        color: AppColor.red,
      ),
      onTap: () {
        print('Select Items');
        Get.rootDelegate.toNamed(Routes.PRODUCT_DETAIL, arguments: product);
      },
      title: Text(
        product.name!,
        style: FONT_CONST.REGULAR.copyWith(
          color: AppColor.blackText,
          fontSize: 16,
        ),
      ),
      subtitle: Text(
        Utility.getValuePrice(price),
        style: FONT_CONST.SEMIBOLD.copyWith(
          color: AppColor.red,
          fontSize: 14,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return _itemProduct();
  }
}
