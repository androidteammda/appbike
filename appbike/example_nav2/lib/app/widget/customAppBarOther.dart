import 'package:example_nav2/app/modules/root/controllers/root_controller.dart';
import 'package:example_nav2/app/routes/app_pages.dart';
import 'package:example_nav2/app/utils/app_color.dart';
import 'package:example_nav2/app/utils/assets.dart';
import 'package:example_nav2/app/widget/customWidget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CustomAppBarOther extends GetView implements PreferredSizeWidget {
  final String title;
  final bool isSearch;
  RootController rootController = Get.find<RootController>();

  CustomAppBarOther({required this.title, required this.isSearch});

  @override
  Widget build(BuildContext context) {
    return rootController.tabIndex.value == 0 ? _appBarHome() : _appBar();
  }

  Widget _appBarHome() {
    return Container(
        color: AppColor.primary_second,
        child: Stack(
          alignment: Alignment.center,
          children: [
            _appBar(),
            _logo(),
          ],
        ));
  }

  Widget _appBar() {
    RootController rootController = Get.find<RootController>();
    bool isShow = false;
    isShow = rootController.tabIndex.value == 0 ? true : false;
    return AppBar(
      toolbarHeight: 56,
      elevation: 0,
      backgroundColor: rootController.tabIndex.value == 0
          ? Colors.transparent
          : AppColor.primary_second,
      leading: Builder(
        builder: (BuildContext context) {
          return IconButton(
            icon: AssetContentApp.ic_menu,
            onPressed: () {
              Scaffold.of(context).openDrawer();
            },
            tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
          );
        },
      ),
      // leading: IconButton(
      //   padding: const EdgeInsets.all(0),
      //   onPressed: () {},
      //   icon: AssetContentApp.ic_menu,
      // ),
      title: rootController.tabIndex.value != 0
          ? Text(
              rootController.titleAppBar.value,
              style: FONT_CONST.REGULAR.copyWith(
                color: AppColor.white,
                fontSize: 24,
              ),
            )
          : SizedBox(),
      centerTitle: true,
      actions: [
        IconButton(
          padding: const EdgeInsets.all(0),
          onPressed: () {
            Get.rootDelegate.toNamed(Routes.SEARCH);
          },
          icon: AssetContentApp.ic_search,
        ),
        IconButton(
          padding: const EdgeInsets.all(0),
          onPressed: () {
            Get.rootDelegate.toNamed(Routes.CART);
          },
          icon: AssetContentApp.ic_cart(rootController.countProductOrder.value.toString()),
        ),
      ],
    );
  }

  Widget _logo() {
    return Container(
        margin: EdgeInsets.only(top: 30),
        child: Image.asset(
          'images/export/ic_logo.png',
          height: 40,
          fit: BoxFit.cover,
        ));
  }

  Widget _searchInput() {
    return Column(
      children: [
        const SizedBox(
          height: 16,
        ),
        inputForm('Tìm kiếm sản phẩm, thương hiệu...'),
        const SizedBox(
          height: 16,
        ),
      ],
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(56);
}
