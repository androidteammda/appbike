import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';

class ImageViewContent extends StatelessWidget {
  String src;
  Widget place;

  ImageViewContent({Key? key, required this.src, required this.place})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<String> err = [];
    // List<String> irr = [
    //   '000019/doi-tuyen-viet-nam.jpg',
    //   '171531e90e084c7b821cfcd33a1ae97d.jpg',
    //   '26e010a5a57f41469e77c35666718854.jpg',
    // ];
    Widget errWidget =
        Container(color: Colors.grey.withOpacity(0.5), child: place);
    if ((src == null) || (src.isEmpty)) return errWidget;
    // if (err.contains(src)) return errWidget;
    // for (int i = 0; i < irr.length; i++)
    //   if (src.contains(irr[i])) return errWidget;
    try {
      return CachedNetworkImage(
        imageUrl: src,
        placeholder: (context, url) => errWidget,
        errorWidget: (context, url, error) => errWidget,
        // fit: BoxFit.fitWidth,
        imageBuilder: (context, imageProvider) => Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: imageProvider,
              fit: BoxFit.fill,
            ),
          ),
        ),
      );
    } catch (e) {
      return errWidget;
    }
  }
}
