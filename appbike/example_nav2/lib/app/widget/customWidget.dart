import 'dart:convert';
import 'dart:ui';

import 'package:example_nav2/app/utils/app_color.dart';
import 'package:example_nav2/app/utils/assets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Widget imgInContent(String src, String? alt, BuildContext context) {
  Widget imageWidget;
  print('src Imag: $src');
  if (src == null) {
    imageWidget = Text(alt ?? "");
  } else if (src.startsWith("data:image") && src.contains("base64,")) {
    final decodedImage = base64.decode(src.split("base64,")[1].trim());
    precacheImage(
      MemoryImage(decodedImage),
      context,
      onError: (exception, StackTrace? stackTrace) {},
    );
    imageWidget = Image.memory(
      decodedImage,
      frameBuilder: (ctx, child, frame, _) {
        if (frame == null) {
          return Text(alt ?? "");
        }
        return child;
      },
    );
  } else if (src.startsWith("asset:")) {
    final assetPath = src.replaceFirst('asset:', '');
    precacheImage(
      AssetImage(assetPath),
      context,
      onError: (exception, StackTrace? stackTrace) {},
    );
    imageWidget = Image.asset(
      assetPath,
      frameBuilder: (ctx, child, frame, _) {
        if (frame == null) {
          return Text(alt ?? "");
        }
        return child;
      },
    );
  } else {
    imageWidget = Image.network(
      src,
      fit: BoxFit.fill,
    );
  }
  return imageWidget;
}

Widget inputForm(String hint,
    {TextEditingController? controller, ValueChanged<String>? onSubmitted}) {
  return Container(
    height: 40,
    margin: const EdgeInsets.only(top: 16, left: 0, right: 0, bottom: 16),
    padding: const EdgeInsets.symmetric(
      horizontal: 4,
    ),
    decoration: BoxDecoration(
      color: AppColor.white,
      borderRadius: BorderRadius.all(Radius.circular(24)),
    ),
    alignment: Alignment.center,
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const SizedBox(
          width: 4,
        ),
        Icon(
          CupertinoIcons.search,
          color: AppColor.gray_icon,
          size: 14,
        ),
        const SizedBox(
          width: 4,
        ),
        Expanded(
          child: TextField(
            controller: controller,
            // keyboardType: isPhone ? TextInputType.phone : TextInputType.text,
            textInputAction: TextInputAction.search,
            style: TextStyle(
              fontSize: 12,
              color: AppColor.grayText,
            ),

            textAlign: TextAlign.start,
            textAlignVertical: TextAlignVertical.center,
            // obscureText: isPassword,
            decoration: InputDecoration(
              hintText: hint,
              hintStyle: TextStyle(
                color: AppColor.grayText,
              ),
              border: InputBorder.none,
              focusedBorder: InputBorder.none,
              contentPadding: EdgeInsets.only(bottom: 16),
            ),
            onSubmitted: onSubmitted,
          ),
        ),
      ],
    ),
  );
}

Widget inputFormCode(String hint, {TextEditingController? controller}) {
  return Container(
    height: 44,
    margin: const EdgeInsets.only(top: 16, left: 0, right: 0, bottom: 16),
    padding: const EdgeInsets.symmetric(
      horizontal: 4,
    ),
    decoration: BoxDecoration(
      color: AppColor.white,
      borderRadius: BorderRadius.all(Radius.circular(24)),
      border: Border.all(
        color: Colors.grey,
        width: 1,
      ),
    ),
    alignment: Alignment.center,
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Expanded(
          child: TextField(
            controller: controller,
            // keyboardType: isPhone ? TextInputType.phone : TextInputType.text,
            style: TextStyle(
              fontSize: 12,
              color: AppColor.grayText,
            ),

            textAlign: TextAlign.start,
            textAlignVertical: TextAlignVertical.center,
            // obscureText: isPassword,
            decoration: InputDecoration(
              hintText: hint,
              hintStyle: TextStyle(
                color: AppColor.grayText,
              ),
              border: InputBorder.none,
              focusedBorder: InputBorder.none,
              contentPadding: EdgeInsets.only(bottom: 16, left: 16, right: 16),
            ),
          ),
        ),
      ],
    ),
  );
}

Widget entryField(String hint,
    {TextEditingController? controller,
    bool isPassword = false,
    bool isEmail = false,
    bool isPhone = false}) {
  return Container(
    margin: const EdgeInsets.only(bottom: 16),
    decoration: BoxDecoration(
      color: Colors.white,
      borderRadius: const BorderRadius.all(Radius.circular(22)),
      boxShadow: [
        BoxShadow(
          color: Colors.grey,
          offset: Offset(0, 0.9), //(x,y)
          blurRadius: 4.0,
        ),
      ],
    ),
    child: TextField(
      controller: controller,
      keyboardType: isPhone
          ? TextInputType.phone
          : isEmail
              ? TextInputType.emailAddress
              : TextInputType.text,
      style: TextStyle(
        fontStyle: FontStyle.normal,
        fontWeight: FontWeight.normal,
      ),
      obscureText: isPassword,
      decoration: InputDecoration(
        hintText: hint,
        hintStyle: TextStyle(
          color: AppColor.grayText,
        ),
        border: InputBorder.none,
        focusedBorder: const OutlineInputBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(30),
          ),
          borderSide: BorderSide(color: Colors.blue),
        ),
        contentPadding: EdgeInsets.symmetric(vertical: 15, horizontal: 20),
      ),
    ),
  );
}

Widget loadingView() {
  return Container(
    child: Center(
      child: CircularProgressIndicator(),
    ),
  );
}

Widget emptyView() {
  return Container(
    padding: const EdgeInsets.all(32),
    child: Center(
      child: Text(
        'Không có dữ liệu',
        style: FONT_CONST.REGULAR.copyWith(
          color: AppColor.blackText,
          fontSize: 16,
        ),
      ),
    ),
  );
}

Widget notLoginView() {
  return Container(
    child: Center(
      child: Text(
        'Bạn chưa đăng nhập',
        style: FONT_CONST.REGULAR.copyWith(
          color: AppColor.blackText,
          fontSize: 16,
        ),
      ),
    ),
  );
}

Widget ImageViewNetWork({required String src}) {
  return src.isEmpty || src.contains('localhost')
      ? AssetContentApp.ic_item
      : Image.network(src);
}
