import 'package:example_nav2/app/utils/app_color.dart';
import 'package:example_nav2/app/utils/text_styles.dart';
import 'package:example_nav2/app/widget/customLoader.dart';
import 'package:example_nav2/app/widget/rippleButton.dart';
import 'package:flutter/material.dart';

class FacebookLoginButton extends StatelessWidget {
  const FacebookLoginButton({Key? key, required this.loader});

  final CustomLoader loader;

  void _facebookLogin(context) {
  }

  @override
  Widget build(BuildContext context) {
    return RippleButton(
      onPressed: () {
        _facebookLogin(context);
      },
      borderRadius: BorderRadius.circular(10),
      child: Container(
        width: double.infinity,
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 12),
        decoration: BoxDecoration(
          color: AppColor.blue_fb,
          borderRadius: BorderRadius.circular(10),
          // boxShadow: <BoxShadow>[
          //   BoxShadow(
          //     color: Color(0xffeeeeee),
          //     blurRadius: 15,
          //     offset: Offset(5, 5),
          //   ),
          // ],
        ),
        child: Wrap(
          children: <Widget>[
            Image.asset(
              'images/facebook_logo.png',
              height: 20,
              width: 20,
            ),
            SizedBox(width: 10),
            Text(
              'Đăng nhập với Facebook',
              style: TextStyles.textNormStyles(AppColor.white, 14),
            ),
          ],
        ),
      ),
    );
  }
}
