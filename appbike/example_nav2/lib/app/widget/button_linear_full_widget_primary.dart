import 'package:example_nav2/app/utils/app_color.dart';
import 'package:example_nav2/app/utils/text_styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

class ButtonLinearAppPrimary extends GetView {
  final String title;
  final Color color;
  final Function() onTap;

  ButtonLinearAppPrimary(
      {required this.title, required this.color, required this.onTap});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        alignment: Alignment.center,
        // margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
        padding: EdgeInsets.symmetric(horizontal: 12, vertical: 14),
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(22),
        ),
        child: Text(
          '$title'.toUpperCase(),
          style:
              FONT_CONST.REGULAR.copyWith(color: AppColor.white, fontSize: 16),
        ),
      ),
    );
  }
}
