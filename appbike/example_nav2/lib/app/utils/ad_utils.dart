// import 'package:example_nav2/app/utils/ad_id.dart';
// import 'package:flutter/material.dart';
// import 'package:get/get.dart';
// import 'package:google_mobile_ads/google_mobile_ads.dart';
//
// class AdUtils {
//   AdUtils._privateConstructor();
//
//   static final AdUtils _instance = AdUtils._privateConstructor();
//
//   static AdUtils get instance => _instance;
//
//   InterstitialAd? _interstitialAd;
//   bool _isShowInterstitial = false;
//
//   BannerAd? _anchoredBanner;
//   bool _loadingAnchoredBanner = false;
//
//   static final AdRequest request = AdRequest(
//     keywords: <String>['foo', 'bar'],
//     contentUrl: 'http://foo.com/bar.html',
//     nonPersonalizedAds: true,
//   );
//
//   void initInterstitialAd() {
//     InterstitialAd.load(
//         adUnitId: InterstitialAd.testAdUnitId,
//         request: request,
//         adLoadCallback: InterstitialAdLoadCallback(
//           onAdLoaded: (InterstitialAd ad) {
//             print('$ad loaded');
//             _interstitialAd = ad;
//             _interstitialAd!.setImmersiveMode(true);
//           },
//           onAdFailedToLoad: (LoadAdError error) {},
//         ));
//   }
//
//   Future<void> showInterstitialAd(void onDismiss()) async {
//     if (_interstitialAd == null) {
//       print('Warning: attempt to show interstitial before loaded.');
//       onDismiss();
//       return;
//     }
//     _interstitialAd!.fullScreenContentCallback = FullScreenContentCallback(
//       onAdShowedFullScreenContent: (InterstitialAd ad) =>
//           print('ad onAdShowedFullScreenContent.'),
//       onAdDismissedFullScreenContent: (InterstitialAd ad) {
//         print('$ad onAdDismissedFullScreenContent.');
//         ad.dispose();
//         onDismiss();
//         initInterstitialAd();
//       },
//       onAdFailedToShowFullScreenContent: (InterstitialAd ad, AdError error) {
//         print('$ad onAdFailedToShowFullScreenContent: $error');
//         ad.dispose();
//         initInterstitialAd();
//       },
//     );
//     _interstitialAd!.show();
//     _interstitialAd = null;
//   }
//
//   Future<void> loadAnchoredBanner(void onAddBanner(BannerAd? bannerAd)) async {
//     if (_loadingAnchoredBanner) {
//       return;
//     }
//     _loadingAnchoredBanner = true;
//     final AnchoredAdaptiveBannerAdSize? size =
//         await AdSize.getAnchoredAdaptiveBannerAdSize(Orientation.portrait,
//             MediaQuery.of(Get.context!).size.width.truncate());
//     if (size == null) {
//       print('Unable to get height of anchored banner.');
//       return;
//     }
//
//     final BannerAd banner = BannerAd(
//       size: size,
//       request: request,
//       adUnitId: AdsId.bannerAdId,
//       listener: BannerAdListener(
//         onAdLoaded: (Ad ad) {
//           print('$BannerAd loaded.');
//           _anchoredBanner = ad as BannerAd?;
//           onAddBanner(_anchoredBanner!);
//         },
//         onAdFailedToLoad: (Ad ad, LoadAdError error) {
//           print('$BannerAd failedToLoad: $error');
//           ad.dispose();
//         },
//         onAdOpened: (Ad ad) => print('$BannerAd onAdOpened.'),
//         onAdClosed: (Ad ad) => print('$BannerAd onAdClosed.'),
//       ),
//     );
//     banner.load();
//   }
//
//   void disposeAd() {
//     _interstitialAd?.dispose();
//     _anchoredBanner?.dispose();
//   }
// }
