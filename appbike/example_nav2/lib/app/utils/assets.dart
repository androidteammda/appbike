import 'package:example_nav2/app/utils/app_color.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class AssetContentApp {
  // static const ImageProvider logo = AssetImage('assets/images/icon-480.png');
  static Widget ic_item = Image.asset(
    'images/ic_place_item.png',
    fit: BoxFit.cover,
  );

  static Widget ic_item_180 = Image.asset(
    'images/ic_place_item.png',
    height: 180,
    fit: BoxFit.cover,
  );

  static AssetImage flash_sale = AssetImage('images/bg_flash.png');

  static Widget logo = Image.asset(
    'images/logo.png',
    fit: BoxFit.cover,
  );

  static Widget logo_ba_vuong = Image.asset(
    'images/logo_ba_vuong.png',
    height: 116,
    width: 148,
  );

  static Widget logo_bag = Image.asset(
    'images/logo_bag.png',
    height: 260,
    width: 260,
    fit: BoxFit.cover,
  );

  static Widget ic_menu = SvgPicture.asset(
    'images/icon/ic_menu.svg',
    color: AppColor.white,
    fit: BoxFit.cover,
  );

  static Widget ic_dat_hang = SvgPicture.asset(
    'images/icon/ic_dat_hang.svg',
    fit: BoxFit.cover,
  );

  static Widget ic_bell = SvgPicture.asset(
    'images/icon/ic_bell.svg',
    fit: BoxFit.cover,
  );

  static Widget ic_call = Image.asset(
    'images/icon/ic_call.png',
    width: 26,
    height: 26,
    fit: BoxFit.cover,
  );

  static Widget ic_cart(String count) {
    return Container(
      width: 35,
      height: 45,
      child: Stack(
        alignment: Alignment.center,
        children: [
          Image.asset(
            'images/icon/ic_cart.png',
            width: 30,
            height: 30,
            fit: BoxFit.cover,
          ),
          Container(
            width: 45,
            height: 50,
            alignment: Alignment.bottomRight,
            child: Container(
              margin: EdgeInsets.only(left: 5),
              width: 15,
              height: 15,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Color(0xffc32c37),
                  border: Border.all(color: Colors.white, width: 1)),
              child: Padding(
                padding: const EdgeInsets.all(0.0),
                child: Center(
                  child: Text(
                    count,
                    style: TextStyle(fontSize: 10),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  static Widget ic_search = Image.asset(
    'images/icon/ic_search.png',
    width: 30,
    height: 30,
    fit: BoxFit.cover,
  );

  static Widget ic_paint = Image.asset(
    'images/icon/ic_paint.png',
    width: 26,
    height: 26,
    fit: BoxFit.cover,
  );

  static Widget ic_product = Image.asset(
    'images/icon/ic_product.png',
    width: 26,
    height: 26,
    fit: BoxFit.cover,
  );

  static Widget logo_honda = Image.asset(
    'images/logo_honda.png',
    height: 60,
    width: 72,
    fit: BoxFit.cover,
  );

  static Widget logo_yamaha = Image.asset(
    'images/logo_yamaha.png',
    height: 60,
    width: 72,
    fit: BoxFit.cover,
  );

  static Widget logo_piaggio = Image.asset(
    'images/logo_piaggio.png',
    height: 60,
    width: 72,
    fit: BoxFit.cover,
  );
  static Widget logo_sym = Image.asset(
    'images/logo_sym.png',
    height: 60,
    width: 72,
    fit: BoxFit.cover,
  );
  static Widget logo_suzuki = Image.asset(
    'images/logo_suzuki.png',
    height: 60,
    width: 72,
    fit: BoxFit.cover,
  );

// static Widget img_placeholder = Image.asset(
//   'images/icon/placeholder.png',
//   fit: BoxFit.cover,
// );
//
// static String card_qt = 'assets/images/card_qt.png';
}
