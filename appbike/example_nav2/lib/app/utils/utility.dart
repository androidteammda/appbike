import 'dart:ui';

import 'package:example_nav2/app/models/product/products_model.dart';
import 'package:example_nav2/app/modules/root/controllers/root_controller.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../main.dart';

class Utility {
  static String getPrice(
      RootController rootController, ItemsProduct itemsProduct) {
    String price = '${itemsProduct.price}';
    if (rootController.isLogged) {
      if (rootController.userDeviceModel.typeUser!.contains('vip1')) {
        if (itemsProduct.price_vip1 != null &&
            itemsProduct.price_vip1!.isNotEmpty) {
          price = '${itemsProduct.price_vip1}';
        }
      } else if (rootController.userDeviceModel.typeUser!.contains('vip2')) {
        if (itemsProduct.price_vip2 != null &&
            itemsProduct.price_vip2!.isNotEmpty) {
          price = '${itemsProduct.price_vip2}';
        }
      }
    }
    return price;
  }

  static String getPollTime(String date) {
    int hr, mm;
    String msg = 'Poll ended';
    var enddate = DateTime.parse(date);
    if (DateTime.now().isAfter(enddate)) {
      return msg;
    }
    msg = 'Poll ended in';
    var dur = enddate.difference(DateTime.now());
    hr = dur.inHours - dur.inDays * 24;
    mm = dur.inMinutes - (dur.inHours * 60);
    if (dur.inDays > 0) {
      msg = ' ' + dur.inDays.toString() + (dur.inDays > 1 ? ' Days ' : ' Day');
    }
    if (hr > 0) {
      msg += ' ' + hr.toString() + ' hour';
    }
    if (mm > 0) {
      msg += ' ' + mm.toString() + ' min';
    }
    return (dur.inDays).toString() +
        ' Days ' +
        ' ' +
        hr.toString() +
        ' Hours ' +
        mm.toString() +
        ' min';
  }

  static String getSocialLinks(String? url) {
    if (url != null && url.isNotEmpty) {
      url = url.contains("https://www") || url.contains("http://www")
          ? url
          : url.contains("www") &&
                  (!url.contains('https') && !url.contains('http'))
              ? 'https://' + url
              : 'https://www.' + url;
    } else {
      return '';
    }
    // cprint('Launching URL : $url');
    return url;
  }

  static bool validateCredentials(BuildContext context,
      {String? phone, String? password}) {
    if (phone == null || phone.isEmpty) {
      showToast(context, 'Bạn chưa nhập số điện thoại');
      return false;
    } else if (password == null || password.isEmpty) {
      // customSnackBar(_scaffoldKey, 'Bạn chưa nhập mật khẩu');
      showToast(context, 'Bạn chưa nhập mật khẩu');
      return false;
    } else if (password.length < 4) {
      showToast(context, 'Mật khẩu tôi phải dài 4 ký tự');
      return false;
    }
    var isPhone = validateMobile(phone);
    if (isPhone) {
      return true;
    }
    showToast(context, 'Vui lòng nhập số điện thoại hợp lệ');
    return false;
  }

  static bool validatePhoneAndEmail(BuildContext context,
      {String? phone, String? password, String? email}) {
    if (phone == null || phone.isEmpty) {
      showToast(context, 'Bạn chưa nhập số điện thoại');
      return false;
    // } else if (email == null || email.isEmpty) {
    //   showToast(context, 'Bạn chưa nhập email');
    //   return false;
    } else if (password == null || password.isEmpty) {
      // customSnackBar(_scaffoldKey, 'Bạn chưa nhập mật khẩu');
      showToast(context, 'Bạn chưa nhập mật khẩu');
      return false;
    } else if (password.length < 4) {
      showToast(context, 'Mật khẩu phải dài 5 ký tự');
      return false;
    }
    // var isEmail = validateEmal(email);

    var isPhone = validateMobile(phone);
    if (isPhone
        // && isEmail
    ) {
      return true;
    }
    showToast(context, 'Vui lòng nhập số điện thoại hợp lệ');
    return false;
  }

  static bool validateMobile(String value) {
    String pattern = r'(^(?:[+0]9)?[0-9]{10,12}$)';
    RegExp regExp = new RegExp(pattern);
    if (value.length == 0) {
      return false;
    } else if (!regExp.hasMatch(value)) {
      return false;
    }
    return true;
  }

  static bool validateEmal(String email) {
    String p =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regExp = new RegExp(p);
    var status = regExp.hasMatch(email);
    return status;
  }

  static void showDialog() {}

  static String getValuePrice(String price) {
    if (price.contains('null') || price.isEmpty) {
      price = '0';
    }
    return NumberFormat.simpleCurrency(locale: 'vi')
        .format(double.parse(price));
  }

  static bool iphoneXDevice() {
    return window.viewPadding.bottom > 0;
  }
}
