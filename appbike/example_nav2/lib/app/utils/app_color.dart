import 'package:flutter/material.dart';

const appFontFamily = "Montserrat";

// ignore: avoid_classes_with_only_static_members
class AppColor {
  static const Color primary = Color(0xFF136F9B);
  static const Color primary_second = Color(0xFF136F9B);
  static const Color red_prim = Color(0xFFBC0505);
  static const Color red_second = Color(0xFFFF0000);
  static const Color primaryOrange = Color(0xffFDA020);
  static const Color primaryBlue = Color(0xff24AEB1);
  static const Color secondary = Color(0xff14171A);
  static const Color darkGrey = Color(0xff1657786);
  static const Color extraExtraLightGrey = Color(0xfF5F8FA);
  static const Color white = Color(0xFFffffff);
  static const Color bg_white_button = Color(0x1AFFFFFF);
  static const Color white_transparent_2 = Color(0x33FCFCFC);
  static const Color blackText = Color(0xff212121);
  static const Color grayText = Color(0xff707070);
  static const Color black = Color(0xff000000);
  static const Color blue_1 = Color(0xFF259EA1);
  static const Color blue_2 = Color(0xFF00509B);

  static const Color normalGrey = Colors.grey;
  static const Color? lightGrey = Color(0xfff1f1f1);
  static const Color red = Color(0xFF136F9B);
  static const Color blue = Color(0xff1DA1F2);
  static const Color gradientBottom = Color(0xFFA993EA);
  static const Color gradientTop = Color(0xFFF8A7A7);
  static const Color blue_app = Color(0xFF0E294E);
  static const Color blue_text = Color(0xFF153E73);

  static const Color black_click = Color(0x12000000);
  static const Color grey_click = Color(0xfff1f1f1);

  static const Color lightGrey2 = Color(0xfff1f1f1);
  static const Color searchBar = Color(0xFFFFFF);

  static const Color blue_fb = Color(0xFF4267B2);

  static const Color background_app = Color(0xFFF1F0F5);

  static const Color background_grey = Color(0xFFE0E0E0);

  static const Color background_blur = Color(0xFFF4F7FC);
  static const Color background_intro = Color(0xFFF4F7FC);

  static const Color color_orange = Color(0xFFFB6709);
  static const Color color_orange_alpha = Color(0xFFFFE5D5);
  static const Color blue_bottom = Color(0xFF0E284D);
  static const Color bottom_sheet_white = Color(0xFFEAECF0);
  static const Color color_green = Color(0xFF8DDA7A);
  static const Color blue_weight = Color(0xFF1BA4A8);
  static const Color blue_weight_alpha = Color(0xFFD6FEFF);

  static const Color color_green_alpha = Color(0xFF8CDA79);
  static const Color color_green_progress = Color(0xFF1C9B00);

  static const Color gray_icon = Color(0xFFAAAAAA);
  static const Color gray_bg_input = Color(0xFFBABABA);
  static const Color gray_bg_main = Color(0xFFE2E2E2);

  static const Color button_green = Color(0xFF24AEB1);


}

class FONT_CONST {
  static final REGULAR = TextStyle(
    fontFamily: appFontFamily,
    fontSize: 16,
    fontWeight: FontWeight.w400,
    color: AppColor.blackText,
  );

  static final MEDIUM = TextStyle(
    fontFamily: appFontFamily,
    fontSize: 18,
    fontWeight: FontWeight.w500,
    color: AppColor.blackText,
  );

  static final SEMIBOLD = TextStyle(
    fontFamily: appFontFamily,
    fontSize: 20,
    fontWeight: FontWeight.w700,
    color: AppColor.blackText,
  );

  static final TEXT_THEME = TextTheme(
    /// Used for emphasizing text that would otherwise be [bodyText2].
    bodyText1: FONT_CONST.REGULAR,

    /// The default text style for [Material].
    bodyText2: FONT_CONST.REGULAR,

    /// Used for text on [ElevatedButton], [TextButton] and [OutlinedButton].
    button: FONT_CONST.REGULAR,

    /// Used for auxiliary text associated with images.
    caption: FONT_CONST.REGULAR,

    /// Used for the primary text in app bars and dialogs
    headline6: FONT_CONST.REGULAR,

    /// The smallest style. Typically used for captions or to introduce a (larger) headline.
    overline: FONT_CONST.REGULAR,

    /// Used for the primary text in lists (e.g., [ListTile.title]).
    subtitle1: FONT_CONST.REGULAR,

    /// For medium emphasis text that's a little smaller than [subtitle1].
    subtitle2: FONT_CONST.REGULAR,
  );
}
