enum AuthStatus {
  NOT_DETERMINED,
  DETERMINED,
  NOT_LOGGED_IN,
  LOGGED_IN,
}

enum TypeUser {
  NORMAL,
  VIP1,
  VIP2,
}
