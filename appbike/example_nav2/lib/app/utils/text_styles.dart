import 'package:example_nav2/app/utils/app_color.dart';
import 'package:flutter/material.dart';

class TextStyles {
  TextStyles._();

  static TextStyle get onWhiteTitleText {
    return TextStyle(
        color: AppColor.white,
        fontWeight: FontWeight.w600,
        fontSize: 18,
        fontFamily: appFontFamily);
  }

  static TextStyle get onWhiteSubTitleText {
    return TextStyle(
        color: AppColor.white,
        fontSize: 14,
        fontWeight: FontWeight.bold,
        fontFamily: appFontFamily);
  }

  static TextStyle get onBlackTitleText {
    return TextStyle(
        color: AppColor.blackText,
        fontWeight: FontWeight.w600,
        fontSize: 18,
        fontFamily: appFontFamily);
  }

  static TextStyle get onBlackSubTitleText {
    return TextStyle(
        color: AppColor.blackText,
        fontSize: 14,
        fontWeight: FontWeight.bold,
        fontFamily: appFontFamily);
  }

  static TextStyle get titlePrimaryStyle {
    return TextStyle(
        color: AppColor.primary, fontSize: 18, fontFamily: appFontFamily);
  }

  static TextStyle textNormStyles(Color color, double size) {
    return TextStyle(
        color: color,
        fontSize: size,
        decoration: TextDecoration.none,
        fontFamily: appFontFamily,
        fontWeight: FontWeight.normal);
  }

  static TextStyle textBoldStyles(Color color, double size) {
    return TextStyle(
        color: color,
        fontSize: size,
        decoration: TextDecoration.none,
        fontFamily: appFontFamily,
        fontWeight: FontWeight.bold);
  }

  static TextStyle textBoldStyles2(Color color, double size) {
    return TextStyle(
        color: color,
        fontSize: size,
        decoration: TextDecoration.none,
        decorationColor: AppColor.black,
        decorationStyle: TextDecorationStyle.solid,
        fontFamily: appFontFamily,
        fontWeight: FontWeight.bold);
  }

  static TextStyle textItalicStyles(Color color, double size) {
    return TextStyle(
        color: color,
        fontSize: size,
        fontFamily: appFontFamily,
        fontStyle: FontStyle.italic);
  }
}
