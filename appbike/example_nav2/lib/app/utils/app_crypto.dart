import 'dart:async';
import 'dart:convert';
import 'dart:core';
import 'dart:io';

import 'package:crypto/crypto.dart';

final _usage = 'Usage: dart hash.dart <md5|sha1|sha256> <input_filename>';

class AppCypto {
  static final String key = "1e5dAyBN0JVdrwYx4c";
  static final String appId = "kFytZJORDqv0JSKBpmW6RM71qcYpZY5nSh5K";
  static final String key_nail = "jWXhOdSBDWngVtY7i9";
  static final String appId_nail = "vWxXencr53KF2t4YjqWGmRjVd7kP15V2U3SN";
  static final String path = "/api/favorites/getListCategory";

  static final String pathRegister = "/api/user/register";
  static final String pathLogin = "/api/user/login";
  static final String pathGetInfo = "/api/user/info";
  // static final String pathChangePassWord = "/api/user/changePassWord";
  static final String pathChangePassWord = "/api/user/forgotpass";
  static final String pathForgotPassWord = "/api/user/forgotpass";
  static final String pathUpdateProfile = "/api/user/updateProfile";
  static final String pathNotification =
      "/api/notification?per_page=10&page=1&type=event";
  static final String pathConfig = "/api/config";

  //-- new path api
  static final String pathFavoritePosts = "/api/post/getFavoritePosts";
  static final String pathLocations = '/api/location/getAll';
  static final String pathSubmitBook = '/api/user/submitBooking';

  static final String pathCategories = '/api/categories?';
  static final String pathCategoriesProduct = '/api/categories/products?';
  static final String pathProductDetail = '/api/product/detail?';
  static final String pathProductFavorite = '/api/product/like?';
  static final String pathListProductRelated = '/api/product/related?';
  static final String pathListProductHot = '/api/product/hot?';
  static final String pathListProductFlashSale = '/api/product/flashsale?';
  static final String pathListProductFavorite = '/api/product/favorite?';
  static final String pathLikeProduct = '/api/product/like?';
  static final String pathListProductSearch = '/api/product/search?';

  static final String pathListProductOrder = '/api/order?';
  static final String pathOrderCreate = '/api/order/create';
  static final String pathOrderCancel = '/api/order/cancel?';
  static final String pathOrderPromotionCode = '/api/order/promotionCode?';
  static final String pathOrderShipFee = '/api/order/shipFee?';
  static final String pathOrderPaymentFee = '/order/paymentFee?';

  static final String pathLocationRegion = '/api/location/region?';
  static final String pathLocationDistrict = '/api/location/district?';
}

Future<String> crypto(String sign) async {
  var bytes = utf8.encode(sign);
  var sha1_sign = sha1.convert(bytes);
  return "$sha1_sign";
}

Future main(List<String> args) async {
  if (args.length != 2) {
    print(_usage);
    exitCode = 64; // Command was used incorrectly.
    return;
  }

  Hash hasher;
  switch (args[0]) {
    case 'md5':
      hasher = md5;
      break;
    case 'sha1':
      hasher = sha1;
      break;
    case 'sha256':
      hasher = sha256;
      break;
    default:
      print(_usage);
      exitCode = 64; // Command was used incorrectly.
      return;
  }

  var filename = args[1];
  var input = File(filename);

  if (!input.existsSync()) {
    print('File "$filename" does not exist.');
    exitCode = 66; // An input file did not exist or was not readable.
    return;
  }

  var value = await hasher.bind(input.openRead()).first;

  print(value);
}
